import json
import requests
import sys

def usage(exe):
    print(f'Usage: {exe} <package name>\n')

# main
if len(sys.argv) < 2:
    usage(sys.argv[0])
    sys.exit(1)
pkg = sys.argv[1]

url = f'https://artefact.skao.int/service/rest/v1/search?sort=version&repository=docker-internal&name={pkg}'
r = requests.get(url)
data = r.json()

print(data['items'][0]['version'])
