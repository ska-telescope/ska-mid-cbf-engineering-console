#!/bin/bash

#NOTE: This script must be run as a user with passwordless root permission
#NOTE: This script must be run as the account that owns the repository (required by git commands)

# Update environment variables to include quartus tools (needed when running via cronjob):
	# License server for Intel Quartus FPGA tools
	LM_LICENSE_FILE=28060@localhost
	
	# Intel Quartus
	FPGA_INTEL=/data/intelFPGA_pro/22.2
	QSYS_ROOTDIR=$FPGA_INTEL/qsys/bin
	PATH=$FPGA_INTEL/quartus/bin:$FPGA_INTEL/quartus/linux64:$PATH
	
	# SKA Project
	FPGA_TOOLS=~/fw_repo/fpga_tools
	REGDEF_PATH=~/fw_repo/registerDef
	PATH=$PATH:/data/share/bin
	
	FPGA_BUILD=$FPGA_TOOLS/fpga_build
	PYTHONPATH=$FPGA_TOOLS:$REGDEF_PATH


USAGE_BANNER="Usage: ./setup.sh BOARDS
  BOARDS: List of talon boards to test (valid: 1-4). Examples:  1,2  2,1,3  3,4  1,2,3,4
    Note that while all boards are 'exercised', only the first board will have its data captured
    and will determine the pass/fail status of the test."

BOARDS=$1

if [ "$BOARDS" == "" ]; then
	echo "$USAGE_BANNER"
	exit 1
fi

# Input Validation
for talon in ${BOARDS//,/ }
do
        if [ "$talon" != "1" ] && [ "$talon" != "2" ] && [ "$talon" != "3" ] && [ "$talon" != "4" ]; then
                echo "ERROR: Invalid talon board: $talon"
                echo "$USAGE_BANNER"
                exit 1
        fi
done

## GLOBAL VARIABLES
TALON_BITSTREAM_ARCHIVE="ska-mid-cbf-talondx-0.2.3.tar.gz"
MINIKUBE_PORT="30176"
CURRENT_USER=$(whoami)
DEV1_HOSTNAME="rmdskadevdu001.mda.ca"
DEV2_HOSTNAME="rmdskadevdu002.mda.ca"
TALON_SCRIPT_REPO="/shared/talon-dx-utilities/bin"
ENG_CONSOLE_REPO="/home/$CURRENT_USER/git/ska-mid-cbf-engineering-console"
MINIKUBE_REPO="/home/$CURRENT_USER/git/ska-cicd-deploy-minikube"
TEMP_SETUP_LOGS_DIR="$ENG_CONSOLE_REPO/automation/results/temp_log"
PING_ATTEMPTS=15
PING_ECHOES_NEEDED_FOR_SUCCESS=1

echo "#############################################################################################################"
echo "### Extracting networking information based on Test Environment"
echo "#############################################################################################################"

if [ "$(hostname)" == "$DEV1_HOSTNAME" ];then
	RDMA_IFACE_NAME="ens2f1np1"
elif [ "$(hostname)" == "$DEV2_HOSTNAME" ];then
	RDMA_IFACE_NAME="ens4f1np1"
fi


function recordConfiguration()
{
	echo ""
	echo "#############################################################################################################"
	echo "### Recording configuration"
	echo "#############################################################################################################"
	echo ""
	echo "Setup start time: $(date --iso-8601=seconds)"
	echo ""
	echo "Current server: $(hostname)"
	echo "RDMA_IFACE_NAME: $RDMA_IFACE_NAME"
	echo ""
	echo "Talons under test: $BOARDS"
	echo ""
	echo "TALON_BITSTREAM_ARCHIVE: $TALON_BITSTREAM_ARCHIVE"
	echo ""
	cd $ENG_CONSOLE_REPO
	echo "Git commit HASH for Automated Script: $(git rev-parse HEAD)"
	git fetch
	git status
}


function startMinikube()
{
	echo ""
	echo "#############################################################################################################"
	echo "### Starting minikube"
	echo "#############################################################################################################"
	echo ""

	cd $MINIKUBE_REPO
	git pull
	
	if grep -q "k8s-minikube" <<< $(docker ps | grep minikube); then
		echo "INFO: Minikube already running. Deleting minikube before starting it under current profile."
		make minikube-clean
		echo ""
		sleep 1
	fi

	echo "INFO: Running 'make minikube-install'..."
	if [ "$(hostname)" == "$DEV2_HOSTNAME" ]; then #TODO Are different arguments necessary? Why?
		make minikube-install DRIVER=docker CPUS=8 MEM=32768MB
	else #DEV1
		make minikube-install DRIVER=docker
	fi
	sleep 3

	echo ""
	echo "INFO: Checking minikube status..."
	MINIKUBE_STATUS=$(minikube status 2>&1)
	echo "$MINIKUBE_STATUS"
	if ! grep -q "host: Running" <<< "$MINIKUBE_STATUS"; then
		echo "ERROR: Minikube (host) not running nominally. Aborting..."
		exit 1
	fi
	if ! grep -q "kubelet: Running" <<< "$MINIKUBE_STATUS"; then
		echo "ERROR: Minikube (kubelet) not running nominally. Aborting..."
		exit 1
	fi
	if ! grep -q "apiserver: Running" <<< "$MINIKUBE_STATUS"; then
		echo "ERROR: Minikube (apiserver) not running nominally. Aborting..."
		exit 1
	fi
	if ! grep -q "kubeconfig: Configured" <<< "$MINIKUBE_STATUS"; then
		echo "ERROR: Minikube not configured. Aborting..."
		exit 1
	fi
	cd $ENG_CONSOLE_REPO

	echo ""
	echo "INFO: Configuring arp table..."
	arp -H ether -i $RDMA_IFACE_NAME -s 10.50.0.10 02:22:33:44:55:67
}

function saveDevServerNetworkConfiguration()
{
	echo "Server: $(hostname)"
	echo "Time: $(date --iso-8601=seconds)"

	echo "#$ route -n"
	route -n

	echo "#$ ip a"
	ip a


	printf "\n\n###Testing DB connection from host server\n"
	nc -zv tango-host-databaseds-from-makefile-test.ska-mid-cbf.svc.cluster.local 10000


	echo "#$ arp"
	arp

	echo "#$ sudo iptables-save"
	sudo iptables-save

	echo "#$ cat /etc/resolv.conf"
	cat /etc/resolv.conf

	echo "#$ cat /etc/hosts"
	cat /etc/hosts
}

# Program Talon Boards
function pingTalon()
{
        talon=$1
        echo "Pinging $talon..."
        talon_ping_status=$(ping -c $PING_ATTEMPTS $talon 2>&1)
        echo "$talon_ping_status"

        number_of_ping_packets_received=$(echo "$talon_ping_status" | grep "packets transmitted" | awk '{print $4}')

        echo ""
        echo "number_of_ping_packets_received: $number_of_ping_packets_received"

        if [ "$number_of_ping_packets_received" == "" ]; then
                echo "ERROR: Could not determine number_of_ping_packets_received."
                return "1"
        fi

        if [ "$number_of_ping_packets_received" -lt "$PING_ECHOES_NEEDED_FOR_SUCCESS" ]; then
                echo "Received $number_of_ping_packets_received/$PING_ATTEMPTS packets. Talon not pingable."
                return "1"
        else
                echo "Received $number_of_ping_packets_received/$PING_ATTEMPTS packets. Talon pingable."
                return "0"
        fi
}

function restartLRU()
{
        LRU=$1
        echo "Restarting LRU #$LRU..."
        $TALON_SCRIPT_REPO/talon_power_lru.sh "lru$LRU" off
	echo "Sleeping 5 seconds..."
        sleep 5
        $TALON_SCRIPT_REPO/talon_power_lru.sh "lru$LRU" on
        echo "Sleeping 30 seconds..."
        sleep 30
}

function verifyTalonUp()
{
        TALON="talon$1"
        LRU="$(( ($1/2) + ($1%2) ))"
        pingTalon $TALON
        if [ $? -ne "0" ]; then
                restartLRU $LRU
                pingTalon $TALON
                if [ $? -ne "0" ]; then
                        echo "$TALON still unpingable after LRU restart. Aborting..."
                        exit 1
                fi
        fi
}

function checkTalonConfigurationFile()
{
        echo ""
	echo "INFO: Checking talon configuration file..."
	
	if [ ! -d $ENG_CONSOLE_REPO/automation/mnt ]; then
		echo "$ENG_CONSOLE_REPO/automation/mnt missing. Run automated script manually to initialize. Aborting..."
		exit 1
	fi

        TALON_CONFIG_FILE=$(find $ENG_CONSOLE_REPO/automation/mnt/talondx-config/fpga-talon -name $TALON_BITSTREAM_ARCHIVE)
        echo "INFO: Using Talon DX Config: $TALON_CONFIG_FILE"
        if [ $(echo "$TALON_CONFIG_FILE" | wc -l) -ne "1" ]; then
                echo "ERROR: Did not find a unique talon configuration file (see above). Aborting..."
                exit 1
        fi
}

function programTalonQSPIFlash()
{
        TALON="talon$1"
        
	echo ""
	echo "INFO: Updating $TALON QSPI Flash via update_qspi_remote.sh..."
	echo "FORCE_PROGRAM_QSPI = $FORCE_PROGRAM_QSPI"
	if [ ! -z ${FORCE_PROGRAM_QSPI} ]; then
		echo "INFO: Forcefully updating QSPI Flash on Talon boards..."
		UPDATE_QSPI_STATUS=$($TALON_SCRIPT_REPO/update_qspi_remote.sh $TALON_CONFIG_FILE $TALON software -f 2>&1)
	else
		echo "INFO: Updating QSPI Flash on Talon boards..."
		UPDATE_QSPI_STATUS=$($TALON_SCRIPT_REPO/update_qspi_remote.sh $TALON_CONFIG_FILE $TALON software 2>&1)
	fi
  
	UPDATE_QSPI_EXIT_CODE=$?
	echo "$UPDATE_QSPI_STATUS"
	echo "exit code: $UPDATE_QSPI_EXIT_CODE"
	if [ $UPDATE_QSPI_EXIT_CODE -ne 0 ]; then
		echo "ERROR: QSPI Flash update failed."

		echo "INFO: Programming $TALON QSPI Flash via JTAG (program_qspi_flash.sh)..."
		echo "FORCE_PROGRAM_QSPI = $FORCE_PROGRAM_QSPI"
		if [ ! -z ${FORCE_PROGRAM_QSPI} ]; then
			echo "INFO: Forcefully programming QSPI Flash on Talon boards..."
			TALON_PROGRAM_STATUS=$($TALON_SCRIPT_REPO/program_qspi_flash.sh $TALON_CONFIG_FILE $TALON -f 2>&1)
		else
			echo "INFO: Programming QSPI Flash on Talon boards..."
			TALON_PROGRAM_STATUS=$($TALON_SCRIPT_REPO/program_qspi_flash.sh $TALON_CONFIG_FILE $TALON 2>&1)
		fi

		echo "$TALON_PROGRAM_STATUS"
		if grep -q "Quartus Prime Programmer was successful. 0 errors, 0 warnings" <<< "$TALON_PROGRAM_STATUS"; then
			echo "(QSPI flash programming successful)"
		elif grep -q "QSPI flash already programmed" <<< "$TALON_PROGRAM_STATUS"; then
			echo "(QSPI flash programming not necessary, skipping...)"
		else
			echo "$TALON QSPI flash programming failed. Aborting..."
			exit 1
		fi
	else
		echo "INFO: QSPI Flash update successful."
	fi

}

function prepareTalonBoards()
{
	echo ""
	echo "#############################################################################################################"
	echo "### Preparing talon boards"
	echo "#############################################################################################################"
	echo ""
	
	echo "INFO: Verifying talon boards are responsive..."
	for talon in ${BOARDS//,/ }
	do
		verifyTalonUp $talon
	done
	
	checkTalonConfigurationFile
	
	echo "SKIP_PROGRAM_QSPI = $SKIP_PROGRAM_QSPI"
	if [ ! -z ${SKIP_PROGRAM_QSPI} ]; then 
		echo "INFO: Skipping programming QSPI Flash on Talon boards."
	else
		echo "INFO: Programming QSPI Flash on Talon boards..."
		for talon in ${BOARDS//,/ }
		do
			programTalonQSPIFlash $talon
		done
	fi
}



# Core
recordConfiguration

echo "SKIP_START_MINIKUBE = $SKIP_START_MINIKUBE"
if [ ! -z ${SKIP_START_MINIKUBE} ]; then
	echo "INFO: Skipping starting minikube."
else
	startMinikube
fi

echo "INFO: Saving Dev Server Network Configuration..."
saveDevServerNetworkConfiguration &> $TEMP_SETUP_LOGS_DIR/networkConfig.$(hostname)

prepareTalonBoards 

echo ""
echo "Setup end time: $(date --iso-8601=seconds)"
echo ""
