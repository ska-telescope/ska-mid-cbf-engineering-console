#!/bin/bash

# NOTE: This script must be run as a user with passwordless root permission
# NOTE: This script must be run from the .../automation directory (pwd command)

USAGE_BANNER="Usage: ./orchestration.sh BOARDS [OPTIONS...]
  BOARDS: List of talon boards to test (valid: 1-4). Examples:  1,2  2,1,3  3,4  1,2,3,4
    Note that while all boards are 'exercised', only the first board will have its data captured
    and will determine the pass/fail status of the test.

  OPTIONS:
  -n NUM_RUNS  Run the test NUM_RUNS times (default=3).
  -q           Skip programming the QSPI Flash on the Talon boards.
  -f           Program the QSPI Flash on the Talon boards regardless of the hash saved on them 
                 (see program_qspi_flash.sh for details). Overruled by '-q'.
  -s           Skip starting minikube before the test.
  -c           Skip 'make minikube-clean' after the test (do not delete the minikube cluster)."

POSITIONAL_ARGS=()
NUM_RUNS=3

unset SKIP_PROGRAM_QSPI
unset FORCE_PROGRAM_QSPI
unset SKIP_START_MINIKUBE
unset SKIP_MINIKUBE_CLEAN

while [[ $# -gt 0 ]]; do
  case $1 in
    -n)   
	    NUM_RUNS="$2"   
      shift # past argument
	    shift # past value
      ;;
    -q)
      echo "Set SKIP_PROGRAM_QSPI"
      export SKIP_PROGRAM_QSPI=1 # used in setup.sh
      shift # past argument
      ;;
    -f)
      echo "Set FORCE_PROGRAM_QSPI"
      export FORCE_PROGRAM_QSPI=1 # used in setup.sh
      shift # past argument
      ;;
    -s)
      echo "Set SKIP_START_MINIKUBE"
      export SKIP_START_MINIKUBE="true" # used in setup.sh
      shift # past argument
      ;;
    -c)
      echo "Set SKIP_MINIKUBE_CLEAN"
      SKIP_MINIKUBE_CLEAN="true"
      shift # past argument
      ;;
    -*|--*)
	  echo "Invalid option $1"
      echo "$USAGE_BANNER"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

BOARDS=$1

if [ "$BOARDS" == "" ]; then
	echo "$USAGE_BANNER"
	exit 1
fi

# Input Validation
for talon in ${BOARDS//,/ }
do
        if [ "$talon" != "1" ] && [ "$talon" != "2" ] && [ "$talon" != "3" ] && [ "$talon" != "4" ]; then
                echo "ERROR: Invalid talon board: $talon"
                echo "$USAGE_BANNER"
                exit 1
        fi
done

## GLOBAL VARIABLES
CURRENT_USER=$(whoami)
MINIKUBE_REPO="/home/$CURRENT_USER/git/ska-cicd-deploy-minikube"
CURR_DIR=$(pwd)
RESULTS_DIR="$CURR_DIR/results"
RESULTS_SUMMARY_FILE="$RESULTS_DIR/results.out"
TEMP_SETUP_LOGS_DIR="$RESULTS_DIR/temp_log"
SETUP_LOG="setup.log"
TEST_LOG="test.log"

LOCKFILE=/tmp/automatedTest.running


if [ -e $LOCKFILE ]
then
	echo "Lockfile \"$LOCKFILE\" exists. This test is already running. Aborting..."
        exit 1
fi
touch $LOCKFILE

function cleanup()
{
	echo "SKIP_MINIKUBE_CLEAN = $SKIP_MINIKUBE_CLEAN"
	if [ ! -z ${SKIP_MINIKUBE_CLEAN} ]; then
		echo "INFO: Skipping 'make minikube-clean'."
	else
		echo "INFO: Deleting minikube cluster and cleaning up..."
		cd $MINIKUBE_REPO
		make minikube-clean
	fi
	
	rm $LOCKFILE
}

function setupTestEnvironment()
{
	rm -rf $TEMP_SETUP_LOGS_DIR
	mkdir -p $TEMP_SETUP_LOGS_DIR
	chmod 777 $RESULTS_DIR
	chmod 777 $TEMP_SETUP_LOGS_DIR
	./setup.sh $BOARDS | tee $TEMP_SETUP_LOGS_DIR/$SETUP_LOG
	
	if [ "${PIPESTATUS[0]}" -ne "0" ]; then
		echo "Setup failed. Deleting minikube and aborting..." | tee -a $TEMP_SETUP_LOGS_DIR/$SETUP_LOG
		run_date=$(date +%Y-%m-%d)
		run_time=$(date +%H-%M-%S)
		TEST_RUN_RESULTS_DIR="$RESULTS_DIR/$run_date/$run_time"
		mkdir -p $TEST_RUN_RESULTS_DIR
		cp -p $TEMP_SETUP_LOGS_DIR/* $TEST_RUN_RESULTS_DIR

		echo "$run_date,$run_time,Setup Failed,$TEST_RUN_RESULTS_DIR" >> $RESULTS_SUMMARY_FILE

		cleanup
		exit 1
	fi
}

function runTest()
{
	run_date=$(date +%Y-%m-%d)
	run_time=$(date +%H-%M-%S)
	TEST_RUN_RESULTS_DIR="$RESULTS_DIR/$run_date/$run_time"
	FINAL_LOG_DIR="$TEST_RUN_RESULTS_DIR/log"
	mkdir -p $TEST_RUN_RESULTS_DIR
	mkdir -p $FINAL_LOG_DIR
	cp -p $TEMP_SETUP_LOGS_DIR/* $FINAL_LOG_DIR
	./script.sh.multiboard $BOARDS $TEST_RUN_RESULTS_DIR 2>&1 | tee $FINAL_LOG_DIR/$TEST_LOG

	if [ "${PIPESTATUS[0]}" -ne "0" ]; then
		echo "Test failed..." | tee -a $FINAL_LOG_DIR/$TEST_LOG
		echo "$run_date,$run_time,Failure,$TEST_RUN_RESULTS_DIR" >> $RESULTS_SUMMARY_FILE
	fi
}

# Core
setupTestEnvironment
for ((i = 0; i < $NUM_RUNS; i++));
do
	runTest
done

cleanup

exit 0
