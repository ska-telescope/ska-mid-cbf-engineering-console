#!/bin/bash

# Source utils file
. utils.sh

## GLOBAL VARIABLES
TANGO_DATABASE=tango-host-databaseds-from-makefile-test
TANGO_HOST=$TANGO_DATABASE:10000
MCS_TANGO_HOST=tango-host-databaseds-from-makefile-test.ska-mid-cbf.svc.cluster.local:10000
SET_IMAGE_TAG="--set ska-mid-cbf.midcbf.image.registry=artefact.skao.int \
                --set ska-mid-cbf.midcbf.image.image=ska-mid-cbf-mcs \
                --set ska-mid-cbf.midcbf.image.tag=0.6.1 \
                --set ska-mid-cbf-tmleafnode.midcbf.image.registry=artefact.skao.int \
                --set ska-mid-cbf-tmleafnode.midcbf.image.image=ska-mid-cbf-mcs \
                --set ska-mid-cbf-tmleafnode.midcbf.image.tag=0.6.1"

EC_IMAGE_VER="0.3.4"

CURR_DIR=$(pwd)

LOG_BASE_DIR=$CURR_DIR/log

# UNINSTALL CHARTS IF UP
helm uninstall ska-mid-cbf --namespace ska-mid-cbf

sleep 10

## CREATE MINIKUBE NAMESPACE
kubectl create namespace ska-mid-cbf

## DELETE ANY EXISTING CHART DIRECTORY OR TARS
rm -r ska-mid-cbf*

## PULL LATEST MID CBF HELM CHART
helm repo update
helm pull skao/ska-mid-cbf

## FIND TGZ FILE PULLED
ska_mid_cbf_tgz=$(find . -name "*ska-mid-cbf*" | grep tgz)

tar -zxvf $ska_mid_cbf_tgz

## INSTALL HELM CHART USING TGZ FILE
helm install ska-mid-cbf \
        --set global.domain="cluster.local" \
        --set global.minikube="true" \
        --set global.tango_host=$TANGO_HOST \
        $SET_IMAGE_TAG --values ska-mid-cbf/values.yaml \
        ska-mid-cbf/ --namespace ska-mid-cbf

sleep 5

## UPDATE DB PORT
kubectl -n ska-mid-cbf patch service/tango-host-databaseds-from-makefile-test --type='json' -p '[{"op":"replace","path":"/spec/ports/0/nodePort","value": 30176}]'

## WAIT TILL FINAL POD STATE IS RUNNING BEFORE PROCEEDING (normally the cbf subarray)
state="NotRunning"

while [[ "$state" != "Running" ]]
do
        state=$(kubectl get pods -n ska-mid-cbf cbfsubarray-cbfsubarray-03-0 -o jsonpath="{.status.phase}")
done

## DELETE HOSTS FILE IF IT EXISTS AND CREATE NEW EMPTY VERSION
if [ -f 'hosts.out' ]; then rm hosts.out;fi
touch hosts.out


## RUN TANGO DNS SCRIPT AND PUT INTO VARIABLE
create_hosts_file

add_hosts=$(cat hosts.out)
space_add_hosts=`echo $add_hosts | tr '\n' ' '`

## PULL ENGINEERING CONSOLE IMAGE
docker pull artefact.skao.int/ska-mid-cbf-engineering-console:$EC_IMAGE_VER
docker pull artefact.skao.int/ska-mid-cbf-engineering-console-rdma:$EC_IMAGE_VER

## CREATE BITE DATA DIR
if [ ! -d 'bite-data' ]; then mkdir bite-data;fi

## CREATE MNT IF DOESNT EXIST
if [ ! -d 'mnt/talondx-config' ]; then mkdir -p mnt/talondx-config;fi

## RUN ENGINEERING CONSOLE CONTAINER IN BACKGROUND
image_id=$(docker run --network host -v $CURR_DIR/bite-data:/app/bite-data:rw -v $CURR_DIR/mnt/talondx-config:/app/artifacts:rw --env RAW_USER_ACCOUNT=$RAW_USER_ACCOUNT --env RAW_USER_PASS=$RAW_USER_PASS --env BITE_DATA_SRC=lstv --env TANGO_HOST=$MCS_TANGO_HOST --env POWER_SWITCH_USER=admin --env POWER_SWITCH_PASS=1234 $space_add_hosts -d artefact.skao.int/ska-mid-cbf-engineering-console:$EC_IMAGE_VER tail -f /dev/null)
echo $image_id

## TURN OFF TALON BOARDS
docker exec -ti $image_id ./talondx.py --talon-power-off

## DOWNLOAD ARTIFACTS FROM CAR
docker exec -ti $image_id ./talondx.py --download-artifacts

## COPY ARTIFACTS INTO CBF CONTROLLER POD
if [ -d 'mnt/talondx-config' ]; then kubectl cp mnt/talondx-config ska-mid-cbf/cbfcontroller-controller-0:/app/mnt/;else echo "ERROR: Cannot find artifacts at talondx-config"; fi

## RUN CONFIG DB TO CONFIGURE TALON DATABASE
docker exec -ti $image_id ./talondx.py --config-db

sleep 5

## RUN TALONDX LOG CONSUMER IMAGE IN THE BACKGROUND
create_log_dir $LOG_BASE_DIR

log_image_id=$(docker run -d --rm --env TANGO_HOST=$MCS_TANGO_HOST --network host $space_add_hosts artefact.skao.int/ska-mid-cbf-engineering-console:$EC_IMAGE_VER python ./talondx_log_consumer/talondx_log_consumer.py talondxlogconsumer-001 -ORBendPoint giop:tcp:142.73.34.173:60721 -ORBendPointPublish giop:tcp:169.254.100.88:60721)
echo "LOG IMAGE ID: $log_image_id"

## TURN ON MCS BOARDS
docker exec -ti $image_id ./talondx.py --mcs-on

sleep 5

## CONFIGURE ARP
sudo arp -H ether -i enp179s0f1 -s 10.50.0.10 02:22:33:44:55:67

## CHECK TALON VERSION
docker exec -ti $image_id ./talondx.py --talon-version

## CONFIGURE BITE
docker exec -ti $image_id ./talondx.py --talon-bite-config

sleep 10

## RDMA-RX
rdma_image_id=$(docker run -d --rm --network host -v $CURR_DIR/mnt/talondx-config/dsrdmarx/bin:/app/dsrdmarx/bin --device=/dev/infiniband/uverbs0 --device=/dev/infiniband/uverbs1 --device=/dev/infiniband/rdma_cm --env TANGO_HOST=$MCS_TANGO_HOST $space_add_hosts artefact.skao.int/ska-mid-cbf-engineering-console-rdma:$EC_IMAGE_VER ./dsrdmarx/bin/dsrdmarx talon2_test -v4)

sleep 2

docker exec -ti $image_id ./talondx.py --rdma-on-commands

sleep 2

create_capture_dir "bite-data/capture-results"

## DISH PACKET CAPTURE
docker run -d --network host -v $CURR_DIR/$final_capture_dir:/app/bite-data:rw --env BITE_DATA_SRC=lstv --env TANGO_HOST=$MCS_TANGO_HOST --env CAPTURE_DIR=/app/bite-data $space_add_hosts artefact.skao.int/ska-mid-cbf-engineering-console:$EC_IMAGE_VER ./talondx.py --dish-packet-capture

sleep 2

## TALON BITE REPLAY
docker exec -ti $image_id ./talondx.py --talon-bite-lstv-replay

## EXTRACT LOGS FILES FROM LOG CONSUMER CONTAINER
docker exec $log_image_id bash -c "mkdir -p /extract; cp -f /app/mid_csp_cbf_talondx_log_consumer_talondxlogconsumer-001* /extract"
docker cp $log_image_id:/extract/. $final_log_dir

sleep 5

## EXTRACT RDMA DATA FROM
docker cp $rdma_image_id:/app/dsrdmarx/rdma-rx-data.dat $final_log_dir

sleep 5

## KILL BACKGROUND CONTAINERS
docker kill $image_id
docker kill $log_image_id
docker kill $rdma_image_id

sleep 5

## WRITE RESULTS
RESULTS_DIR=$CURR_DIR/results
if [ ! -d $RESULTS_DIR ]; then mkdir -p $RESULTS_DIR;fi
write_bite_results $final_capture_dir $final_log_dir "none" $RESULTS_DIR
