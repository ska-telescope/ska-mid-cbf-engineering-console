#!/bin/bash

create_hosts_file () {
	kubectl get services -n "ska-mid-cbf" | while read -r line;
	do
    		svc_split=($line)
    		if [ "${svc_split[1]}" = "LoadBalancer" ]; then
        		host_name="${svc_split[0]%"-external"}.ska-mid-cbf.svc.cluster.local"
        		echo "--add-host=$host_name:${svc_split[3]}" >> hosts.out
    		fi
	done
}

create_directory () {
        dir=$1
        if [[ -z $dir ]]; then echo "No directory provided"; exit 5;fi
        if [[ ! -d $dir ]]; then mkdir -p $dir; fi
}

viewTalonBoardNetworkConfiguration() {

	TALON="talon$1"

	echo "Talon Board: $TALON"
	echo "Time: $(date --iso-8601=seconds)"

	printf "\n\n###cat /proc/version\n"
	ssh root@$TALON cat /proc/version

	printf "\n\n###route -n\n"
	ssh root@$TALON route -n

	printf "\n\n###ip a\n"
	ssh root@$TALON ip a

	printf "\n\n###nslookup tango-host-databaseds-from-makefile-test.ska-mid-cbf.svc.cluster.local\n"
	ssh root@$TALON nslookup tango-host-databaseds-from-makefile-test.ska-mid-cbf.svc.cluster.local

	printf "\n\n###cat /etc/resolv.conf\n"
	ssh root@$TALON cat /etc/resolv.conf

	printf "\n\n###cat /etc/hosts\n"
	ssh root@$TALON cat /etc/hosts

	printf "\n\n###cat /etc/network/interfaces\n"
	ssh root@$TALON cat /etc/network/interfaces

	printf "\n\n###cat /lib/firmware/hps_software/hps_master_mcs.sh\n"
	ssh root@$TALON cat /lib/firmware/hps_software/hps_master_mcs.sh
}
