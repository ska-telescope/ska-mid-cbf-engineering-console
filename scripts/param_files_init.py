import argparse
import os
import logging
import json
import subprocess
from jsonschema import validate

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
BITE_CONFIGS_DIR = os.path.abspath('../../ska-mid-cbf-system-tests/test_parameters/cbf_input_data/bite_config_parameters/')
CBF_INPUT_DATA_DIR = os.path.abspath('../../ska-mid-cbf-system-tests/test_parameters/cbf_input_data')
TEST_PARAMS_DIR = os.path.abspath('../../ska-mid-cbf-system-tests/test_parameters/')
BITE_DEVICE_CLIENT_DIR = os.path.abspath('../src/ska_mid_cbf_engineering_console/bite_device_client/')

SCHEMAS_DIR = os.path.abspath('../../ska-mid-cbf-internal-schemas/')
BITE_CONFIGS_SCHEMA_DIR = os.path.abspath('../../ska-mid-cbf-internal-schemas/test_parameters/cbf_input_data/bite_config_parameters/')
CBF_INPUT_DATA_SCHEMA_DIR = os.path.abspath('../../ska-mid-cbf-internal-schemas/test_parameters/cbf_input_data/')
TEST_PARAMS_SCHEMA_DIR = os.path.abspath('../../ska-mid-cbf-internal-schemas/test_parameters/')

def validate_bite_config_param_files():
    # Validate Tests config file 
    validate(
        instance=json.load(open(os.path.join(TEST_PARAMS_DIR, 'tests.json'))),
        schema=json.load(open(os.path.join(TEST_PARAMS_SCHEMA_DIR, 'tests_schema.json'), "r"))
    )

    # Validate CBF Input Data config file
    validate(
        instance=json.load(open(os.path.join(CBF_INPUT_DATA_DIR, 'cbf_input_data.json'))),
        schema=json.load(open(os.path.join(CBF_INPUT_DATA_SCHEMA_DIR, 'cbf_input_data_schema.json'), "r"))
    )

    # Validate BITE config file
    validate(
        instance=json.load(open(os.path.join(BITE_CONFIGS_DIR, 'bite_configs.json'))),
        schema=json.load(open(os.path.join(BITE_CONFIGS_SCHEMA_DIR, 'bite_configs_schema.json'), "r"))
    )

    # Validate filter definitions file
    validate(
        instance=json.load(open(os.path.join(BITE_CONFIGS_DIR, 'filters.json')))['filters'],
        schema=json.load(open(os.path.join(BITE_CONFIGS_SCHEMA_DIR, 'filters_schema.json'), "r"))
    )


def copy_bite_config_param_files(docker_name: str):

    subprocess.run(
        [
            f"docker exec {docker_name} mkdir -p test_parameters/cbf_input_data/bite_config_parameters"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )

    # Copy tests.json and cbf_input_data folder to test_parameters/
    dest_path = f"{docker_name}:/app/src/ska_mid_cbf_engineering_console/bite/test_parameters/"
    subprocess.run(
        [
            f"docker cp {os.path.join(TEST_PARAMS_DIR, 'tests.json')} {dest_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    subprocess.run(
        [
            f"docker cp {CBF_INPUT_DATA_DIR} {dest_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )

    # Copy device_server_list.json
    dest_path = f"{docker_name}:/app/src/ska_mid_cbf_engineering_console/bite/bite_device_client/json"
    subprocess.run(
        [
            f"docker cp {os.path.join(BITE_DEVICE_CLIENT_DIR, 'json/device_server_list.json')} {dest_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )

    # Copy midcbf_bite.py
    dest_path = f"{docker_name}:/app/src/ska_mid_cbf_engineering_console/bite"
    subprocess.run(
        [
            f"docker cp {os.path.join(BITE_DEVICE_CLIENT_DIR, '../midcbf_bite.py')} {dest_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    # Copy bite_client.py
    dest_path = f"{docker_name}:/app/src/ska_mid_cbf_engineering_console/bite/bite_device_client"
    subprocess.run(
        [
            f"docker cp {os.path.join(BITE_DEVICE_CLIENT_DIR, 'bite_client.py')} {dest_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )

    # Copy schemas
    subprocess.run(
        [
            f"docker exec {docker_name} mkdir -p schemas/cbf_input_data/bite_config_parameters"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )

    dest_path = f"{docker_name}:/app/src/ska_mid_cbf_engineering_console/bite/schemas/"
    subprocess.run(
        [
            f"docker cp {os.path.join(TEST_PARAMS_SCHEMA_DIR, 'tests_schema.json')} {dest_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )
    subprocess.run(
        [
            f"docker cp {CBF_INPUT_DATA_SCHEMA_DIR} {dest_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )

    # Copy device_server_list_schema.json
    dest_path = f"{docker_name}:/app/src/ska_mid_cbf_engineering_console/bite/bite_device_client/"
    subprocess.run(
        [
            f"docker cp {os.path.join(BITE_DEVICE_CLIENT_DIR, 'device_server_list_schema.json')} {dest_path}"
        ],
        shell=True,
        stdout=subprocess.PIPE,
        text=True,
        check=False,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--validate', help='Validate the BITE parameter files against their schemas.', action='store_true')
    parser.add_argument('-c', '--copy', help='Copy test parameter files, and their schema files, to the EC-BITE container', type=str)
    args = parser.parse_args()

    if args.validate:
        validate_bite_config_param_files()
    
    if args.copy:
        copy_bite_config_param_files(args.copy)