"""
Wraps TANGO device-based BITE commands for use via the Makefile. See src/ska_mid_cbf_engineering_console/bite/bite.py.
"""
from tango import DeviceProxy, DevFailed, ConnectionFailed
import argparse
import logging
import os
import json

LOG_FORMAT = "[ds_local_bite.py: line %(lineno)s]%(levelname)s: %(message)s"

BITE_FQDN = "mid_csp_cbf/ec/bite"

def load_files(bite, logger, config_directory:str, test_id:str) -> bool:
    """
    Loads in the required JSON config files needed before invoking commands on the device. Returns true if successful, false otherwise
    """
    # Check all files are available in expected sub directories
    tests = os.path.join(config_directory,"tests.json")
    bite_configs = os.path.join(config_directory,"cbf_input_data/bite_config_parameters/bite_configs.json")
    filters = os.path.join(config_directory,"cbf_input_data/bite_config_parameters/filters.json")
    cbf_input_data = os.path.join(config_directory,"cbf_input_data/cbf_input_data.json")
    if not os.path.isfile(tests):
         logger.error(f"Did not find a tests.json file in the provided directory: {tests}")
         return False
    if not os.path.isfile(bite_configs):
        logger.error(f"Did not find a bite_config.json file in the provided directory: {bite_configs}")
        return False
    if not os.path.isfile(filters):
        logger.error(f"Did not find a filters.json file in the provided directory: {filters}")
        return False
    if not os.path.isfile(cbf_input_data):
        logger.error(f"Did not find a cbf_input_data.json file in the provided directory: {cbf_input_data}")
        return False
    logger.info("Found all files required for config.")

    # Use the test_id to grab what cbf config we need to use from Tests.json
    with open(tests) as f:
        cbf_id = json.load(f)["tests"][test_id]["cbf_input_data"]
    # Use this config id to grab the config from the cbf input data file
    with open(cbf_input_data) as f:
        cbf_input = json.load(f)["cbf_input_data"][cbf_id]
    
    # Push this to the bite proxy as a string
    try:
        bite.load_cbf_input_data(json.dumps(cbf_input))
    except DevFailed:
        logger.error("BITE loading of CBF input data failed, check logs!")
        return False
    except ConnectionFailed:
        logger.error("Could not connect to BITE device, ensure it is deployed!")
        return False
    logger.info("CBF input data loaded.")

    # Load in bite_configs.json
    with open(bite_configs) as f:
        bite_config_input = json.load(f)
    try:
        bite.load_bite_config_data(json.dumps(bite_config_input))
    except DevFailed:
        logger.error("BITE loading of config data failed, check logs!")
        return False
    except ConnectionFailed:
        logger.error("Could not connect to BITE device, ensure it is deployed!")
        return False
    logger.info("Config data loaded.")

    # Load in filters.json
    with open(filters) as f:
        filters_input = json.load(f)
    try:
        bite.load_filter_data(json.dumps(filters_input))
    except DevFailed:
        logger.error("BITE loading of filter data failed, check logs!")
        return False
    except ConnectionFailed:
        logger.error("Could not connect to BITE device, ensure it is deployed!")
        return False
    logger.info("Filter data loaded")
    
    logger.info("Done!")
    return True

def generate_bite_data(bite, logger) -> None:
    """
    Generates test configuration based on set data.
    """
    # Pass in JSON of receptors data as str by using dumps
    bite.set_timeout_millis(180000)
    try:
        bite.generate_bite_data()
    except DevFailed as df:
        logger.error(f"An error occurred when running the generate BITE data command: {df}")
        return False
    except ConnectionFailed:
        logger.error("Could not connect to BITE device, ensure it is deployed!")
        return False
    bite.set_timeout_millis(3000)
    logger.info("Done!")

def start_lstv_replay(bite, logger) -> None:
    """
    Starts the generation of LSTV signal for use.
    """
    try:
        bite.start_lstv_replay()
    except DevFailed as df:
        logger.error(f"An error occurred when starting LSTV replay: {df}")
        return False
    except ConnectionFailed:
        logger.error("Could not connect to BITE device, ensure it is deployed!")
        return False
    logger.info("Done!")

def stop_lstv_replay(bite, logger) -> None:
    """
    Stops LSTV replay generation.
    """
    try:
        bite.stop_lstv_replay()
    except DevFailed as df:
        logger.error(f"An error occurred when stopping LSTV replay: {df}")
        return False
    except ConnectionFailed:
        logger.error("Could not connect to BITE device, ensure it is deployed!")
        return False
    logger.info("Done!")

def check_bite_device(bite, logger) -> None:
    """
    Prints the current status of the BITE device.
    """
    logger.info(bite.Status())

def main(args):
    # Set up logger
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    logger = logging.getLogger("ds_local_bite.py")

    # Set up the BITE device proxy
    try:
        bite = DeviceProxy(BITE_FQDN)
    except DevFailed:
        logger.error("Could not create the BITE device proxy, check the database and ensure the BITE device is exported.")
        return

    # Run commands based on the command argument passed 

    if args.check_bite_device:
        check_bite_device(bite, logger)
        return
    
    # Load in the config files if called
    elif args.load_config_files:
        logger.info("Loading in config from JSON files...")
        if args.talon_config_dir is None or args.test_id is None:
            logger.error("Please provide a directory containing the config files for the BITE device and a Test ID to load in.")
            return
        if not load_files(bite, logger, args.talon_config_dir, args.test_id):
            logger.error("Error while loading files!")
            return
    
    # Execute device commands based on the argument passed.
    elif args.generate_bite_data:
        logger.info("Generating BITE data...")
        generate_bite_data(bite, logger)
        return
    
    # Start LSTV replay
    elif args.start_lstv_replay:
        start_lstv_replay(bite, logger)
        return
    
    # Stop LSTV replay
    elif args.stop_lstv_replay:
        stop_lstv_replay(bite, logger)
        return

if __name__ == "__main__":
    # Parse inputs
    parser = argparse.ArgumentParser(description="Wrapper for running BITE device commands.")
    parser.add_argument("--generate-bite-data", help="Generates the BITE data using TANGO device.", action="store_true")
    parser.add_argument("--load-config-files", help="Load in the required JSON to the device using provided files.", action="store_true")
    parser.add_argument("--start-lstv-replay", help="Start LSTV replay on the target Talon board.", action="store_true")
    parser.add_argument("--stop-lstv-replay", help="Stop LSTV replay on the target Talon board.", action="store_true")
    parser.add_argument("--check-bite-device", help="Prints the current status of the BITE device.", action="store_true")
    parser.add_argument("--talon-config-dir", help="Directory containing the config JSON files", type=str)
    parser.add_argument("--test-id", help="ID of the test to run e.g Test_1", type=str)

    main(parser.parse_args())
