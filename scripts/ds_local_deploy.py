"""
Wraps TANGO device-based deployer commands for use via the Makefile. See src/ska_mid_cbf_engineering_console/deployer/deployer_device.py.
"""
from tango import DeviceProxy, DevFailed
import argparse
import logging

from ska_mid_cbf_engineering_console.deployer.midcbf_deployer import FspMode

LOG_FORMAT = "[ds_local_deploy.py: line %(lineno)s]%(levelname)s: %(message)s"

DEPLOYER_FQDN = "mid_csp_cbf/ec/deployer"
DEPLOY_TIME = 100000

def download_artifacts(deployer, logger) -> None:
    """
    Downloads the device artifacts from the CAR repository.
    """
    logger.info("Making sure deployer device is on:")
    deployer.On()
    logger.info(deployer.Status())
    logger.info(f"Targeting board(s) {deployer.targetTalons}.")
    deployer.set_timeout_millis(DEPLOY_TIME)
    try:
        deployer.download_artifacts()
    except DevFailed:
        logger.error("Timed out when trying to deploy.")
    deployer.set_timeout_millis(3000)
    logger.info("Done!")

def generate_config_jsons(deployer, logger) -> None:
    """
    Generates config JSONs and saves them.
    """
    try:
        deployer.generate_config_jsons()
    except DevFailed:
        logger.error("Failed to generate talondx config")
    logger.info("Done!")

def configure_database(deployer, logger) -> None:
    """ 
    Deploys the devices to the TANGO database.
    """
    #This timeout is specifically set for minikube envs, as the tango DB is much slower when in a minikube.
    deployer.set_timeout_millis(10000)
    try:
        deployer.configure_db()
    except DevFailed:
        logger.error("Failed to configure the TANGO DB")
    deployer.set_timeout_millis(3000)
    logger.info("Done!")

def check_deployer(deployer, logger) -> None:
    """
    Helper function that that the deployer device is present and accessible.
    """
    logger.info(f"Deployer status is: {deployer.Status()}")

def device_check(deployer, logger) -> None:
    """
    Pings all devices in the TANGO DB through the deployer device
    """
    try:
        deployer.device_check()
    except DevFailed:
        logger.error("Failed to check devices")
    logger.info("Done!")

def main(args):
    # Set up logger
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    logger = logging.getLogger("ds_local_deploy.py")
    # Set up the deploy device proxy for sending commands
    try:
        deployer = DeviceProxy(DEPLOYER_FQDN)
    except DevFailed as e:
        logger.error("Could not create the TANGO device proxy, check the database and ensure the deployer is exported.")
        return
    
    # Run device commands based on argument passed
    if args.check_deployer:
        logger.info("Checking deployer...")
        check_deployer(deployer, logger)
    elif args.device_check:
        logger.info("Running device check...")
        device_check(deployer, logger)
    elif args.configure_db:
        logger.info("Configuring database...")
        configure_database(deployer, logger)
    elif args.download_artifacts:
        logger.info("Downloading artifacts...")
        download_artifacts(deployer, logger)
    # Commands below here requires target boards passed as a argument
    elif args.boards is None:
        # For these commands, ensure some number of boards provided.
        logger.error("Please provide one or more Talon boards # to target.")
        return
    elif args.generate_config:
        boards = list(map(int,(args.boards.split(','))))
        deployer.targetTalons = boards
        
        converted_fsp_mode_boardmap_config = []
        logger.info("Generating Config...")
        if args.fsp_modes is None:
            logger.debug("FSP mode(s) was not given for the given target board(s); " 
                         "Defaulting to CORR for all boards")
            converted_fsp_mode_boardmap_config = [FspMode.CORR for _ in boards]
        else:
            fsp_modes = args.fsp_modes.split(",")
            for i in range(len(fsp_modes)):
                fsp_mode = fsp_modes[i]
                if fsp_mode in FspMode.__members__:
                    converted_fsp_mode_boardmap_config.append(FspMode[fsp_mode])
                else:
                    logger.debug(f"A FSP Mode given for the board {boards[i]} in the boards list is not valid; " 
                                   "must be one of [CORR, PST]); "
                                   f"Defaulting to CORR for board {boards[i]}")
                    converted_fsp_mode_boardmap_config.append(FspMode.CORR)
        deployer.fspModeList = converted_fsp_mode_boardmap_config
                
        generate_config_jsons(deployer, logger)
    else:
        logger.info("Please provide a command.")
        return

if __name__ == "__main__":
    # Parse data from function call
    parser = argparse.ArgumentParser(description="Wrapper for running Deployer device commands.")
    parser.add_argument("--check-deployer", help="Check if the deployer device is exported the in TANGO Device db.", action="store_true")
    parser.add_argument("--generate-config", help="Generate configuration JSONs for the deployer and loads them to the device.", action="store_true")
    parser.add_argument("--download-artifacts", help="Downloads device artifacts from the CAR and deploys them into the TANGO database.", action="store_true")
    parser.add_argument("--configure-db", help="Generate JSON configs for the deployer.", action="store_true")
    parser.add_argument("--device-check", help="Generate JSON configs for the deployer.", action="store_true")
    parser.add_argument("--boards", help="Target Talon board to run the commands for.", type=str)
    parser.add_argument("--fsp-modes", help="Sets the FSP Mode configurations for the target Boards. A comma sperated list with one of [CORR, PST]", type=str)
    
    main(parser.parse_args())
