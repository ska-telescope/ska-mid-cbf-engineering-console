import argparse
import os
import logging
import json
import subprocess

log = logging.getLogger(__name__)

def copy_bite_binaries(scp_bins, run_bins, talon):

    ds_list_path = '../src/ska_mid_cbf_engineering_console/bite/bite_device_client/json/device_server_list.json'
    bins_path = '../mnt/talondx-config'
    tango_host = '192.168.1.7:10000'
    talon_bin_path = 'bin_nlamb'
    bins_folder = 'ds_bins'

    with open(ds_list_path) as f:
        bite_device_servers = json.load(f)
    
    try:
        os.makedirs(bins_folder)
    except FileExistsError as err:
        raise FileExistsError(f"Tried to create a {bins_folder} folder, but one already exists. User action suggested.") from err
    
    print(str(bite_device_servers.get("device_servers")))
    for ds in bite_device_servers.get("device_servers"):
        subprocess.run(["cp", f"{bins_path}/{ds}/bin/{ds}", bins_folder])
    
    subprocess.run(["tar", "-zcvf", f"{bins_folder}.tar.gz", bins_folder])
    subprocess.run(["rm", "-rf", bins_folder])

    if scp_bins:
        subprocess.run(["scp", f"{bins_folder}.tar.gz", f"root@{talon}:{talon_bin_path}"])
        sshProcess = subprocess.Popen([f"ssh", "-tt", f"root@{talon}"],
                                      stdin = subprocess.PIPE,
                                      stdout= subprocess.PIPE,
                                      universal_newlines=True,
                                      bufsize=0)
        sshProcess.stdin.write(f"tar -xf {talon_bin_path}/{bins_folder}.tar.gz --directory {talon_bin_path}\n")
        sshProcess.stdin.write(f"rm {talon_bin_path}/{bins_folder}.tar.gz\n")
        sshProcess.stdin.write("logout\n")

    if run_bins:
        sshProcess = subprocess.Popen([f"ssh", "-tt", f"root@{talon}"],
                                      stdin = subprocess.PIPE,
                                      stdout= subprocess.PIPE,
                                      universal_newlines=True,
                                      bufsize=0)
        sshProcess.stdin.write(f"export TANGO_HOST={tango_host}\n")
        sshProcess.stdin.write(f"cd {talon_bin_path}/{bins_folder}\n")
        sshProcess.stdin.write(f"for ds in *; do ./$ds {talon}_test -v2 & done;\n")
        # for ds in *; do ./$ds talon8_test -v2 & done;
        print("Finished running ds files")
        sshProcess.stdin.write("logout\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--scp', help='Copy (scp) binaries to talon', action='store_true')
    parser.add_argument('-r', '--run', help='Run binaries on talon', action='store_true')    
    parser.add_argument('-t', '--talon', help='Talon board (i.e. talon1, talon2, etc.)')
    args = parser.parse_args()

    # if args.scp or args.run:
    #     args.talon(required=True)
    #     args = parser.parse_args()

    copy_bite_binaries(args.scp, args.run, args.talon)

