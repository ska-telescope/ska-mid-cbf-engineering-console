#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the mid-cbf-prototype project
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

import tango


class SubscribeTest:
    def __init__(self, fqdn):
        self.event_id = -1
        self.device = tango.DeviceProxy(fqdn)

    def test_callback(self, argin):
        print(str(argin))

    def test_subscribe(self, attr):
        self.event_id = self.device.subscribe_event(
            attr,
            tango.EventType.CHANGE_EVENT,
            self.test_callback,
            stateless=True,
        )
        return self.event_id

    def test_unsubscribe(self, event_id):
        self.device.unsubscribe_event(event_id)
