#!/usr/bin/env python3
import argparse
import datetime
import json
import logging
import time

from astropy import units as astropy_units
from astropy.time import Time
from pytango_client_wrapper import PyTangoClientWrapper

EMULATOR_TRL = "ska_mid/tm_leaf_node/csp_subarray_01"
TIMEOUT_MILLISECONDS = 5000

LOG_FORMAT = (
    "[delay_models_publisher.py: line %(lineno)s]%(levelname)s: %(message)s"
)


class DelayModelsPublisher:
    """Class used to publish a delay model package."""

    def __init__(self):
        """
        Connects to the TMC emulator to publish delay models.
        """

        self._fqdn_tm = EMULATOR_TRL
        self.tm = PyTangoClientWrapper()
        self.tm.create_tango_client(self._fqdn_tm)
        self.tm.set_timeout_millis(TIMEOUT_MILLISECONDS)


if __name__ == "__main__":
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    logger_ = logging.getLogger("delay_models_publisher.py")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--publish-delay-models",
        help="Update delay details and publish delay models to TMC emulator",
        action="store_true",
    )
    parser.add_argument("--intermediate-dm-full-path", type=str)
    parser.add_argument("--publish-lead-time-seconds", type=float)
    args = parser.parse_args()
    if args.publish_delay_models:
        publisher = DelayModelsPublisher()

        with open(args.intermediate_dm_full_path, "r") as f:
            data = json.load(f)
            models = data["models"]

        logger_.info(
            f"{datetime.datetime.now()} publishing delay model(s) to TMC emulator..."
        )

        ska_epoch = "1999-12-31T23:59:28Z"
        ska_epoch_utc = Time(ska_epoch, scale="utc")
        ska_epoch_tai = ska_epoch_utc.unix_tai

        logger_.info(
            f"publish lead time seconds: {args.publish_lead_time_seconds}"
        )

        for n, obj in enumerate(models):
            # convert delay model start_validity_sec to Time
            dm_start_validity = obj["model"]["start_validity_sec"]
            dm_start_tai = Time(
                dm_start_validity + ska_epoch_tai,
                scale="utc",
                format="unix_tai",
            )
            logger_.info(
                f"the next delay model to publish (index {n}): {json.dumps(obj['model'])}"
            )

            now_utc = Time(
                datetime.datetime.now(datetime.timezone.utc), scale="utc"
            )

            dt = dm_start_tai.datetime - now_utc.datetime
            delta_sec = dt.total_seconds()
            if delta_sec > 0:
                logger_.debug(
                    f"difference in seconds between now and start_validity_sec: {delta_sec}"
                )

                time.sleep(delta_sec - args.publish_lead_time_seconds)

                publisher.tm.write_attribute(
                    "delayModel", json.dumps(obj["model"])
                )
                publish_time = dm_start_tai - (
                    args.publish_lead_time_seconds * astropy_units.second
                )
                logger_.info(
                    f"published delay model at {dm_start_validity - args.publish_lead_time_seconds} (SKA Epoch offset) or {publish_time.datetime} (UTC)"
                )
            else:
                # if the first delay model (dm1) has start_validity_sec in the past, publish it right away
                # publish subsequent models after (their start validity - dm1 start validity) seconds
                if n >= 1:
                    sleep_seconds = (
                        obj["model"]["start_validity_sec"]
                        - models[n - 1]["model"]["start_validity_sec"]
                    )
                    time.sleep(sleep_seconds)
                    now_utc = Time(
                        datetime.datetime.now(datetime.timezone.utc),
                        scale="utc",
                    )
                publisher.tm.write_attribute(
                    "delayModel", json.dumps(obj["model"])
                )
                logger_.info(f"published delay model at {now_utc} (UTC)")
        logger_.info(
            f"{datetime.datetime.now()} finished publishing delay models",
        )
