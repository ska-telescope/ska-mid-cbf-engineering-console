import datetime
import json

from tango import DeviceProxy


class FspIntegrationTools:
    def __init__(self, talon: int):
        self._fqdn_rdt = (
            f"talondx-00{talon}/resamplerdelaytracker/resampler_fsp_0"
        )
        self._rdt = DeviceProxy(self._fqdn_rdt)

        self._fqdn_fsp = f"talondx-00{talon}/fsp-app/fsp-controller"
        self._fsp = DeviceProxy(self._fqdn_fsp)

        self._fqdn_fsp_corr = f"talondx-00{talon}/fsp-app/fsp-corr-controller"
        self._fsp_corr = DeviceProxy(self._fqdn_fsp_corr)

        self._fqdn_host_lut_s1 = (
            f"talondx-00{talon}/dshostlutstage1/host_lut_s1"
        )
        self._host_lut_s1 = DeviceProxy(self._fqdn_host_lut_s1)

        self._fqdn_host_lut_s2 = (
            f"talondx-00{talon}/dshostlutstage2/host_lut_s2"
        )
        self._host_lut_s2 = DeviceProxy(self._fqdn_host_lut_s2)

        self.input_sample_rate = 220000000
        self.output_sample_rate = 220200960

    def set_function_mode(self, func):
        self._fsp.command_inout("SetFunctionMode", func)

    def configure_lut(self):
        program_arg = dict(
            {"Subarrays": [{"subarray_id": 0, "dest_host_data": []}]}
        )

        for i in range(0, 744):
            program_arg["Subarrays"][0]["dest_host_data"].append(
                dict(
                    {
                        "chan_id": i,
                        "ip_addr": 171049218,
                        "udp_port_num": 12000 + i,
                    }
                )
            )

        self._host_lut_s1.command_inout("Unprogram")
        self._host_lut_s2.command_inout("Unprogram")

        self._host_lut_s1.command_inout("connectToHostLUTStage2")

        self._host_lut_s2.write_attribute(
            "src_mac_addr_high", (146889004390 >> 32)
        )
        self._host_lut_s2.write_attribute(
            "src_mac_addr_low", (146889004390 & 0xFFFFFFFF)
        )
        self._host_lut_s2.write_attribute("src_ip_addr", (171049228))

        self._host_lut_s2.command_inout("Program", json.dumps(program_arg))
        self._host_lut_s1.command_inout("Program")

    def configure_lut_stage_1(self):
        print("Hello")

    def update_delay_models(self):
        delay_model = {"delay_model": []}
        hodm = {
            "receptor": 1,
            "epoch": float(
                datetime.datetime.utcnow()
                .replace(tzinfo=datetime.timezone.utc)
                .timestamp()
            ),
            "validity_period": float(60),
            "poly_info": [
                {"polarization": "X", "coeffs": [0, 0, 0, 0, 0, 0]},
                {"polarization": "Y", "coeffs": [0, 0, 0, 0, 0, 0]},
            ],
        }
        delay_model["delay_model"].append(hodm)

        self._fsp_corr.command_inout("On")
        self._fsp_corr.command_inout(
            "UpdateDelayModels", json.dumps(delay_model)
        )

    def configure_rdt(self):
        self._rdt.command_inout("stop_resampler_delay_tracker")

        self._rdt.write_attribute("dithering_seed_polX", 1)
        self._rdt.write_attribute("dithering_seed_polY", 2)
        self._rdt.write_attribute("first_input_timestamp_start_offset", 3.1)

        self._rdt.write_attribute(
            "ho_start_utc_time",
            float(
                datetime.datetime.utcnow()
                .replace(tzinfo=datetime.timezone.utc)
                .timestamp()
            ),
        )
        self._rdt.write_attribute("num_coeffs", 6)
        self._rdt.write_attribute("fo_validity_interval", 0.001)
        self._rdt.write_attribute("fo_start_time", 0)
        self._rdt.write_attribute("num_lsq_points", 10)
        self._rdt.write_attribute("fo_fill_threshold", 3000)
        self._rdt.write_attribute("fo_low_threshold", 512)

        self._rdt.command_inout(
            "set_input_sample_rate", self.input_sample_rate
        )
        self._rdt.command_inout(
            "set_output_sample_rate", self.output_sample_rate
        )

        dm = [0.0 for _ in range(8)]
        t_start = 0.0
        for i in range(32):
            t_start = i * 7.5
            dm[0] = float(t_start)
            dm[1] = float(t_start + 8)
            self._rdt.command_inout("push_hodm_polX", dm)
            self._rdt.command_inout("push_hodm_polY", dm)

        self._rdt.command_inout("start_resampler_delay_tracker")

    def configure_and_run_scan(self):
        configure_scan_arg = {
            "sub_id": 0,
            "fsp_id": 1,
            "function_mode": "CORR",
            "receptor_ids": [1],
            "corr_receptor_ids": [1],
            "frequency_slice_id": 1,
            "zoom_factor": 1,
            "zoom_window_tuning": 4700000,
            "integration_factor": 10,
            "channel_offset": 14880,
            "channel_averaging_map": [
                [0, 1],
                [744, 1],
                [1488, 1],
                [2232, 1],
                [2976, 1],
                [3720, 1],
                [4464, 1],
                [5208, 1],
                [5952, 1],
                [6696, 1],
                [7440, 1],
                [8184, 1],
                [8928, 1],
                [9672, 1],
                [10416, 1],
                [11160, 1],
                [11904, 1],
                [12648, 1],
                [13392, 1],
                [14136, 1],
            ],
            "output_link_map": [
                [0, 4],
                [744, 8],
                [1488, 12],
                [2232, 16],
                [2976, 20],
                [3720, 24],
                [4464, 28],
                [5206, 32],
                [5952, 36],
                [6696, 40],
                [7440, 44],
                [8184, 48],
                [8928, 52],
                [9672, 56],
                [10416, 60],
                [11160, 64],
                [11904, 68],
                [12648, 72],
                [13392, 76],
                [14136, 80],
            ],
            "output_host": [[0, "10.50.1.2"]],
            "output_mac": [[0, "b8:3f:d2:24:17:b9"]],
            "output_port": [[0, 12000, 1]],
            "config_id": "band:5a, fsp1, 744 channels average factor 8",
            "frequency_band": "1",
            "band_5_tuning": [5.85, 7.25],
            "frequency_band_offset_stream_1": 0,
            "frequency_band_offset_stream_2": 0,
        }

        internal_params = {
            "resampler_delay_tracker": {
                "fo_validity_interval": 0.001,
                "num_lsq_points": 10,
                "fo_fill_threshold": 3000,
                "fo_low_threshold": 512,
                "set_input_sample_rate": 220000000,
                "set_output_sample_rate": 220200960,
            },
            "fine_channelizer": {"gain": 1},
            "ddr4_corner_turner": {
                "min_integration_time_num_samps": 1904,
                "read_threshold": 3808,
                "read_timeout_threshold": 450000000,
            },
            "correlator": {"num_frequency_channel": 744},
        }

        hps_fsp_configuration = dict({"configure_scan": configure_scan_arg})
        hps_fsp_configuration.update(internal_params)

        self._fsp_corr.ConfigureScan(json.dumps(hps_fsp_configuration))
