FROM artefact.skao.int/ska-tango-images-pytango-builder:9.3.33 as buildenv
FROM artefact.skao.int/ska-tango-images-pytango-runtime:9.3.20

LABEL \
      author="Rob Huxtable <Robert.Huxtable@mda.space>" \
      description="Mid CBF Engineering Console."

# Since the docker build context is at the root project
# level so the build can access the project dependencies
# (pyproject.toml, poetry.lock)
# we need to set the default directory to be used at
# container launch time to avoid having to access
# scripts through the directories each time
WORKDIR /app/images/ska-mid-cbf-engineering-console-etc/

RUN mkdir -p /app/images/ska-mid-cbf-engineering-console-etc/artifacts
RUN mkdir -p /app/images/ska-mid-cbf-engineering-console-etc/bite-data
USER root

# Poetry virtual environment not needed in Docker image
RUN poetry config virtualenvs.create false

RUN pip install --upgrade pip

# Put the project dependency files where they can be found
COPY pyproject.toml poetry.lock /app/

# Get dependencies
RUN poetry export --format requirements.txt --output poetry-requirements.txt --without-hashes --without dev && \
    pip install -r poetry-requirements.txt && \
    rm poetry-requirements.txt

COPY /images/ska-mid-cbf-engineering-console-etc/conan_local/remotes.json /root/.conan/remotes.json
RUN conan remote list

CMD ./talondx.py

