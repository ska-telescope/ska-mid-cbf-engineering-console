from __future__ import annotations

import json
import logging
import math
import os
from typing import Any

import numpy as np
import tango
import tango_db_ops
from jsonschema import validate
from tango import DeviceProxy

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
DEVICE_SERVER_LIST_SCHEMA = os.path.join(
    THIS_DIR, "device_server_list_schema.json"
)
LOG_FORMAT = (
    "[BiteClient.%(funcName)s: line %(lineno)s]%(levelname)s: %(message)s"
)

# Constants
TARGET_SAMPLE_RATE = 220000000  # samples per second (at the FSP, post retimer, must be this for the 16k Channeliser)
VCC_FRAME_SIZE = 18  # Number of samples input to the VCC.
VCC_CHANNELS = 20  # Number of oversampled channels output by the VCC. Oversampling factor = VCC_CHANNELS/VCC_FRAME_SIZE.
LSTV_SAMPLES_PER_DDR_WORD = 21
LSTV_DDR_WORD_IN_BYTES = 64
BYTES_IN_GIGABYTES = 2**30  # 2**30 = 1024 x 1024 x 1024

# BITE shares memory with the long term accumulator; LTA at low addresses, BITE above.
# FIXME: Can't run at full speed because sharing DDR with LTA
# 1.0 = approximately real time as from DISH. 0.01 = Rate that the DELL server running UDP can keep up with.
PACKET_RATE_SCALE_FACTOR = 0.01


class BiteClient:
    """
    Class to configure the low level BITE devices
    """

    # Utility functions.

    def _ceil_pow2(self: BiteClient, val: int) -> int:
        return 2 ** math.ceil(math.log(val, 2))

    def __init__(self: BiteClient, inst: str, log_to_UML: bool) -> None:
        """
        Initialize BITE parameters

        :param inst: name of Talon HPS DS server instance
        :type inst: str
        :param log_to_UML: set to True to print BITE output to UML format
        :type log_to_UML: bool
        """
        logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
        self._log_to_UML = log_to_UML
        self._logger = logging.getLogger("BiteClient")
        self._server_inst = inst
        self._dp_dict = {}
        self._device_servers = None
        self._receiver_selector = int(0)
        self._source_selector = int(0)
        self._tone_selector = int(0)
        self._filter_energy_rcv_X = 0
        self._filter_energy_rcv_Y = 0
        self._rx_gen = {}
        self._src_gen = []
        self._rfi = []
        self._fir_filter_gen_out_rcv_file_name = ""
        self._lstv_start_word_addr = 0
        self._lstv_end_word_addr = 0
        self._low_address_allocation = 0
        self._high_address_allocation = 0

        self._sample_rate = TARGET_SAMPLE_RATE * VCC_FRAME_SIZE
        src_index = dict(sstv=0, ramp=1, lstv=2)
        self._src_select = src_index[os.getenv("BITE_DATA_SRC")]
        self._logger.info(f"Source selected: {self._src_select}")

    def init_devices(self: BiteClient, input_file_name: str) -> None:
        """
        Initialize the device servers from the JSON file

        :param input_file_name: the name of the JSON file containing
            the device server data
        """
        self._input_file_name = input_file_name
        self._device_servers = json.load(open(input_file_name))
        with open(DEVICE_SERVER_LIST_SCHEMA, "r") as schema_fd:
            bite_client_schema = json.load(schema_fd)
        validate(instance=self._device_servers, schema=bite_client_schema)
        for name in self._device_servers:
            self._create_dps_for_server(name)

    def write_talon_status(
        self: BiteClient,
        status_attr_filename: str,
        talondx_status_output_dir: str,
    ) -> None:
        """
        Print Talon Status
        """
        self._logger.info("Entering ...")
        if not self._device_servers:
            self._logger.error("init_devices not called")
            return
        try:
            self._create_dps_for_server("ska-talondx-status-ds")
            status_attr_file = open(status_attr_filename)
            status_attr_json = json.load(status_attr_file)
            status_dict = {}
            for attr in status_attr_json:
                attr_status = (
                    self._dp_dict["status"].read_attribute(attr).value
                )
                if type(attr_status) is np.ndarray:
                    attr_status = attr_status.tolist()
                status_dict[attr] = attr_status
            out_status_filename = os.path.join(
                talondx_status_output_dir,
                self._server_inst + "_talon_status.json",
            )
            out_status_file = open(out_status_filename, "w")
            json.dump(status_dict, out_status_file, indent=4)
            out_status_file.close()
        except Exception as e:
            self._logger.error(f"{str(e)}")

    def _select_input(
        self: BiteClient, jsonSelector: int, n: int, selector: int
    ) -> int:
        if jsonSelector == 1:
            return selector | 0x1 << int(n)
        return selector

    def _command_read_write(
        self: BiteClient,
        dp: tango.DeviceProxy,
        command_name: str,
        input_args=None,
    ) -> None:
        """
        Wrapper function that calls a tango command

        :param dp: the device proxy whose command will be called
        :param command_name: the command name
        :param input_args: the command input arguments (None if the command has no arguments)
        """
        try:
            if self._log_to_UML:
                print(
                    f"BiteClient -> {dp.dev_name()} ** : CMD {command_name}({input_args})"
                )
            else:
                self._logger.info(
                    f"command_read_write({dp.dev_name()}, {command_name}, {input_args})"
                )
            return dp.command_inout(command_name, input_args)
        except Exception as e:
            self._logger.error(str(e))

    def _write_attribute(
        self: BiteClient, dp: tango.DeviceProxy, attr_name: str, attr_val: Any
    ) -> None:
        """
        Wrapper function that writes to a tango device attribute

        :param dp: the device proxy whose attribute we will be writen
        :param attr_name: the name of the attribute to be written
        :param attr_val: the value to be written to the attribute
        """
        try:
            if self._log_to_UML:
                print(
                    f"BiteClient -> {dp.dev_name()} ** : {attr_name} = {attr_val})"
                )
            else:
                self._logger.info(
                    f"write_attribute({dp.dev_name()}, {attr_name}, {attr_val})"
                )
            return dp.write_attribute(attr_name, attr_val)
        except Exception as e:
            self._logger.error(str(e))

    def _create_dps_for_server(self: BiteClient, serverName: str) -> None:
        """
        Get a list of device names from the specified device server and create proxies
            for each device name

        :param serverName: the name of the device server
        """

        server_full_name = serverName + "/" + self._server_inst

        db_dev_list = tango_db_ops.get_existing_devs(server_full_name)

        if db_dev_list is not None:
            for db_dev in db_dev_list:
                if self._create_deviceProxy(db_dev):
                    self._logger.info(
                        "Created device proxy for {}".format(db_dev)
                    )
                else:
                    self._logger.error(
                        "Error on creating device proxy for  {}".format(db_dev)
                    )
        else:
            self._logger.error(
                "The server {} is not running".format(serverName)
            )

    def _create_deviceProxy(self: BiteClient, deviceName: str) -> bool:
        """
        Create a device proxy using the device name

        :param deviceName: the name of the device
        """
        try:
            key = deviceName.split("/")[-1]
            self._logger.info(
                "Creating device server for {}".format(deviceName)
            )
            self._dp_dict[key] = DeviceProxy(deviceName)
        except tango.DevFailed as df:
            for item in df.args:
                self._logger.error(
                    f"Failed to create proxy for {deviceName} : {item.reason} {item.desc}"
                )
            return False
        return True
