#!/usr/bin/env python3
import argparse
import datetime
import json
import logging
import os
import os.path
from pathlib import Path

from astropy.time import Time

DEFAULT_DM_UPDATE_BUFFER_SECONDS = 5
DEFAULT_PUBLISH_LEAD_TIME_SECONDS = 5

DM_DIR = "/app/images/ska-mid-cbf-engineering-console-etc/delay-models"

LOG_FORMAT = "[DelayModelsFormatter.%(funcName)s: line %(lineno)s]%(levelname)s: %(message)s"


class DelayModelsFormatter:
    """Class used to generate a delay model package with valid epochs."""

    def __init__(
        self,
        overwrite_start_validity: bool,
        dm_update_buffer_seconds: float,
        publish_lead_time_seconds: float,
        dm_template_full_path: str = "",
        intermediate_dm_full_path: str = "",
        use_dms_from_tool: bool = False,
        drop_models: list = None,
    ):
        """
        Class that generates delay model objects, in the required format, from
        given inputs, for both test vectors simulation and signal processing.

        :param dm_template_full_path: path to delay models file to be formatted
        :param intermediate_dm_full_path: path to delay models file that will be created
            by the formatter, which is ready to be published.
        :param use_dms_from_tool: not yet supported
        :drop_models: list of indexes for delay models that will be deleted from the
            package (1-based). Deleting delay models will create gaps in the time range
            covered by the delay model package file
        :param dm_update_buffer_seconds: time in seconds added to the start_validity_sec
            of the first delay model to account for the time it takes to update delay models.
        :param publish_lead_time_seconds: time in seconds added to the start_validity_sec
            of the first delay model. Delay models will be published these many seconds
            before their start_validity_sec.
        :param overwrite_start_validity: if True, the delay models' start_validity_sec
            will be overwritten. For the first delay model, start_validity_sec will be
            the current time + dm_update_buffer_seconds + publish_lead_time_seconds.
            For subsequent delay models, start_validity_sec will be the start_validity_sec
            of the previous delay model + the cadence_sec of the previous delay model
        """
        logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
        self._logger = logging.getLogger("DelayModelsFormatter")

        # TODO: add an output_dir_path input so the location is not fixed
        # TODO: instead of hard-coded DMs, we will later use a DM generator tool
        self._use_dms_from_tool = use_dms_from_tool
        if self._use_dms_from_tool:
            raise NotImplementedError("TODO - not yet supported")

        # TODO: remove when use_dms_from_tool is True
        self._dm_template_full_path = dm_template_full_path

        self._intermediate_dm_full_path = intermediate_dm_full_path

        if self._use_dms_from_tool is False:
            if self._dm_template_full_path:
                self._logger.info(
                    f"Extracting delay models from {self._dm_template_full_path}"
                )
                models = self._get_delay_models_from_file(
                    self._dm_template_full_path
                )
            else:
                models = self._get_default_delay_model()

            self._formatted_delay_models = {"models": models}

            if overwrite_start_validity:
                self._update_delay_model_epochs(
                    dm_update_buffer_seconds, publish_lead_time_seconds
                )
            if drop_models:
                self._logger.info(
                    f"Dropping delay models with the following 1-based indexes: {drop_models}"
                )
                models = self._formatted_delay_models["models"]

                for index, model in enumerate(models.copy()):
                    if (index + 1) in drop_models:
                        models.remove(model)

                self._formatted_delay_models["models"] = models
            self._write_dm_package_to_file()

    def _update_delay_model_epochs(
        self, update_buffer_seconds: float, publish_lead_time_seconds: float
    ) -> None:
        """Update start_validity_sec for all delay models"""
        models = self._formatted_delay_models["models"]
        # populate the first delay model's start_validity_sec
        models[0]["model"]["start_validity_sec"] = (
            self._get_epoch_from_current_time()
            + update_buffer_seconds
            + publish_lead_time_seconds
        )
        self._logger.info(
            f"Overwriting start_validity_sec with {models[0]['model']['start_validity_sec']} for delay model at index 0"
        )
        previous_time = models[0]["model"]["start_validity_sec"]

        # starting from the second delay model:
        # start_validity_sec for model n =  start_validity_sec for model (n-1) + cadence_sec for model (n-1)
        for n, obj in enumerate(models[1:]):
            time_sec = previous_time + models[n - 1]["model"]["cadence_sec"]
            obj["model"]["start_validity_sec"] = time_sec
            self._logger.info(
                f"Overwriting start_validity_sec with {time_sec} for delay model at index {n + 1}"
            )
            previous_time = time_sec
        self._formatted_delay_models["models"] = models

    def _write_dm_package_to_file(self) -> None:
        path = os.path.dirname(self._intermediate_dm_full_path)
        Path(path).mkdir(mode=0o777, parents=True, exist_ok=True)
        with open(self._intermediate_dm_full_path, "w") as f:
            json.dump(self._formatted_delay_models, f, indent=4)

    def _get_delay_models_from_file(self, full_path: str) -> list:
        with open(full_path, "r") as f:
            data = json.load(f)
        models = data["models"]
        return models

    def _get_epoch_from_current_time(self) -> datetime.datetime:
        ska_epoch = "1999-12-31T23:59:28Z"

        ska_epoch_utc = Time(ska_epoch, scale="utc")
        ska_epoch_tai = ska_epoch_utc.unix_tai
        self._logger.debug(f"ska_epoch_utc = {ska_epoch_utc}")
        self._logger.debug(f"ska_epoch_tai = {ska_epoch_tai}")

        start_utc = Time(
            datetime.datetime.now(datetime.timezone.utc),
            scale="utc",
        )
        start_tai = start_utc.unix_tai
        self._logger.debug(f"start_utc = {start_utc}")
        self._logger.debug(f"start_tai = {start_tai}")

        # Calculate start_utc_time_offset relative to SKA epoch date
        start_utc_time_offset = start_tai - ska_epoch_tai
        self._logger.debug(
            "start_utc_time_offset = start_tai - ska_epoch_tai ="
            f"{start_utc_time_offset}"
        )
        self._logger.debug(f"start_utc datetime {start_utc.datetime}")
        return start_utc_time_offset

    def _get_default_delay_model(self) -> list:
        formatter_path = os.path.dirname(os.path.realpath(__file__))
        self._logger.debug(f"formatter path {formatter_path}")
        draft_path = os.path.join(
            formatter_path, "delay_models/input/dm_package_draft.json"
        )
        models = self._get_delay_models_from_file(draft_path)
        return [models[0]]


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--update-delay-models",
        help="Generate delay models and update their start_validity_sec fields",
        action="store_true",
    )
    parser.add_argument(
        "--dm-template-full-path",
        help="Delay models package template (JSON file to be formatted)",
    )
    parser.add_argument(
        "--overwrite-start-validity-sec",
        help="whether to overwrite the delay models' start_validity_sec",
        default=False,
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        "--dm-update-buffer-seconds",
        help="seconds added to the start_validity_sec of the first delay model to account for the time it takes to update delay models",
        default=DEFAULT_DM_UPDATE_BUFFER_SECONDS,
        type=float,
    )
    parser.add_argument(
        "--publish-lead-time-seconds",
        help="Each delay model will be published this many seconds before the delay model's start_validity_sec",
        default=DEFAULT_PUBLISH_LEAD_TIME_SECONDS,
        type=float,
    )
    parser.add_argument(
        "--intermediate-dm-full-path",
        help="path to delay models file that will be created by the formatter, which is ready to be published",
        type=str,
    )
    parser.add_argument(
        "--drop-models",
        nargs="+",
        type=int,
        help="space-separated indexes (1-based) for delay models to drop",
    )
    args = parser.parse_args()
    if args.update_delay_models:
        f = DelayModelsFormatter(
            dm_template_full_path=args.dm_template_full_path,
            intermediate_dm_full_path=args.intermediate_dm_full_path,
            drop_models=args.drop_models,
            dm_update_buffer_seconds=args.dm_update_buffer_seconds,
            publish_lead_time_seconds=args.publish_lead_time_seconds,
            overwrite_start_validity=args.overwrite_start_validity_sec,
        )
