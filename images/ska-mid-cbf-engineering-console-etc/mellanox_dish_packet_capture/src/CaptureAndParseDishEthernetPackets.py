#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This s/w was commissioned by and written for the Canadian NRC, DRAO
# This s/w is part of the test and verification tools for the TDC project
#
# See the COPYRIGHT file at the top-level directory of this distribution
# for details of code ownership.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
"""
This module contains class CaptureAndParseDishEthernetPackets
The class captures DISH Mode 2 packets from a 100GE Ethernet interface device and
parses/extracts meta and sample data from the encoded packets.
"""

import logging as log

# Standard imports
import socket
import sys

import numpy as np

# -----------------
# Class Constants
# -----------------
ETH_P_ALL = 3
""" To replace from scapy.all import ETH_P_ALL """
DISH_PACKET_SIZE_IN_BYTES = 8000
""" Number of bytes in a raw DISH Ethernet packet """
NUMBER_OF_FRAMES_IN_PACKET = 125
""" Number of frames in a raw DISH Ethernet packet """
NUMBER_OF_META_FRAMES_IN_PACKET = 1
""" Number of frames in a raw DISH Ethernet packet """
NUMBER_OF_SAMPLE_FRAMES_IN_PACKET = 124
""" Number of frames in a raw DISH Ethernet packet """
NUMBER_OF_BYTES_IN_FRAME = 64
""" Number of bytes in the frame of a packet """
NUMBER_OF_SAMPLES_IN_SIGNAL_FRAME = 21
""" Number of X and Y signal samples in packet frame """
NUMBER_OF_SAMPLES_IN_PACKET = (
    NUMBER_OF_FRAMES_IN_PACKET - 1
) * NUMBER_OF_SAMPLES_IN_SIGNAL_FRAME
""" Number of X and Y signal samples in complete packet """

# Structure type to be used to store meta data fields parsed from packet metadata frames
metaFrameType = np.dtype(
    [
        ("destination_mac_address", "uint64"),  # field 01
        ("source_mac_address", "uint64"),  # field 02
        ("ethernet_type", "uint16"),  # field 03
        ("dish_id", "uint16"),  # field 04
        ("band_id", "uint8"),  # field 05
        ("unassigned_1", "uint64"),  # field 06
        ("packet_counter_from_1pps", "uint32"),  # field 07
        ("number_of_time_samples_in_packet", "uint16"),  # field 08
        ("unassigned_2", "uint8"),  # field 09
        ("number_of_frames", "uint8"),  # field 10
        ("transport_sample_rate", "uint64"),  # field 11
        ("noise_diode_states", "uint8"),  # field 12
        ("noise_diode_change_frame", "uint8"),  # field 13
        ("utc_time", "uint32"),  # field 14
        ("noise_diode_transition_holdoff_count", "uint16"),  # field 15
        ("unassigned_3", "uint16"),  # field 16
        ("number_of_time_samples_from_1pps", "uint64"),  # field 17
        ("unassigned_4", "uint16"),  # field 18
        ("hardware_source_id", "uint64"),  # field 19
    ]
)

# Define structure type to be used to store parsed X/Y sample data parsed from packet sample data frames
packet_frame_sample_type = np.dtype(
    [("x", "int16"), ("y", "int16")]
)  # Horizontally and Vertically polarised samples


class CaptureAndParseDishEthernetPackets:
    """Summary of CaptureAndParseDishEthernetPackets

    This class is used to capture DISH Ethernet packets from a 100GE I/F device
    and to extract meta and sample data from the encoded packets

    Methods
    ------
    connectToDishInterfaceDevice    : connects to the DISH 100G interface
    readPackets                     : reads Ethernet packets from the DISH interface
    parsePackets                    : extracts meta sand sample data from the encoded packets
    """

    # --------------------------
    # Properties and Attributes
    # --------------------------

    # Structure of the Ethernet packet meta frame, with field name, field offset and field size in bytes
    metaFrameFormat = np.array(
        [
            ("destination_mac_address", 0, 6),  # field 01
            ("source_mac_address", 6, 6),  # field 02
            ("ethernet_type", 12, 2),  # field 03
            ("dish_id", 14, 2),  # field 04
            ("band_id", 16, 1),  # field 05
            ("unassigned_1", 17, 7),  # field 06
            ("packet_counter_from_1pps", 24, 4),  # field 07
            ("number_of_time_samples_in_packet", 28, 2),  # field 08
            ("unassigned_2", 30, 1),  # field 09
            ("number_of_frames", 31, 1),  # field 10
            ("transport_sample_rate", 32, 6),  # field 11
            ("noise_diode_states", 38, 1),  # field 12
            ("noise_diode_change_frame", 39, 1),  # field 13
            ("utc_time", 40, 4),  # field 14
            ("noise_diode_transition_holdoff_count", 44, 2),  # field 15
            ("unassigned_3", 46, 2),  # field 16
            ("number_of_time_samples_from_1pps", 48, 6),  # field 17
            ("unassigned_4", 54, 2),  # field 18
            ("hardware_source_id", 56, 8),  # field 19
        ],
        dtype=[
            ("field_name", "U40"),
            ("field_offset", "uint16"),
            ("field_size", "uint16"),
        ],
    )

    def __init__(self):
        pass

    def __del__(self):
        pass

    def readPacketsFromSocket(
        self,
        packets_to_capture,
        ethernet_device,
        output_file_name,
        first_packet_to_capture=None,
    ):
        """Method captures DISH Ethernet packets from the 100GE interface socket connection

        Input Parameters:
        packets_to_capture      : number of consecutive packets to be captured

        Return:
        dish_packet_buffer      : array of bytes for each captured packet
        packets_captured        : actual number of packets captured
        """
        """ Clear out all received packet events"""
        self.dish_socket = socket.socket(
            socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(ETH_P_ALL)
        )
        self.dish_socket.setsockopt(
            socket.SOL_SOCKET, socket.SO_RCVBUF, 2**28
        )
        self.dish_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.dish_socket.bind((ethernet_device, 0))

        """ Setup buffer, poll for and capture N raw Ethernet packets """
        dish_packet_buffer = np.zeros(
            (packets_to_capture, DISH_PACKET_SIZE_IN_BYTES), dtype=np.uint8
        )
        for i in range(5):
            receive_buffer = self.dish_socket.recv(
                DISH_PACKET_SIZE_IN_BYTES
            )  # flush receive buffer

        packets_captured = 0
        metaFrame1ppsIdx = self.returnIndexPacketCounterFrom1pps()

        while packets_captured < packets_to_capture:
            receive_buffer = self.dish_socket.recv(DISH_PACKET_SIZE_IN_BYTES)
            # if things have been correctly read in, assign the data and perform any checks
            if (
                receive_buffer
                and len(receive_buffer) == DISH_PACKET_SIZE_IN_BYTES
            ):
                dish_packet_buffer[packets_captured] = np.frombuffer(
                    receive_buffer, dtype=np.uint8
                )
                # if it's the very first packet to be captured and first packet to capture specified, check it
                if (
                    packets_captured == 0
                    and first_packet_to_capture is not None
                ):
                    packet_counter = self.returnPacketCounterFrom1pps(
                        dish_packet_buffer[packets_captured], metaFrame1ppsIdx
                    )
                    if (
                        packet_counter < first_packet_to_capture
                    ):  # need to wait
                        # WARNING: adding debug print statements in here will cause program to lag behind
                        # socket and occasionally skip chunks of packets
                        continue
                    if (
                        packet_counter > first_packet_to_capture
                    ):  # we've gone past the packet we wanted
                        print(
                            "\nERROR: current packet counter",
                            packet_counter,
                            "is greater than ",
                            "the first packet specified to capture ",
                            first_packet_to_capture,
                        )
                        print(
                            "Please restart lstv replay and quickly start analysis tool to capture earlier packet."
                        )
                        print(
                            "Consider increasing Slow Down Factor to catch earlier packets in time."
                        )
                        print("Exiting analysis tool \n")
                        sys.exit(2)
                if (
                    packets_captured == 0
                ):  # if we're here we've made it through the checks, initial print.
                    print("Capturing packets starting with packet number ")
                    print(
                        self.returnPacketCounterFrom1pps(
                            dish_packet_buffer[packets_captured],
                            metaFrame1ppsIdx,
                        )
                    )
                packets_captured += 1
            else:
                print(
                    "Data capture error or ran out of data and restarting replay"
                )
                print(
                    "If error persists with every run, check first_packet_to_capture",
                    "not too high (cannot be more than total packets generated)",
                )
                break
        self.dish_socket.close()

        """ Write to output file if selected """
        if output_file_name:
            packet_data_file = open(output_file_name, "wb")
            packet_data_file.write(dish_packet_buffer)
            packet_data_file.close()

        return (dish_packet_buffer, packets_captured)

    def readPacketsFromFile(
        self, packets_to_capture, input_file_name, packet_offset
    ):
        """Method captures DISH Ethernet packets from a binary file

        Input Parameters:
        packets_to_capture      : number of consecutive packets to be captured
        input_file_name         : complete file name to read packet from
        packet_offset           : number of packets offset into file

        Return:
        dish_packet_buffer      : array of bytes for each captured packet
        packets_captured        : actual number of packets captured
        """
        packet_data_file = open(input_file_name, "rb")
        if packet_offset > 0:
            packet_data_file.read(packet_offset * DISH_PACKET_SIZE_IN_BYTES)
        metaFrame1ppsIdx = self.returnIndexPacketCounterFrom1pps()

        """ Create buffer for packets to be returned """
        dish_packet_buffer = np.zeros(
            (packets_to_capture, DISH_PACKET_SIZE_IN_BYTES), dtype=np.uint8
        )
        for i in range(packets_to_capture):
            receive_buffer = packet_data_file.read(DISH_PACKET_SIZE_IN_BYTES)
            dish_packet_buffer[i] = np.frombuffer(
                receive_buffer, dtype=np.uint8
            )
            # check what first packet counter is, print for user
            if i == 0:
                print("First packet in file:")
                print(
                    self.returnPacketCounterFrom1pps(
                        dish_packet_buffer[i], metaFrame1ppsIdx
                    )
                )
        packets_captured = packets_to_capture
        return (dish_packet_buffer, packets_captured)

    def returnIndexPacketCounterFrom1pps(self):
        for i in range(len(self.metaFrameFormat)):
            if self.metaFrameFormat[i][0] == "packet_counter_from_1pps":
                return i
        print(
            "ERROR: packet_counter_from_1pps field not found in metaFrameFormat"
        )
        sys.exit()

    def returnPacketCounterFrom1pps(self, packet_data, idx_packet_counter=6):
        meta_frame = packet_data[0:NUMBER_OF_BYTES_IN_FRAME]

        field = meta_frame[
            (self.metaFrameFormat[idx_packet_counter][1]) : (  # noqa: E203
                self.metaFrameFormat[idx_packet_counter][1]
                + self.metaFrameFormat[idx_packet_counter][2]
            )
        ]
        packet_counter = int.from_bytes(field, byteorder="big", signed=False)
        return packet_counter

    def parsePackets(self, packet_data, packets_to_parse):
        """Method parses N encoded Ethernet packet into meta and sample data structures

        Input Parameters:
        packet_data             : array of packets
        packets_to_parse        : number of packets contained in array packet_data

        Return:
        meta_data               : array containing the parsed meta data for each packet
        sample_data             : array containing the parsed X/Y sample data for each packet
        """
        packet_number = 0
        meta_data = np.zeros(packets_to_parse, dtype=metaFrameType)
        sample_data = np.zeros(
            (packets_to_parse, NUMBER_OF_SAMPLES_IN_PACKET),
            dtype=packet_frame_sample_type,
        )
        while packet_number < packets_to_parse:
            """Parse meta data frame (1)"""
            meta_frame = packet_data[packet_number][0:NUMBER_OF_BYTES_IN_FRAME]
            for i in range(np.size(self.metaFrameFormat, 0)):
                field = meta_frame[
                    (self.metaFrameFormat[i][1]) : (  # noqa: E203
                        self.metaFrameFormat[i][1] + self.metaFrameFormat[i][2]
                    )
                ]
                meta_data[packet_number][i] = int.from_bytes(
                    field, byteorder="big", signed=False
                )
            """ Parse sample data frames (124) """
            byte_offset = 0
            samples_frame = packet_data[packet_number][
                NUMBER_OF_BYTES_IN_FRAME:DISH_PACKET_SIZE_IN_BYTES
            ]
            for frame_number in range(1, NUMBER_OF_FRAMES_IN_PACKET):
                for frame_sample in range(NUMBER_OF_SAMPLES_IN_SIGNAL_FRAME):
                    packet_sample_number = (
                        frame_number - 1
                    ) * NUMBER_OF_SAMPLES_IN_SIGNAL_FRAME + frame_sample
                    b1 = samples_frame[byte_offset]
                    b2 = samples_frame[byte_offset + 1] & 0x0F
                    x = int.from_bytes(
                        [b1, b2], byteorder="little", signed=False
                    )
                    if x & 0x800:  # if negative value sample
                        x = (
                            x | 0xF000
                        )  # extend 12 bit negative number to 16 bits
                    byte_offset += 1
                    sample_data[packet_number][packet_sample_number][
                        "x"
                    ] = np.int16(x)
                    b1 = samples_frame[byte_offset] & 0xF0
                    b2 = samples_frame[byte_offset + 1]
                    y = (
                        int.from_bytes(
                            [b1, b2], byteorder="little", signed=False
                        )
                        >> 4
                    )
                    if y & 0x800:  # if negative value sample
                        y = (
                            y | 0xF000
                        )  # extend 12 bit negative number to 16 bits
                    byte_offset += 2
                    sample_data[packet_number][packet_sample_number][
                        "y"
                    ] = np.int16(y)
                samples_frame[byte_offset]
                byte_offset += 1
            packet_number += 1
        return meta_data, sample_data

    def checkMetaData(self, meta_data, packets_captured):
        """Method checks the consistency of DISH Ethernet packet meta data

        Input Parameters:
        meta_data               : complete file name to read packet from
        packets_captured        : number of valid meta data records

        Return:
        number_of_errors        : number of meta data errors detected
        """
        message_string = "checkMetaData: First packet meta fields ..."
        log.info(message_string)
        number_of_errors = 0
        previous_number_of_errors = 0
        for i in range(packets_captured):
            for j in range(len(meta_data[0])):
                if i > 0:
                    if meta_data[i][j] != meta_data[0][j]:
                        if j == 6:  # field "packet_counter_from_1pps"
                            delta = meta_data[i][j] - meta_data[i - 1][j]
                            if delta != 1:
                                number_of_errors += 1
                        elif (
                            j == 16
                        ):  # field "number_of_time_samples_from_1pps"
                            delta = meta_data[i][j] - meta_data[i - 1][j]
                            if delta != meta_data[i][7]:
                                number_of_errors += 1
                        else:
                            number_of_errors += 1
                    if number_of_errors != previous_number_of_errors:
                        previous_number_of_errors = number_of_errors
                        log.info(
                            "checkMetaData: error in packet {:} field {:} value {:}".format(
                                i, self.metaFrameFormat[j][0], meta_data[i][j]
                            )
                        )
                else:  # i = 0, fields of first meta frame
                    message_string = "checkMetaData: {:<40}  = 0x{:>012X}"
                    log.info(
                        message_string.format(
                            self.metaFrameFormat[j][0], meta_data[i][j]
                        )
                    )
        return number_of_errors
