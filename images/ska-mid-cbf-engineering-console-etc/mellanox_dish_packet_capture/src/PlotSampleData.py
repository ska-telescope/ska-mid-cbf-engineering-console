#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This s/w was commissioned by and written for the Canadian NRC, DRAO
# This s/w is part of the test and verification tools for the TDC project
#
# See the COPYRIGHT file at the top-level directory of this distribution
# for details of code ownership.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
"""
This program captures DISH Band 2 Ethernet packets from a Mellanox X-5 100GE Ethernet network card.
The program parses meta and sample data from the packet. The program then plots the sample X/Y data
and a histogram of the sample data.
"""
# Standard imports
import matplotlib

matplotlib.use("Agg")

import argparse
import json
import logging
import os
import sys

import CaptureAndParseDishEthernetPackets as cpdep
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

LOG_FORMAT = "[PlotSampleData.py: line %(lineno)s]%(levelname)s: %(message)s"

# import scipy
# from scipy.signal import kaiserord, lfilter, firwin, freqz

# Imports

MLX_100G_ETHERNET_DEV = "enp179s0f1"  # MELLANOX_X-5 100 G Ethernet Interface
""" Mellanox X-5 100GE Ethernet interface device device name (obtained using Linux command ifconfig) """

CAPTURE_DIR = os.environ.get("CAPTURE_DIR")
NETWORK_DEVICE = os.environ.get("BITE_IFACE_NAME")


def capture_and_parse_packet_data(capture_params):
    """Captures consecutive raw DISH Ethernet packets from the 100GE interface card
    Parses the encoded DISH packets into meta data and sample data fields

    Input Parameters:
    capture_params              : dict containing the following keys
        capture_from_device     : true/false whether capturing from device
        network_device          : device to capture packets from
        num_packets_to_capture  : number of consecutive packets to capture and parse
        output_filename         : file name for captured packets read from socket
        packet_data_filename    : file name of file to read packets from
        packet_offset           : offset packet number into input file
        capture_file            : file name to save test packets to

    Return:
    meta_data               : array containing the parsed meta data for each packet
    sample_data             : array containing the parsed X/Y sample data for each packet
    packets_captured        : actual number of packets captured
    """
    packets_to_process = capture_params["num_packets_to_capture"]
    """ Create instance of the packet reader """
    packet_processor = cpdep.CaptureAndParseDishEthernetPackets()
    if capture_params["capture_from_device"]:
        logger_.info(
            "Reading packets from socket {:}  ...".format(
                capture_params["network_device"]
            )
        )
        (
            dish_packets,
            packets_captured,
        ) = packet_processor.readPacketsFromSocket(
            packets_to_process,
            NETWORK_DEVICE,
            capture_params["capture_file"],
            capture_params["first_packet_counter_to_capture"],
        )
    else:
        logger_.info(
            "\nReading packets from file {:}  ...".format(
                capture_params["packet_data_filename"]
            )
        )
        (
            dish_packets,
            packets_captured,
        ) = packet_processor.readPacketsFromFile(
            packets_to_process,
            capture_params["packet_data_filename"],
            capture_params["packet_offset"],
        )
    if dish_packets is None:
        logger_.info(
            "capture_and_parse_packet_data: Error, couldn't capture requested packets"
        )
        return

    logger_.info("Number of packets to parse: {:}".format(packets_to_process))
    logger_.info("Parsing packets ...")
    (meta_data, sample_data) = packet_processor.parsePackets(
        dish_packets, packets_captured
    )

    if meta_data is None or sample_data is None:
        logger_.info(
            "capture_and_parse_packet_data: Error, couldn't parse requested packets"
        )
        return

    logger_.info("Checking meta frame data consistency ...")
    meta_frame_errors_detected = packet_processor.checkMetaData(
        meta_data, packets_captured
    )
    if meta_frame_errors_detected:
        logger_.info("Meta frame field errors detected, review log file")
        logger_.info(
            "capture_and_parse_packet_data: Meta frame field errors detected = {:}".format(
                meta_frame_errors_detected
            )
        )

    logger_.info("capture_and_parse_packet_data: Parsed and checked packets")
    return (meta_data, sample_data, packets_captured)


def plot_xypol_correlation(
    x_samples, y_samples, sample_rate, xypol_corr_iters, result_params
):
    """Plots the correlation of the x and y polarization data showing the spectral density
    Uses different integration time, uncorrelated receiver noise dies off more with longer time
    Input Parameters:
    x_samples              : one dimensional array of X samples (length = N packets x samples in packet)
    y_samples              : one dimensional array of Y samples (length = N packets x samples in packet)
    sample_rate            : sample rate
    result_params         : dictionary filled with parameters for the analysis process, including...
        save_results       : whether results should be saved
        output_dir         : the output results directory
        show_live_plot     : whether results should be plotted live
    xypol_corr_iters       : iterations to average xypol correlation over for plotting (integration length)
    Return:
    """
    save_results = result_params["save_results"]
    results_dir = CAPTURE_DIR
    show_live_plot = result_params["show_live_plots"]

    # normalization used before doing STFT. to match with other spectrum plot.
    # TODO: change to be based on input number of bits
    full_scale = 2**11

    # fix FFT size, lower N if necessary
    fft_size = 2048
    Nmax = int(len(x_samples) / fft_size)  # maximum allowable iterations
    logger_.info(
        f"Maximum iterations for corr plotting with {len(x_samples)}, is {Nmax}"
    )
    if Nmax > max(xypol_corr_iters):
        Nmax = max(xypol_corr_iters)  # decrease max to max that you need
    else:  # decrease those above allowed max to allowed max
        xypol_corr_iters[xypol_corr_iters > Nmax] = Nmax
        logger_.info(
            "Warning: desired iterations too high, reducing to maximum allowable"
        )
    xypol_corr_iters = np.unique(
        xypol_corr_iters
    )  # sort and remove duplicates (from multiple above Nmax)
    logger_.info(f"New iteration list for corr plotting is {xypol_corr_iters}")

    # cut sample stream length to maximum multiple (Nmax) of the fft_size
    x_samples_cut = x_samples[: fft_size * Nmax]
    y_samples_cut = y_samples[: fft_size * Nmax]
    X_freq, tx, X_fsamples = signal.stft(
        x_samples_cut / full_scale,
        fs=sample_rate,
        window="hann",
        nperseg=fft_size,
        noverlap=0,
        padded=False,
        boundary=None,
    )
    Y_freq, ty, Y_fsamples = signal.stft(
        y_samples_cut / full_scale,
        fs=sample_rate,
        window="hann",
        nperseg=fft_size,
        noverlap=0,
        padded=False,
        boundary=None,
    )

    # get the cumulative mean to show effect of different integration times
    XY_corr = X_fsamples * np.conjugate(Y_fsamples)
    cmean = np.cumsum(XY_corr, axis=1) / np.arange(1, Nmax + 1)

    logger_.info(
        "Plotting cross polarization power spectrum (x and y signals correlated) over different integration times... "
    )
    plt.figure()
    for ii in xypol_corr_iters:  # plot desired number of averaged iterations
        plt.plot(
            X_freq / 10**6,
            10 * np.log10(np.abs(cmean[:, ii - 1])),
            label="%d Iteration(s)" % ii,
        )
    plt.ylabel("Magnitude (dB)")
    plt.xlabel("Frequency (MHz)")
    plt.legend()
    plt.grid()
    plt.title("Magnitude of averaged cross polarization power spectrum")
    if save_results:
        logger_.info("saving cross pol power spectrum magnitude plot")
        plt.savefig(results_dir + "/xypol_corr_magnitude.png")
    if show_live_plot:
        plt.show()

    if False:  # can enable plot for further testing
        logger_.info(
            "Plotting linear cross polarization power spectrum (x and y signals correlated) over different integration times... "
        )
        plt.figure()
        for (
            ii
        ) in xypol_corr_iters:  # plot desired number of averaged iterations
            plt.plot(
                X_freq / 10**6,
                np.abs(cmean[:, ii - 1]),
                label="%d Iteration(s)" % ii,
            )
        plt.ylabel("Magnitude (linear correlator ^2 units)")
        plt.xlabel("Frequency (MHz)")
        plt.legend()
        plt.grid()
        plt.title("Magnitude of averaged cross polarization power spectrum")
        if save_results:
            logger_.info(
                "saving cross pol power spectrum magnitude plot linear units"
            )
            plt.savefig(results_dir + "/xypol_corr_magnitude_linear.png")
        if show_live_plot:
            plt.show()

    plt.figure()
    for ii in xypol_corr_iters:  # plot desired number of averaged iterations
        plt.plot(
            X_freq / 10**6,
            np.angle(cmean[:, ii - 1]),
            label="%d Iteration(s)" % ii,
        )
    # plots 0 to pi phase ramp created by delay enable for visual comparison
    # plt.plot([min(X_freq)/10**6, max(X_freq)/10**6], [0, np.pi], 'k-.', label='pi ramp')
    plt.ylabel("Phase angle (rad)")
    plt.xlabel("Frequency (MHz)")
    plt.legend(loc="upper right")
    plt.grid()
    plt.title("Phase of averaged cross polarization power spectrum")
    if save_results:
        logger_.info("saving wrapped phase plot")
        plt.savefig(results_dir + "/xypol_corr_phase.png")

    plt.figure()
    for ii in xypol_corr_iters:  # plot desired number of averaged iterations
        plt.plot(
            X_freq / 10**6,
            np.unwrap(np.angle(cmean[:, ii - 1])),
            label="%d Iteration(s)" % ii,
        )
    plt.ylabel("Phase angle (rad)")
    plt.xlabel("Frequency (MHz)")
    plt.legend(loc="upper right")
    plt.grid()
    plt.title("Phase of averaged cross polarization power spectrum")
    if save_results:
        logger_.info("saving unwrapped phase plot")
        plt.savefig(results_dir + "/xypol_corr_phase_unwrapped.png")
    if show_live_plot:
        plt.show()


def read_filter_coeffs(fir_filter_file):
    fir_filter_info = json.load(open(fir_filter_file))

    h_fxp_polX = fir_filter_info["filter_params"][0]["h_fxp"]
    h_fxp_polY = fir_filter_info["filter_params"][1]["h_fxp"]

    coefArray_polX = np.array(
        [elem for singleList in h_fxp_polX for elem in singleList]
    )
    coefArray_polY = np.array(
        [elem for singleList in h_fxp_polY for elem in singleList]
    )

    return coefArray_polX, coefArray_polY


def scaled_filter_freq_resp(
    coefArray, noise_std, sample_rate, fft_size, full_scale
):
    w1, h1 = signal.freqz(coefArray, fs=sample_rate, worN=fft_size)
    h1 *= (noise_std) / full_scale / np.sqrt(fft_size / 2)
    return w1, h1


def plot_packet_data(
    x_samples,
    y_samples,
    sample_rate,
    histogram_bin_size,
    lstv_in_params_file,
    result_params,
):
    """Plots X/Y data samples and generates a histogram of the X/Y data samples
    Input Parameters:
    x_samples              : one dimensional array of X samples (length = N packets x samples in packet)
    y_samples              : one dimensional array of Y samples (length = N packets x samples in packet)
    sample_rate            : sample rate
    TODO: change description for bin size when range changed to depend on variable bit size
    histogram_bin_size     : number of histogram bins to use for 12 bit samples (range of -2048 to +2047)
    lstv_in_params_file    : the name of the lstv parameter file that was used to generate the data
    result_params          :dict with parameters for the analysis process, including...
        save_results       : whether results should be saved
        output_dir         : the output results directory
        show_live_plot     : whether results should be plotted live
    Return:
    """

    """
    # Write data to a matlab format file
    logger_.info("plot_packet_data: Plot {:} data samples".format(len(x_samples)))
    x_data = np.asarray(x_samples, dtype=np.int16)
    y_data = np.asarray(y_samples, dtype=np.int16)
    mdic = {"x_pol_data": x_data, "y_pol_data": y_data}
    scipy.io.savemat('sample_data.mat', mdic)
    """
    save_results = result_params["save_results"]
    results_dir = result_params["output_dir"]
    show_live_plot = result_params["show_live_plots"]

    # Count  samples that are saturated
    # TODO: change hardcoded saturated limits to be based on number of bits in data (will be input)
    saturated_count = 0
    for i in range(0, len(x_samples)):
        if (
            (x_samples[i] == -2048)
            or (x_samples[i] == 2047)
            or (y_samples[i] == -2048)
            or (y_samples[i] == 2047)
        ):
            saturated_count += 1
    if saturated_count > 0:
        logger_.info(
            f"# of data samples that are saturated = {saturated_count}"
        )

    logger_.info(
        f"Sanity check max and min x_sample value : {min(x_samples)}, {max(x_samples)}"
    )
    logger_.info(
        f"Sanity check max and min y_sample value : {min(y_samples)}, {max(y_samples)}"
    )

    plot_samples = False  # or debugging
    # Plot the X/Y sample data
    if plot_samples:
        logger_.info("\nPlotting sample data (X/Y)...")
        plt.subplot(211)
        plt.plot(x_samples, ".")
        plt.title("Amplitudes of X and Y polarized samples", fontsize=14)
        plt.ylabel("X (digitization units)", fontsize=12)
        plt.subplot(212)
        plt.plot(y_samples, ".")
        plt.ylabel("Y (digitization units)", fontsize=12)
        plt.xlabel("Sample #", fontsize=14)
        if save_results:
            logger_.info("saving plot of sample data")
            plt.savefig(results_dir + "/xandy_sample_data.png")
        if show_live_plot:
            plt.show()

    plot_yvsx_samples = True
    if plot_yvsx_samples:
        logger_.info("Plotting sample data Y vs X ...")
        plt.figure()
        plt.title("Y vs X polarized samples", fontsize=14)
        plt.plot(x_samples, y_samples, ".")
        plt.ylabel("Y (digitization units)", fontsize=12)
        plt.xlabel("X (digitization units)", fontsize=12)
        if save_results:
            logger_.info("saving xvsy samples plot")
            plt.savefig(results_dir + "/yvsx_sample_data.png")
        if show_live_plot:
            plt.show()

        plt.figure()
        logger_.info("Plotting heat map of Y vs X data (hist2d) ...")
        plt.hist2d(x_samples, y_samples, bins=50, cmap="inferno")
        plt.title("Heatmap Y vs X polarized samples", fontsize=14)
        plt.ylabel("Y (digitization units)", fontsize=12)
        plt.xlabel("X (digitization units)", fontsize=12)
        plt.colorbar()
        if save_results:
            logger_.info("saving xvsy heatmap")
            plt.savefig(results_dir + "/heatmap.png")
        if show_live_plot:
            plt.show()

    # TODO: change histogram plot range based on number of bits
    plot_histo = False  # or debugging
    if plot_histo:
        # Plot the histogram of the X/Y sample data
        logger_.info("\nPlotting Histogram of sample data (X/Y) ...")
        plt.subplot(211)
        plt.autoscale()
        plt.hist(x_samples, bins=histogram_bin_size, range=(-2048, 2047))
        plt.title("Histogram of X/Y pol sample data", fontsize=14)
        plt.ylabel("X (bin counts)", fontsize=12)
        plt.grid()
        plt.subplot(212)
        plt.autoscale()
        # plt.autoscale(enable=True, axis='y')
        plt.hist(y_samples, bins=histogram_bin_size, range=(-2048, 2047))
        plt.ylabel("Y (bin counts)", fontsize=12)
        plt.xlabel("Histogram bins", fontsize=12)
        plt.grid()
        if save_results:
            logger_.info("saving histogram")
            plt.savefig(results_dir + "/histogram.png")
        if show_live_plot:
            plt.show()

    plot_PSD = False  # or debugging
    # Plot the PSD of the X/Y sample data
    if plot_PSD:
        logger_.info(
            "\nPlotting Power Spectrum Density of sample data (X/Y) ..."
        )
        plt.subplot(211)
        plt.axis([0, sample_rate / 2, -110, -30])
        plt.grid()
        plt.psd(x_samples, Fs=sample_rate)
        plt.title(
            "Power Spectral Density of X and Y polarized samples ", fontsize=12
        )
        plt.ylabel("X PSD [dB/Hz]", fontsize=12)
        plt.subplot(212)
        plt.axis([0, sample_rate / 2, -110, -30])
        plt.grid()
        plt.psd(y_samples, Fs=sample_rate)
        plt.ylabel("Y PSD [dB/Hz]", fontsize=12)
        plt.xlabel("Frequency [Hz]", fontsize=12)
        if save_results:
            logger_.info("saving power spectrum density")
            plt.savefig(results_dir + "/PSD.png")
        if show_live_plot:
            plt.show()

    # Read LSTV gen input params from JSON file
    lstv_in_params_info = json.load(open(lstv_in_params_file))

    # parse lstv input parameters
    receiver_information = lstv_in_params_info["receiver"]
    receiver_select = receiver_information["select"]
    rx_noise_std_polX = receiver_information["noise_info_polX"]["noise_std"]
    rx_noise_std_polY = receiver_information["noise_info_polY"]["noise_std"]

    # celestial sky source information
    source_information = lstv_in_params_info["source"]
    source_select = source_information[0]["select"]
    src_noise_std_polX = source_information[0]["noise_info_polX"]["noise_std"]
    src_noise_std_polY = source_information[0]["noise_info_polY"]["noise_std"]

    rfi_information = lstv_in_params_info["rfi"]
    tone1_select = rfi_information[0]["select"]
    tone1_phase_inc_polX = rfi_information[0]["tone_info_polX"]["phase_inc"]
    tone1_mag_scale_polX = rfi_information[0]["tone_info_polX"]["magnitude"]
    tone1_phase_inc_polY = rfi_information[0]["tone_info_polY"]["phase_inc"]
    tone1_mag_scale_polY = rfi_information[0]["tone_info_polY"]["magnitude"]

    # digital FIR filter information - coefficients and scaled freq response
    # Gain the filter response by the std dev relative to full scale.
    # TODO: change hardcoded to be based on input number of bits
    fft_size = 2**11
    scaling_factor = 2**4  # 4 bit right shift
    full_scale = 2**11  # 2**(Nb-1)
    # todo from Michelle - should plot coeffs against h_fxp_int16_polX, h_fxp_int16_polYs
    if receiver_select:
        coefArray_polX, coefArray_polY = read_filter_coeffs(
            lstv_in_params_info["fir_filter_rcv_file"]
        )
        w1_polX, h1_polX = scaled_filter_freq_resp(
            coefArray_polX,
            rx_noise_std_polX,
            sample_rate,
            fft_size,
            full_scale,
        )
        w1_polY, h1_polY = scaled_filter_freq_resp(
            coefArray_polY,
            rx_noise_std_polY,
            sample_rate,
            fft_size,
            full_scale,
        )
    if source_select:  # celestial source
        coefArray_src_polX, coefArray_src_polY = read_filter_coeffs(
            lstv_in_params_info["fir_filter_src_file"]
        )
        w1_src_polX, h1_src_polX = scaled_filter_freq_resp(
            coefArray_src_polX,
            src_noise_std_polX,
            sample_rate,
            fft_size,
            full_scale,
        )
        w1_src_polY, h1_src_polY = scaled_filter_freq_resp(
            coefArray_src_polY,
            src_noise_std_polY,
            sample_rate,
            fft_size,
            full_scale,
        )

    design_filter = False  # or debugging
    # Design Digital Bandpass filter for test data
    if design_filter:
        numtaps = 1024
        f1, f2 = 0.48, 0.52
        coefArray = signal.firwin(numtaps, [f1, f2], pass_zero=False)
        # filter the X Pol signal data
        signal.filtfilt(coefArray, [1.0], x_samples)

    # generate tone signals, calculate spectrum
    if tone1_select:
        Fc_polX = (tone1_phase_inc_polX / 2**32) * sample_rate
        N = len(x_samples)
        time = np.arange(N) / float(sample_rate)
        samples = (tone1_mag_scale_polX / scaling_factor) * (
            np.sin(2 * np.pi * Fc_polX * time)
        ).astype(np.float32)
        f, t, Zxx = signal.stft(
            samples / full_scale,
            fs=sample_rate,
            window="hann",
            nperseg=fft_size,
            noverlap=0,
            padded=False,
            boundary=None,
        )
        tone_abs_polX = np.mean(np.abs(Zxx), axis=1)
        Fc_polY = (tone1_phase_inc_polY / 2**32) * sample_rate
        samples = (tone1_mag_scale_polY / scaling_factor) * (
            np.sin(2 * np.pi * Fc_polY * time)
        ).astype(np.float32)
        f, t, Zyy = signal.stft(
            samples / full_scale,
            fs=sample_rate,
            window="hann",
            nperseg=fft_size,
            noverlap=0,
            padded=False,
            boundary=None,
        )
        tone_abs_polY = np.mean(np.abs(Zyy), axis=1)

    # Short Time Fourier Transform (STFT) of X pol filtered data
    logger_.info(
        "Plotting spectrum using Short Time Fourier Transform (STFT) of X pol filtered data (X/Y) ..."
    )
    f, t, Zxx = signal.stft(
        2 * x_samples / full_scale,
        fs=sample_rate,
        window="hann",
        nperseg=fft_size,
        noverlap=0,
        padded=False,
        boundary=None,
    )
    X_abs = np.mean(np.abs(Zxx), axis=1)
    plt.subplot(211)
    plt.ylim(-160, 0)
    plt.grid()
    if receiver_select:
        plt.plot(
            w1_polX,
            20 * np.log10(np.abs(h1_polX)),
            color="r",
            lw=3,
            label="Receiver Filter Mag",
        )
    if source_select:  # celestial source
        plt.plot(
            w1_src_polX,
            20 * np.log10(np.abs(h1_src_polX)),
            color="y",
            lw=3,
            label="Source Filter Mag",
        )
    if tone1_select:
        plt.plot(
            f, 20 * np.log10(tone_abs_polX), color="c", lw=3, label="Tone PSD"
        )
    plt.plot(f, 20 * np.log10(X_abs), color="b", label="Signal PSD")
    plt.ylabel("X Magnitude [dBFS]", fontsize=12)
    plt.legend(loc="upper right")
    # Short Time Fourier Transform (STFT) of Y pol filtered data
    f, t, Zyy = signal.stft(
        2 * y_samples / full_scale,
        fs=sample_rate,
        window="hann",
        nperseg=fft_size,
        noverlap=0,
        padded=False,
        boundary=None,
    )
    Y_abs = np.mean(np.abs(Zyy), axis=1)
    plt.title("Spectrum (STFT method) of X/Y Pol samples", fontsize=12)
    plt.subplot(212)
    plt.ylim(-160, 0)
    plt.grid()
    if receiver_select:
        plt.plot(
            w1_polY,
            20 * np.log10(np.abs(h1_polY)),
            color="r",
            lw=3,
            label="Receiver Filter Mag",
        )
    if source_select:  # celestial source
        plt.plot(
            w1_src_polX,
            20 * np.log10(np.abs(h1_src_polY)),
            color="y",
            lw=3,
            label="Source Filter Mag",
        )
    if tone1_select:
        plt.plot(
            f, 20 * np.log10(tone_abs_polY), color="c", lw=3, label="Tone PSD"
        )
    plt.plot(f, 20 * np.log10(Y_abs), color="b", label="Signal PSD")
    plt.ylabel("Y Magnitude [dBFS]", fontsize=12)
    plt.xlabel("Frequency [Hz]", fontsize=12)
    plt.legend(loc="upper right")
    if save_results:
        logger_.info("saving spectrum figure")
        plt.savefig(results_dir + "/STFT_spectrum.png")
    if show_live_plot:
        plt.show()

    # 3D Spectrogram of the X pol filtered data
    plot_spectros = False  # for debugging
    if plot_spectros:
        plot_3D_spectros = False
        if plot_3D_spectros:
            logger_.info(
                "\nPlotting 3D Spectrogram of X pol filtered sample data  ..."
            )
            f, t, ZxxX = signal.stft(
                x_samples, fs=sample_rate, window="hann", nperseg=fft_size
            )
            ZxxX_abs = np.abs(ZxxX)
            ZxxX_abs_db_norm = 20 * np.log10(ZxxX_abs / np.amax(ZxxX_abs))
            fig = plt.figure()
            ax = fig.gca(projection="3d")
            x = []
            y = []
            for counter, i in enumerate(f):
                x.append(np.array([i for k in t]))
                y.append(t)
            # vmin=-40, vmax=0
            p = ax.plot_surface(
                np.array(x), np.array(y), ZxxX_abs_db_norm, cmap="jet"
            )
            fig.colorbar(p)
            ax.set_zlim3d(-80, 0)
            ax.set_zlabel("Signal Level[dB]")
            # plt.autoscale()
            plt.title("3D Spectrogram of Y pol sample data")
            plt.xlabel("Frequency [Hz]")
            plt.ylabel("Time [sec]")
            if save_results:
                logger_.info("saving 3D spectrogram for xpol")
                plt.savefig(results_dir + "/3Dspectrogram_xpol.png")
            if show_live_plot:
                plt.show()

            # 3D Spectrogram of the Y pol filtered data
            logger_.info(
                "\nPlotting 3D Spectrogram of Y pol filtered sample data ..."
            )
            f, t, ZxxY = signal.stft(
                y_samples, fs=sample_rate, window="hann", nperseg=fft_size
            )
            ZxxY_abs = np.abs(ZxxY)
            ZxxY_abs_db_norm = 20 * np.log10(ZxxY_abs / np.amax(ZxxY_abs))
            fig = plt.figure()
            ax = fig.gca(projection="3d")
            x = []
            y = []
            for counter, i in enumerate(f):
                x.append(np.array([i for k in t]))
                y.append(t)
            # vmin=-40, vmax=0
            p = ax.plot_surface(
                np.array(x), np.array(y), ZxxY_abs_db_norm, cmap="jet"
            )
            fig.colorbar(p)
            ax.set_zlim3d(-80, 0)
            ax.set_zlabel("Signal Level[dB]")
            # plt.autoscale()
            plt.title("3D Spectrogram of Y pol sample data")
            plt.xlabel("Frequency [Hz]")
            plt.ylabel("Time [sec]")
            if save_results:
                logger_.info("saving 3D spectrogram for ypol")
                plt.savefig(results_dir + "/3Dspectrogram_ypol.png")
            if show_live_plot:
                plt.show()

        # 2D Spectrogram of the X pol filtered data
        logger_.info("\nPlotting 2D Spectrogram of X filtered sample data...")
        f, t, ZxxX = signal.stft(
            x_samples, fs=sample_rate, window="hann", nperseg=fft_size
        )
        ZxxX_abs = np.abs(ZxxX)
        ZxxX_abs_db_norm = 20 * np.log10(ZxxX_abs / np.amax(ZxxX_abs))
        plt.pcolormesh(t, f, ZxxX_abs_db_norm, shading="gouraud", cmap="jet")
        plt.colorbar()
        plt.autoscale()
        plt.title("Spectrogram of X pol data (Magnitude [dB] in colour)")
        plt.ylabel("Frequency [Hz]")
        plt.xlabel("Time [sec]")
        if save_results:
            logger_.info("saving 2D spectrogram for xpol")
            plt.savefig(results_dir + "/2Dspectrogram_xpol.png")
        if show_live_plot:
            plt.show()

        # 2D Spectrogram of the Y pol filtered data
        logger_.info(
            "\nPlotting 2D Spectrogram of Y pol filtered sample data ..."
        )
        f, t, ZxxY = signal.stft(
            y_samples, fs=sample_rate, window="hann", nperseg=fft_size
        )
        ZxxY_abs = np.abs(ZxxY)
        ZxxY_abs_db_norm = 20 * np.log10(ZxxY_abs / np.amax(ZxxY_abs))
        plt.pcolormesh(t, f, ZxxY_abs_db_norm, shading="gouraud", cmap="jet")
        plt.colorbar()
        plt.autoscale()
        plt.title("Spectrogram of Y Pol data (Magnitude [dB] in colour)")
        plt.ylabel("Frequency [Hz]")
        plt.xlabel("Time [sec]")
        if save_results:
            logger_.info("saving 2D spectrogram for ypol")
            plt.savefig(results_dir + "/2Dspectrogram_ypol.png")
        if show_live_plot:
            plt.show()

    logger_.info("plot_packet_data: Plotted data samples")


def plot_packet_meta_data(
    packet_number_from_1pps, number_of_time_samples_from_1pps, result_params
):
    """Plots X/Y data samples and generates a histogram of the X/Y data samples
    Input Parameters:
    packet_number_from_1pps           : 1D arr packet_number_from_1pps meta frame fields
    number_of_time_samples_from_1pps  : 1D arr of number_of_time_samples_from_1pps meta frame fields
    result_params         : dict with parameters for the analysis process, including...
        save_results       : whether results should be saved
        output_dir         : the output results directory
        show_live_plot     : whether results should be plotted live
    Return:
    """
    logger_.info(
        "plot_packet_meta_data: Plot {:} dynamic meta data samples".format(
            len(packet_number_from_1pps)
        )
    )
    save_results = result_params["save_results"]
    results_dir = CAPTURE_DIR
    show_live_plot = result_params["show_live_plots"]

    """Plot the meta data fields"""
    logger_.info("\nPlotting packet dynamic meta frame fields...")
    plt.subplot(211)
    plt.title("Packet dynamic meta frame fields", fontsize=14)
    plt.plot(packet_number_from_1pps, ".")
    plt.ylabel("Packets from 1pps", fontsize=14)
    plt.subplot(212)
    plt.plot(number_of_time_samples_from_1pps, ".")
    plt.ylabel("Samples from 1pps", fontsize=14)
    plt.xlabel("Sample #", fontsize=14)
    if save_results:
        logger_.info("saving meta data plot")
        plt.savefig(results_dir + "/meta_data.png")
    if show_live_plot:
        plt.show()
    logger_.info("plot_packet_meta_data: Plotted dynamic meta data samples")


# put in full path and file name
# will strip just to the file name (to last '/')
def return_base_name(fname):
    slash_idx = fname.rfind("/")
    if slash_idx > 0:
        return fname[slash_idx + 1 :]  # noqa: E203
    else:
        return fname


def usage_message(app_name):
    """If no parameters provided, provide usage message"""
    logger_.info(
        "\nNAME\n"
        + "      "
        + app_name
        + " application reads DISH packets and processes them.\n"
    )
    logger_.info("SYNOPSIS")
    logger_.info(
        "      "
        + app_name
        + " [lstv_in_params_file]\n"
        + " [analysis_in_params_file]\n"
    )
    logger_.info("DESCRIPTION")
    logger_.info(
        "      "
        + app_name
        + " application is used to read DISH packets from a 100GE network interface,\n"
        + "      or binary file containing packets that had been previously captured with this application.\n"
        + "      The packet meta data is checked and the sample data statistics are calculated\n"
    )
    logger_.info("OPTIONS")
    logger_.info(
        "      lstv_in_params_file\n"
        + "            Json file filled with a the information used to generate the \n"
        + "            lstv that will be analyzed with the analysis tool."
    )
    logger_.info(
        "      analysis_in_params_file\n"
        + "            Json file filled with a complete set of parameters that will be read in.\n"
        + "            A default input parameter file is included in the src directory\n"
        + "            (default_inputs.json) with the appropriate key names for each parameter.\n"
        + "            File names and other input parameters should be adjusted as needed."
    )
    logger_.info("For more information see readme")


def main(args=None, **kwargs):
    """Main loop that requests user input, reads, parses and plots the DISH packet sample data"""
    # logger = log.getLogger("packet_processor")
    # log_format = "%(asctime)-15s %(message)s"
    # log.basicConfig(
    #     filename="PlotSampleData.log", filemode="w", level=logger_.info, format=log_format
    # )
    logger_.info("main: DISH packet processor started")

    parameter_error = False

    application = os.path.basename(sys.argv[0])
    app_name = application.strip()

    """Parse command line parameters"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "lstv_in_params_file", type=str, default="default_inputs.json"
    )
    parser.add_argument(
        "analysis_in_params_file", type=str, default="default_inputs.json"
    )

    parsed_args, unparsed_args = parser.parse_known_args()
    lstv_in_params_file = sys.argv[1]
    analysis_in_params_file = sys.argv[2]
    analysis_inputs = json.load(open(analysis_in_params_file))

    """Read from input json file to check if enough parameters"""
    packets_to_process = analysis_inputs["capture_params"][
        "num_packets_to_capture"
    ]
    capture_from_device = analysis_inputs["capture_params"][
        "capture_from_device"
    ]
    if capture_from_device:  # capturing packets from a network device
        device_to_capture = NETWORK_DEVICE
    else:  # read in packets from a file
        input_filename = analysis_inputs["capture_params"][
            "packet_data_filename"
        ]

    # parameters controlling the output
    plot_selection = analysis_inputs["result_params"]["plot_selection"]
    save_results = analysis_inputs["result_params"]["save_results"]
    if save_results:
        output_dir = analysis_inputs["result_params"]["output_dir"]
        if output_dir[-1] != "/":  # check proper format for directory
            output_dir += "/"

    """If input parameters are insufficient, provide usage message and exit"""
    if capture_from_device and not device_to_capture:
        logger_.info(
            "ERROR: capture_from_device selected but not device_to_capture specified"
        )
        parameter_error = True
    elif not capture_from_device and not input_filename:
        logger_.info(
            "ERROR: no input_filename specified to read data from and capture_from_device false"
        )
        parameter_error = True
    elif packets_to_process == 0:
        logger_.info("ERROR: zero packets specified to process")
        parameter_error = True
    if parameter_error:
        usage_message(app_name)
        sys.exit(2)

    """Make save results directory and save input files"""
    if save_results:
        results_dir = CAPTURE_DIR

        # want to save data packets in the correct directory
        # NOTE: if save_results is on packets will save even if output_filename is blank
        capture_file = analysis_inputs["capture_params"]["capture_file"]
        if capture_file:  # if it's not blank add the results directory prefix
            analysis_inputs["capture_params"]["capture_file"] = (
                results_dir + "/" + capture_file
            )
        else:
            logger_.info(
                "No name specified for output data packet 'capture_file' saving as 'bite_packet_data.dat'"
            )
            analysis_inputs["capture_params"]["capture_file"] = (
                results_dir + "/" + "bite_packet_data.dat"
            )

        # save a copy of the lstv params json
        lstv_base_fname = return_base_name(lstv_in_params_file)
        os.popen(
            "cp "
            + lstv_in_params_file
            + " "
            + results_dir
            + "/"
            + lstv_base_fname
        )

        # save a copy of the input parameter json
        analysis_input_base_fname = return_base_name(analysis_in_params_file)
        os.popen(
            "cp "
            + analysis_in_params_file
            + " "
            + results_dir
            + "/"
            + analysis_input_base_fname
        )

        # TODO: possibly save a copy of the filter file too?

        # overwrite for later use
        analysis_inputs["result_params"]["output_dir"] = results_dir

    else:
        # if save_results is false we don't want to save the data
        analysis_inputs["capture_params"]["capture_file"] = ""

    """Capture and parse required number of packets"""
    (meta_data, sample_data, packets_parsed) = capture_and_parse_packet_data(
        analysis_inputs["capture_params"]
    )

    # check if # samples in each packet is consistent.
    number_of_time_samples_in_packet = meta_data[
        "number_of_time_samples_in_packet"
    ].flatten()
    for i in range(0, len(number_of_time_samples_in_packet)):
        if i > 0:
            if (
                number_of_time_samples_in_packet[i]
                != number_of_time_samples_in_packet[i - 1]
            ):
                logger_.info(
                    f"End of 1PPS period detected, # of samples in packet is different \
                 {number_of_time_samples_in_packet[i]}"
                )

    # Read DISH sample rate
    transport_sample_rate = meta_data["transport_sample_rate"].flatten()

    if packets_parsed and (packets_parsed == packets_to_process):
        """Plot dynamic meta fields"""
        if plot_selection == "a" or plot_selection == "m":
            packet_counter_from_1pps = meta_data[
                "packet_counter_from_1pps"
            ].flatten()
            number_of_time_samples_from_1pps = meta_data[
                "number_of_time_samples_from_1pps"
            ].flatten()
            plot_packet_meta_data(
                packet_counter_from_1pps,
                number_of_time_samples_from_1pps,
                analysis_inputs["result_params"],
            )

        """ Plot sample data"""
        x_samples = sample_data["x"].flatten()
        y_samples = sample_data["y"].flatten()
        """ Calculate and logger_.info mean, STD and variance of sample data """
        x_sample_mean = np.mean(x_samples)
        x_sample_std = np.std(x_samples)
        logger_.info(
            "X Sample statistics:\nMean = {:10.5}\nStd  = {:10.7}".format(
                x_sample_mean, x_sample_std
            )
        )
        y_sample_mean = np.mean(y_samples)
        y_sample_std = np.std(y_samples)
        logger_.info(
            "Y Sample statistics:\nMean = {:10.5}\nStd  = {:10.7}".format(
                y_sample_mean, y_sample_std
            )
        )

        """Save statistics to results folder"""
        if save_results:
            logger_.info(
                f"Saving statistics to statistics.json in {results_dir}"
            )
            f = open(results_dir + "/statistics.json", "w")
            stats_dict = {}
            stats_dict["x"] = {}
            stats_dict["x"]["mean"] = x_sample_mean
            stats_dict["x"]["std"] = x_sample_std
            stats_dict["y"] = {}
            stats_dict["y"]["mean"] = y_sample_mean
            stats_dict["y"]["std"] = y_sample_std
            json.dump(stats_dict, f, indent=4)
            f.close()

        """Plot sample data and histogram of sample data for required number of packets"""
        if plot_selection == "a" or plot_selection == "s":
            # TODO: change number of histogram bins based on number of bits in data
            histogram_bins = 512
            plot_packet_data(
                x_samples,
                y_samples,
                transport_sample_rate[0],
                histogram_bins,
                lstv_in_params_file,
                analysis_inputs["result_params"],
            )
            plot_xypol_correlation(
                x_samples,
                y_samples,
                transport_sample_rate[0],
                np.array(analysis_inputs["result_params"]["xypol_corr_iters"]),
                analysis_inputs["result_params"],
            )
    else:
        logger_.info("main: Data not found to calculate statistics and plot")
        logger_.info("\nNo data available ...")


if __name__ == "__main__":
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    logger_ = logging.getLogger("PlotSampleData.py")
    main()
