# test_fir_filter
import json
from pprint import pprint

import matplotlib.pyplot as plt
import numpy as np

fir_filter_file = "T1_BD1_fir_filter_gen_out_params.json"
# fir_filter_file = "T3_LP_fir_filter_gen_out_params.json"
pol_idx = 1  # 0 (for X) or 1 (for Y)
plot_spectrum = False
fft_size = 2048
# ------------------------------------------------------------------------

fir_filter_info = json.load(open(fir_filter_file))

# NOTE: h_fxp_int16 is scaled (multiplied) by 2**15 with respect to h_fxp.
print("fir  filter file name: {}".format(fir_filter_file))
print("fir_filter_info design parameters:")
print(
    "pole_id: {}".format(fir_filter_info["filter_params"][pol_idx]["pole_id"])
)
pprint(fir_filter_info["filter_params"][pol_idx]["design_params"])
print("")
# Extract filter_coeffs for pol X
h_fxp_int16 = fir_filter_info["filter_params"][pol_idx]["h_fxp_int16"]
h_fxp_int16 = np.squeeze(np.array(h_fxp_int16, dtype=np.int32))

h_fxp = fir_filter_info["filter_params"][pol_idx]["h_fxp"]
h_fxp = np.squeeze(np.array(h_fxp))

h_fxp_int16_magsq = np.sum(np.square(h_fxp_int16))
h_fxp_magsq = np.sum(np.square(h_fxp))

print("max(h_fxp_int16) = {}".format(h_fxp_int16.max(0)))
print("h_fxp_int16_magsq = {}".format(h_fxp_int16_magsq))
print("h_fxp_int16_mag = {}\n".format(np.sqrt(h_fxp_int16_magsq)))

print("max(h_fxp) = {}".format(h_fxp.max(0)))
print("h_fxp_magsq = {}".format(h_fxp_magsq))
print("h_fxp_mag = {}".format(np.sqrt(h_fxp_magsq)))

if plot_spectrum:
    # Plot the filter spectrum in dB
    n_coeffs = h_fxp.shape[0]
    H = np.fft.fftshift(np.fft.fft(h_fxp, n=fft_size))

    # generater the frequency axis
    ff = 0.5 * np.linspace(-1 + 1 / fft_size, 1, fft_size)

    fig, axs = plt.subplots(1, 1)
    axs.plot(ff, 20 * np.log10(abs(H)), color="c")
    # axs.plot(ff, abs(H), color="b")
    axs.set_title("Filter Spectrum", fontsize=10)
    axs.set_xlabel("Normalized Frequency", fontsize=10)
    axs.set_ylabel("Magnitude [dB]", fontsize=10)
    axs.autoscale(enable=True, axis="x", tight=True)
    axs.grid()
    fig.tight_layout()
    plt.show()
