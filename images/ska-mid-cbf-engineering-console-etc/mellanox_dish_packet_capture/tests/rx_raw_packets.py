import socket
import sys
from timeit import default_timer as timer

import numpy as np
from scapy.all import ETH_P_ALL

FILE_NAME = "raw_packets.dat"
DISH_PACKET_SIZE_IN_BYTES = 8000

"""Read command line parameters"""
if len(sys.argv) != 3:
    print(
        "{:} <interface name, i.e. enp179s0f1> <# packets to capture>".format(
            sys.argv[0]
        )
    )
    sys.exit()
else:
    device_to_capture = sys.argv[1]
    packets_to_capture = int(sys.argv[2])
    print(
        "Capture {:} raw ethernet packet(s) from device {:}".format(
            packets_to_capture, device_to_capture
        )
    )

"""Setup receiver socket """
dish_socket = socket.socket(
    socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(ETH_P_ALL)
)
dish_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 2**28)
dish_socket.bind((device_to_capture, 0))

"""Loop reading DISH packets"""
dish_packets = np.zeros(
    (packets_to_capture, DISH_PACKET_SIZE_IN_BYTES), dtype=np.uint8
)
print("Capturing...")
receive_buffer = dish_socket.recv(DISH_PACKET_SIZE_IN_BYTES)  # flushing buffer
start = timer()
for packets_captured in range(packets_to_capture):
    receive_buffer = dish_socket.recv(DISH_PACKET_SIZE_IN_BYTES)
    #    if receive_buffer and len(receive_buffer) == DISH_PACKET_SIZE_IN_BYTES:
    if receive_buffer:
        dish_packets[packets_captured] = np.frombuffer(
            receive_buffer, dtype=np.uint8
        )
    else:
        print("Unexpected socket event, i.e., a timeout or an error event")
        break

end = timer()

"""
print("Extracting header value...")
field_size = 6
field_offset = 48
xx= np.array([i for i in range (packets_to_capture)])
for i in range(len(dish_packets)):
   field = dish_packets[i][field_offset: field_offset + field_size]
   xx[i] = int.from_bytes(field, byteorder="big", signed=False)
   if i > 1:
      delta = xx[i]-xx[i-1]
      if delta > 2604:
          print(delta)
"""

"""
print("plotting...")
plt.plot(xx)
plt.ylabel('No. of time samples from 1PPS')
plt.show()
print("Saving to file...")
"""

"""
packet_data_file = open(FILE_NAME, 'wb')
packet_data_file.write(dish_packets)
packet_data_file.close()
"""

total_size = packets_captured * DISH_PACKET_SIZE_IN_BYTES
total_time = end - start
print(
    "Thruput (Transfers/sec) = {:6.0f}".format(packets_captured / total_time)
)
print(
    "Thruput (MByte/sec)     = {:6.0f}".format(
        total_size / (total_time * 1000000)
    )
)
