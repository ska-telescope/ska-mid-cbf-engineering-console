"""
This dish_simulator.py test stub program receives a request via a TCP connection from a client to send DISH packets
The server then reads the packets from a file and sends it to the client via a separate UDP connection
"""
import logging as log
import socket
from pathlib import Path

FILE_NAME = "packet.dat"
FILE_FOLDER = Path("")
DATA_FILE_NAME = FILE_FOLDER / FILE_NAME

DISH_PACKET_SIZE_IN_BYTES = 8000
DISH_IP_ADDRESS = "10.50.1.1"
DISH_PORT_NUMBER = 4132
DISH_ADDRESS = (DISH_IP_ADDRESS, DISH_PORT_NUMBER)


def main():
    """Setup test server logger service"""
    log.getLogger("dish_simulator")
    log.basicConfig(
        filename="dish_simulator.txt",
        filemode="w",
        level=log.INFO,
        format="%(asctime)s %(message)s",
    )

    log.info("test_server: Setup DISH UDP socket")
    client_packet_connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    """Loop reading requests from client, sending requested DISH packets to client"""
    while True:
        packets_sent = 0
        """Get request from client"""
        packets_to_send = input("Please enter packets to send:\n")
        packets_to_send = int(packets_to_send)
        if packets_to_send == 0:
            print("Selected {:} packets, quiting...".format(packets_to_send))
            quit()
        else:
            print("Selected {:} packets to sent ...".format(packets_to_send))
            log.info(
                "test_server: Selected {:} packets to send".format(
                    packets_to_send
                )
            )
        """ Loop reading DISH packets from a file,
            send packets to  client via the UDP connection"""
        log.info("test_server: Opened DISH packet file")
        packet_data_file = open(DATA_FILE_NAME, "rb")
        while packets_sent < packets_to_send:
            """Read packet from data file"""
            dish_packet = packet_data_file.read(DISH_PACKET_SIZE_IN_BYTES)
            if len(dish_packet) != DISH_PACKET_SIZE_IN_BYTES:
                log.info("test_server: DISH packet file error or EOF detected")
                packet_data_file.close()
                break  # EOF detected
            """Send packet to the client"""
            try:
                bytes_sent = client_packet_connection.sendto(
                    dish_packet, DISH_ADDRESS
                )
                if bytes_sent == DISH_PACKET_SIZE_IN_BYTES:
                    packets_sent += 1
                    continue
                else:
                    log.info(
                        "test_server: Error sending UDP DISH packet to client"
                    )
                    break
            except socket.error as e:
                log.warning(str(e))
                log.info(
                    "test_server: EXCEPTION, socket error trying to sent UDP packet to client"
                )
                break
        log.info("test_server: Sent {:} packets".format(packets_sent))


if __name__ == "__main__":
    while True:
        main()
