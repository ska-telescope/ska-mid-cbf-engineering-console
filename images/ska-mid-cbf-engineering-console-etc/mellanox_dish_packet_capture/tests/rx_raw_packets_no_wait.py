import select
import socket
import sys
from timeit import default_timer as timer

import numpy as np
from scapy.all import ETH_P_ALL

FILE_NAME = "raw_packets.dat"

DISH_PACKET_SIZE_IN_BYTES = 8000

# Read command line parameters
if len(sys.argv) != 3:
    print(
        "{:} <interface name, i.e. enp179s0f1> <# packets to capture>".format(
            sys.argv[0]
        )
    )
    sys.exit()
else:
    device_to_capture = sys.argv[1]
    packets_to_capture = int(sys.argv[2])
    print(
        "Capture {:} raw ethernet packet(s) from device {:}".format(
            packets_to_capture, device_to_capture
        )
    )

"""Setup receiver socket """
dish_socket = socket.socket(
    socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(ETH_P_ALL)
)
dish_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 2**28)
dish_socket.bind((device_to_capture, 0))
dish_socket.setblocking(False)
poller = select.poll()
read_events = select.POLLIN | select.POLLPRI
poller.register(dish_socket, read_events)

"""Loop reading DISH packets"""
dish_packets = np.zeros(
    (packets_to_capture, DISH_PACKET_SIZE_IN_BYTES), dtype=np.uint8
)
print("Capturing...")
while True:
    packets_captured = 0
    start = timer()
    while packets_captured < packets_to_capture:
        events = poller.poll(5000)
        for flag, fd in events:
            if flag & read_events:
                receive_buffer = dish_socket.recv(DISH_PACKET_SIZE_IN_BYTES)
                dish_packets[packets_captured] = np.frombuffer(
                    receive_buffer, dtype=np.uint8
                )
                packets_captured += 1
            else:
                print(
                    "Unexpected socket event, i.e., a timeout or an error event"
                )
                break

    end = timer()
    print(packets_captured)
    for i in range(len(dish_packets)):
        print(dish_packets[i][24:28])
    print("Saving to file...")
    packet_data_file = open(FILE_NAME, "wb")
    packet_data_file.write(dish_packets)
    packet_data_file.close()

    total_size = packets_captured * DISH_PACKET_SIZE_IN_BYTES
    total_time = end - start
    print(
        "Thruput (Transfers/sec) = {:6.0f}".format(
            packets_captured / total_time
        )
    )
    print(
        "Thruput (MByte/sec)     = {:6.0f}".format(
            total_size / (total_time * 1000000)
        )
    )
    break
