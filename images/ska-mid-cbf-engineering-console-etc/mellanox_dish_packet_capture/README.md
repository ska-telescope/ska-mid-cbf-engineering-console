# BITE MVP - Verification tools for Meta_Frames and Sample data extraction/analysis

## Table of contents
* [Description](#description)
* [Getting started](#getting-started)
* [Repository](#repository)
* [Prerequisities](#prerequisities)
* [Running Application](#running-application)
* [Known bugs](#known-bugs)
* [Troubleshooting](#troubleshooting)
* [License](#license)

## Description
This project contains utilities that can be used capture and save raw DISH packet data that is received the DELL server skadev1 100GE Mellanox X-5 network interface card when sent from a Talon-DX card 100GE interface. 

The utility can also parse the encoded DISH packets into meta and sample data and plots the raw X and Y (H/V polorazation) sample data and a histogram of the X and Y sample data. The 'PlotSampleData.py' utility is located in the 'src' directory and can be run as follows:


## Getting started

The project can be found in the NRC DRAO gitlab repository.

To get a local copy of the project:

```bash
git clone https://gitlab.drao.nrc.ca/SKA/util/tools/mellanox_dish_packet_capture.git
```
## Prerequisities

Only perform these prerequisite steps if 1) your Python3 environment has not been setup and 2) the Mellanox X-5 I/F card has not yet been configured properly (it only needs to be done once after installation or possibly a reboot)

* A Python3 development environment properly configured, as described in [SKA developer portal](https://developer.skatelescope.org/en/latest/tools/tango-devenv-setup.html)
* Setup Python3 to be able to access the Mellanox 100GE Ethernet interface device in raw mode without having to run as 'sudo'
```bash
sudo setcap cap_net_raw,cap_net_admin=eip /usr/bin/python3.7
```
* Setup 100GE interface device in promiscuous mode, to receive all packet types
```bash
sudo ifconfig enp179s0f1 promisc allmulti
```
* Setup 100GE interface device to receive jubbo packets and bring the device up if it is not yet enabled
```bash
sudo ifconfig enp179s0f1 mtu 9000 up
```
* Setup 100GE interface device maximum input buffer size. If this commands fails, try setting the buffer size to a smaller number (e.g. 4096) and then set it to the maximum (8192 in the server case)
```bash
sudo ethtool -G enp179s0f1 rx 8192
```
* Setup 100GE interface device with a large number of receive packet buffers
```bash
sudo sysctl -w net.core.rmem_max=1073741824
sudo sysctl -p
```
## Repository organization

The Mellanox_DISH_Packet_Capture` repository is organized in a single code tree. The
hierarchy contains:

* src: contains the specific python utility to use
* _test_: contains early test/prototype files, to capture raw packets from the 100GE interface

## Running Application
The test setup requires a Talon-DX board 100G output to be connected to server `rmdskadevdu001` Mellanox X-5 100G Ethernet interface adpater. This requires an optical fibre and 2 x 100G QSFP28 optic modules. 

To power on the Talon 1 LRU (and the Talon DX 1 board), issue the following command:
```bash
. /data/share/bin/talon_power_lru1 on
```

To run the `PlotSampleData.py` application, issue the following sequence of commands: 
```bash
. /data/share/hwtests/program_talon1.sh
```
This script uses the INTEL Quartus tool to download the firmware to the Talon-DX board. Ensure that your $PATH contains the path to the Quartus executable.

Next connect to the Talon-DX board Linux kernel on the Stratix 10 chip HPS and run some scripts. Script `lstv_gen_dell.sh` commands the firmware to generate the LSTV.

```bash
ssh root@talon1
cd /lib/firmware/scripts/bite_spl_4
./lstv_gen_dell.sh
```
Script `lstv_replay_dell_32th.sh` commands the Talon-DX Stratix 10 firmware to start sending the LSTV data to the server over the 100GE interface at 1/32th of real-time speed (~380MByte/sec). 
```bash
./lstv_replay_dell_32th.sh
```
Change to the `src` directory where the `PlotSampleData.py` resides. Note you will need the full paths to the lstv_in_params_file and the analysis_in_params_file, as well as a copy of the fir filter files listed in the lstv_in_params_file in the `src` directory. See the application usage information below:
```bash
NAME
      PlotSampleData.py application reads DISH packets and processes them.

SYNOPSIS
      PlotSampleData.py  [lstv_in_params_file] [analysis_in_params_file]

DESCRIPTION
      PlotSampleData.py application is used to read DISH packets from a 100GE network interface,
      or binary file containing packets that had been previously captured with this application.
      The packet meta data is checked and the sample data statistics are calculated

OPTIONS
      lstv_in_params_file
	   Json file filled with a the information used to generate the
		lstv that will be analyzed with the analysis tool.

      analysis_in_params_file
	    Json file filled with a set of parameters that will be read in.
		A default input parameter file is included in the src directory
		(default_inputs.json) with the appropriate key names for each parameter.
		File names and other input parameters should be adjusted as needed.
		Keys are described later in this readme.
EXAMPLES
	1)    Read DISH packets generated with the inputs in 'lstv_params.json' to be
		analyzed using settings from a parameter file 'params.json'
		PlotSampleData.py	lstv_params.json    analysis_params.json

ENVIRONMENT
      Visit the following link see how to setup  the runtime environment:
            https://gitlab.drao.nrc.ca/SKA/util/tools/mellanox_dish_packet_capture

```


```bash
$ python3 PlotSampleData.py

Capturing packets ...

Parsing packets ...

Plotting sample data ...

Histogramming sample data ...

```
![Plots of the sample data and a histogram of the sample data](img/plots_samples_and_histogram.png)
---
**See above examples of the output plots produced by the application**

## Input analysis file parameters
See default_inputs.json for the default input paramter list. The individual parameters are described in more detail below corresponding to their last key.

- **num_packets_to_capture** : Number of DISH packets to read and process.
- **capture_from_device** : true if lstv being read directly from device eg. socket connected to talon baord, false if data being read from file
- **network_device** : network device to read from
- **first_packet_counter_to_capture** : when capturing from device, the first packet counter from 1pps to be captured, used to line up lstv for comparison between tests
- **packet_data_filename** : input binary file as source for DISH packets in place of network-device.
- **packet_offset** : when binary input file is specified, read packets starting after packet offset.
- **plot_selection** : 'a' to plot all data,  'm' to plot meta-data, 's' to plot sample-data.
- **xypol_corr_iters** : used in plotting of cross pol correlation plot; array of number of iterations to average over, comparing effects of different integration times on correlation
- **show_live_plots** : display pop up plots of results as program runs, will need to be closed to continue analysis
- **save_results** : save results of the analysis
- **output_dir** : output directory for results. timestamped subfolder will be created here containing results of a given run.
- **output_filename** : When save_results true, name of binary format file to write DISH input packets to, will be in results directory (if blank default is bite_packet_data.dat). If save_results false, data will not be saved.

## Known bugs

_Still none_

## Troubleshooting

To confirm that the Talon-DX is sending LSTV data and the Mellanox X-5 100G interface device is receiving the data, issue the following command Wireshark command on the server to see that the packets are present:
```bash
tshark -i enp179s0f1 -c 10 -V
```
See that the output is as follows:
```bash
$ tshark -i enp179s0f1 -c 10
Capturing on 'enp179s0f1'
    1 0.000000000 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
    2 0.000042052 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
    3 0.000084163 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
    4 0.000126126 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
    5 0.000169004 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
    6 0.000210868 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
    7 0.000379304 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
    8 0.000420817 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
    9 0.000462846 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
   10 0.000505002 00:aa:bb:cc:dd:ee → Mellanox_56:2b:47 0xfeed 8000 Ethernet II
10 packets captured
```


## License
See the LICENSE file for details.
