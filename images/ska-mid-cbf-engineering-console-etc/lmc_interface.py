import json
import os
import time

from pytango_client_wrapper import PyTangoClientWrapper

class LmcInterface:
    """Class used to simulate the LMC - Mid.CBF interface."""

    def __init__(self):
        """
        Initializes the device proxies to the relevant MCS devices.
        """
        self.simulation_mode = True

        self.fqdn_cbfcontroller = "mid_csp_cbf/sub_elt/controller"
        self.fqdn_subarray = "mid_csp_cbf/sub_elt/subarray_01"
        self.fqdn_tm = "ska_mid/tm_leaf_node/csp_subarray_01"

        self.cbfcontroller = PyTangoClientWrapper()
        self.cbfcontroller.create_tango_client(self.fqdn_cbfcontroller)
        self.cbfcontroller.set_timeout_millis(100000)
        self.cbfcontroller.write_attribute("loggingLevel", "DEBUG")

        self.subarray = PyTangoClientWrapper()
        self.subarray.create_tango_client(self.fqdn_subarray)
        self.subarray.set_timeout_millis(10000)

        self.tm = PyTangoClientWrapper()
        self.tm.create_tango_client(self.fqdn_tm)
        self.tm.set_timeout_millis(5000)

        self.init_sys_param_file = "ext_config/initial_system_param.json"
        self.vcc_to_dish = {1: 'SKA001', 2: 'SKA036', 3: 'SKA063', 4: 'SKA100'}

        print(f"VCC to receptor mapping: {self.vcc_to_dish}")

    def init_sys_param(self):
        """
        Sends the system param as a JSON-formatted string with
        the InitSysParam() command to the CbfController. The command
        only works when MCS is in OFF state.
        """
        with open(self.init_sys_param_file) as f:
            sp = f.read()
        self.cbfcontroller.command_read_write("InitSysParam", sp)

    def on_command(self):
        """
        Enables use of the hardware by MCS, and sends the On()
        command to the CbfController.
        """
        self.cbfcontroller.write_attribute(
            "simulationMode", self.simulation_mode
        )
        self.cbfcontroller.write_attribute("adminMode", 0)

        # the MCS should be in OFF state now
        self.init_sys_param()

        self.cbfcontroller.command_read_write("On")

    def off_command(self):
        """
        Enables use of the hardware by MCS, and sends the On()
        command to the CbfController.
        """
        self.cbfcontroller.command_read_write("Off")

    def add_receptors(self, board_num):
        receptor_ids = []
        receptor_ids.append(self.vcc_to_dish[board_num])
        self.subarray.command_read_write("AddReceptors", receptor_ids)

    def configure_scan(self, board_num, ip_address):
        receptor_ids = []
        receptor_ids.append(self.vcc_to_dish[board_num])

        configure_scan_dict = {
            "interface": "https://schema.skao.int/ska-csp-configure/2.3",
            "subarray": {"subarray_name": "Single receptor"},
            "common": {
                "config_id": "1 receptor, band 1, 1 FSP, no options",
                "frequency_band": "1",
                "subarray_id": 0,
            },
            "cbf": {
                "delay_model_subscription_point": "ska_mid/tm_leaf_node/csp_subarray_01/delayModel",
                "rfi_flagging_mask": {},
                "fsp": [
                    {
                        "fsp_id": board_num,
                        "function_mode": "CORR",
                        "receptors": receptor_ids,
                        "frequency_slice_id": 1,
                        "zoom_factor": 1,
                        "zoom_window_tuning": 450000,
                        "integration_factor": 10,
                        "channel_offset": 14880,
                        "output_link_map": [
                            [0, 4],
                            [744, 8],
                            [1488, 12],
                            [2232, 16],
                            [2976, 20],
                            [3720, 24],
                            [4464, 28],
                            [5206, 32],
                            [5952, 36],
                            [6696, 40],
                            [7440, 44],
                            [8184, 48],
                            [8928, 52],
                            [9672, 56],
                            [10416, 60],
                            [11160, 64],
                            [11904, 68],
                            [12648, 72],
                            [13392, 76],
                            [14136, 80],
                        ],
                        "output_host": [[0, ip_address]],
                        "output_port": [[0, 14000, 1]],
                    }
                ],
            },
        }

        self.subarray.command_read_write(
            "ConfigureScan", json.dumps(configure_scan_dict)
        )

    def scan(self):
        scan_dict = {
            "interface": "https://schema.skao.int/ska-csp-scan/2.2",
            "scan_id": 1,
        }

        self.subarray.command_read_write("Scan", json.dumps(scan_dict))

    def end_scan(self):
        self.subarray.command_read_write("EndScan")
        time.sleep(5)
        self.subarray.command_read_write("GoToIdle")
        time.sleep(5)
        self.subarray.command_read_write("RemoveAllReceptors")
