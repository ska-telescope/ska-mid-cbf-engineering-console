# SKA Mid.CBF Engineering Console

Documentation on the Developer's portal:
[ReadTheDocs](https://developer.skao.int/projects/ska-mid-cbf-engineering-console/en/latest/)

Code repository: [ska-mid-cbf-engineering-console](https://gitlab.com/ska-telescope/ska-mid-cbf-engineering-console)

The Engineering Console is built in the pipeline and deployed to the Central Artefact Repository [CAR](https://artefact.skao.int/#browse/browse:docker-internal:v2%2Fska-mid-cbf-engineering-console).

## Installation
Now that Engineering Console has been mostly ported to TANGO Devices, the environment requirements are a bit different depending on your needs.
### TANGO Devices
To run the TANGO device database for the devices used, a minikube should be started to handle the images.

Note: Engineering Console (specifically the download-artifacts command) will ONLY work on kube4, kube5 and kube6.

From the [ska-cicd-deploy-minikube](https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube) repo:
```bash
make minikube-install DRIVER=docker
```
Once the minikube status returns it is running, the images for the EC outside of the deployer and BITE can be built. From the repository's base directory run the following (this may take some time):
```bash
eval $(minikube docker-env)
make oci-build-all
```
Then, once the build is done, we deploy the required resources for the minikube from the Helm charts.
```bash
make k8s-install-chart
```
### Jive
Ensure your docker-env is unset:
```bash
eval $(minikube docker-env --unset)
```
```bash
make x-jive
```
## Deployer Device Use
### Generate TalonDX Config File

#### From Command Line
To generate the talonDX config file based on the board configuration. Run the following command:
```bash
make generate-talondx-config BOARDS=<BOARDS> (optional)FSPMODES=<FSPMODES>
```
Where `BOARDS` is a comma-delimited list of board numbers you wish to turn on and deploy the HPS device servers onto (ex. `BOARDS=1,2,3,4`)

Where `FSPMODES` (optional) is a comma-delimited list of possible FSP Modes for the TalonDX Boards, mapped one-to-one with `BOARDS` (ex. `FSPMODES=CORR,CORR,CORR,CORR`)

`FSPMODES` defaults to `CORR` if `FSPMODES` is not used or if a FSP Mode passed to the `FSPMODES` list was invalid.

#### From the Deployer Device
First, set the boards and fsp modes for the boards to be configured using the `configure_boards_and_fsp_mode` command, with a JSON string with a key:value format of Board ID (str):FSP Mode (str). 

Example input:
```
'{"1":"CORR","2":"CORR","3":"CORR","4":"CORR","5":"PST","6":"PST","7":"PST","8":"PST"}'
```

Then generate the configuration file using the `generate_config_jsons` command.

For AA 0.5/1.0, a maximum of 8 boards are supported.  Mapping of FSP modes to the boards must match one of the following options:
- 4 CORR (Boards 1-4),
- 4 PST  (Boards 1-4), or
- 4 CORR (Boards 1-4) and 4 PST (Boards 5-8)

There is a validation step to check that the FSP modes given to the Deployer matches the above configurations.  You can set the `validateBoardModeConfiguration` attribute to False to disable this validation for testing purposes. This will allow FSP mode assignments to any boards.


### Download Artefacts from CAR
To download FPGA bitstreams and Talon Tango device binaries from CAR to the CBF artifacts directory:

Note: The following command will ONLY work on kube4, kube5, or kube6.

To run download-artifacts on kube4:
```bash
sudo ifconfig enp3s0f1 down
make download-artifacts
sudo ifconfig enp3s0f1 up
```
To run download-artifacts on kube5 or kube6:
```bash
make download-artifacts
```
### Configure the Tango DB
```bash
make config-db
```
This command adds the Talon device servers as specified in the `talondx-config.json` file.

Note: the artefacts need to be downloaded before updating the database (the artefacts contain detri JSON files needed for the DB update).
To upload new FPGA bitstreams to CAR for use, [see the ska-mid-cbf-talondx project](https://gitlab.com/ska-telescope/ska-mid-cbf-talondx)

## BITE Device Use

### Pass in BITE config files
Load in the files will be used to configure the scan. 
```bash
make load-bite-configs BITE_CONFIG_DIR="<CONFIG_DIR>" TEST_ID="<TEST_ID>"
```
Where: 
- `<CONFIG_DIR>` is the directory path containing the config files. It should match the [file structure seen in system tests](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/tree/main/test_parameters)
- `<TEST_ID>` is the ID of the to set up for, such as Test_1.

### Set up BITE devices
From the root directory of the engineering console run the following:
```bash
make generate-bite-data
```

### Dish Packet Capture
Open a new terminal. From the root directory of the engineering console run the following:
```bash
make dish-packet-capture
```
This command will not exit until the following command is run. 

### Start LSTV Replay
From the root directory of the engineering console run the following:
```bash
make start-lstv-replay
```

### Stop LSTV Replay
To stop the LSTV replay loop for ds-bite, run the following:
```bash
make stop-lstv-replay
```

## Viewing Logs
```bash
kubectl logs <POD_NAME> -n <NS>
```

## Notes

### Raw Repository
FPGA bitstreams are uploaded manually to the raw repository in CAR (Common Artefact Repository, https://artefact.skao.int/) here: 

`raw-internal/ska-mid-cbf-talondx/fpga-test/talon_dx-{_bitstream-name_}-v{_version_}.tar.gz`

### Ex. Manually Package the BITE Bitstream Files   
```bash
mkdir bin
cp bite5.json bin/
cp mvp5_wip02.core.rbf bin/
cp mvp5_wip02.dtb bin/
tar -cvf talon_dx-bite-v0.5.0.tar bin
gzip -k talon_dx-bite-v0.5.0.tar
```

### Ex. Manually Unpackage the BITE Bitstream Files
```bash
gzip -d talon_dx-bite-v0.5.0.tar.gz
tar -xvf talon_dx-bite-v0.5.0.tar
```
where {_version_} is in the X.Y.Z format.

## Run the Docker Image Interactively
To run the docker interactively:
```bash
make run-interactive
```
which opens a bash shell in the docker. 
To see available test script options: 
```bash
./talondx.py --help
```
To install vim/nano editors while in interactive mode:
```bash
apt-get update
apt-get -y install vim nano
```
## Overriding DS Artefacts with local build
In order for this script to work, ensure to clone and build your device servers in the same root directory:
Example: If clone ds-vcc and ds-lstv-gen device servers ensure both are cloned under the same directory which would like like:
1. /home/user/dev/ds/ds-lstv-gen
2. /home/user/dev/ds/ds-vcc

To override the device servers (ds-lstv-gen,ds-vcc in this example) run the following command:
```bash
make ds_list=ds-lstv-gen,ds-vcc ds_basedir=<path to ds base directory> ec_dir=<path to ec checkout> ds-override-local
```
where ds_basedir is the path to the device server root directory of clone, /home/user/dev/ds from the previous example 

## Pull and run the Docker from CAR
```bash
docker pull artefact.skao.int/ska-mid-cbf-engineering-console:<version>
docker run artefact.skao.int/ska-mid-cbf-engineering-console:<version>
```

## Read the Docs
The Engineering Console project auto-generates [Read the Docs](https://developer.skao.int/projects/ska-mid-cbf-engineering-console/en/latest/) documentation, which includes this README.

To re-generate the documentation locally prior to checking in updates to Git:
```bash
make documentation
```
To see the generated documentation, open `/ska-mid-cbf-engineering-console/docs/build/html/index.html` in a browser -- e.g.,
```bash
firefox docs/build/html/index.html &
```
