#
# Project makefile for a Tango project. You should normally only need to modify
# PROJECT below.
#

#
# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag of
# artefact.skao.int/ska-tango-examples/powersupply
#
PROJECT = ska-mid-cbf-engineering-console

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-mid-cbf
DOMAIN ?= cluster.local

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

OCI_IMAGES ?= ska-mid-cbf-engineering-console ska-mid-cbf-engineering-console-etc
OCI_IMAGES_TO_PUBLISH ?= $(OCI_IMAGES)
OCI_IMAGE_BUILD_CONTEXT = $(PWD)
HELM_RELEASE ?= test## HELM_RELEASE is the release that all Kubernetes resources will be labelled with
HELM_CHART ?= ska-mid-cbf-engineering-console-umbrella## HELM_CHART the chart name
K8S_TEST_TANGO_IMAGE_PARAMS = --set ska-mid-cbf-engineering-console.engineeringconsole.image.tag=$(VERSION)
K8S_UMBRELLA_CHART_PATH ?= ./charts/ska-mid-cbf-engineering-console-umbrella
K8S_CHART ?= $(HELM_CHART)
K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.exposeAllDS=$(EXPOSE_All_DS) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set global.operator=$(SKA_TANGO_OPERATOR) \
	--set ska-tango-base.itango.enabled=$(ITANGO_ENABLED) \
	${K8S_TEST_TANGO_IMAGE_PARAMS} \
	${TARANTA_PARAMS}
TANGO_DATABASE = tango-databaseds-$(HELM_RELEASE)
TANGO_HOST = $(TANGO_DATABASE):10000## TANGO_HOST is an input!
SKA_TANGO_OPERATOR ?= true
# this assumes host and talon board 1g ethernet is on the 192.168 subnet
HOST_IP = $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | grep 192.168) 
TARANTA ?= false# Enable Taranta
MINIKUBE ?= true ## Minikube or not
EXPOSE_All_DS ?= false ## Expose All Tango Services to the external network (enable Loadbalancer service)
ITANGO_ENABLED ?= true## ITango enabled in ska-tango-base
ITANGO_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.5
# use python3 if not CI job
ifeq ($(strip $(CI_JOB_ID)),)
PYTHON_RUNNER = python3 -m
endif


# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= true# Enable jive
WEBJIVE ?= false# Enable Webjive

CI_PROJECT_PATH_SLUG ?= ska-mid-cbf-engineering-console
CI_ENVIRONMENT_SLUG ?= ska-mid-cbf-engineering-console
$(shell echo 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gitlab_values.yaml)

# define private overrides for above variables in here
-include PrivateRules.mak

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/base.mk
include .make/release.mk
include .make/make.mk
include .make/python.mk
include .make/oci.mk
include .make/docs.mk
include .make/k8s.mk
include .make/helm.mk
include .release

IMG_DIR := $(PWD)/images/ska-mid-cbf-engineering-console
SRC_DIR := $(PWD)/src/ska_mid_cbf_engineering_console

# Test runner - run to completion job in K8s
# name of the pod running the k8s_tests
TEST_RUNNER = test-runner-$(CI_JOB_ID)-$(RELEASE_NAME)

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=$(IMG_DIR)
PYTHON_VARS_AFTER_PYTEST = -m "not post_deployment"
# PYTHON_BUILD_TYPE = non_tag_setup
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,E402,E501,F407,W503
PYTHON_LINT_TARGET = $(SRC_DIR)

MCS_DATABASE_POD = $(shell kubectl get svc -n $(KUBE_NAMESPACE) | grep databaseds | grep "10000:" | cut -d ' ' -f1)
MCS_TANGO_HOST = $(MCS_DATABASE_POD).$(KUBE_NAMESPACE).svc.$(DOMAIN):10000

run:  ## Run docker container
	docker run --rm $(strip $(OCI_IMAGE)):$(release)

ADD_HOSTS_FILE := $(PWD)/scripts/hosts.out
ADD_HOSTS = $(shell cat $(ADD_HOSTS_FILE))

# overwrite to use a different JSON
TALONDX_LOCAL_DIR = $(PWD)/mnt/talondx-config/
MNT_LOCAL_DIR = $(PWD)/mnt/

EXT_CONFIG_LOCAL_DIR=$(PWD)/ext_config/

HW_CONFIG_FILE=$(EXT_CONFIG_LOCAL_DIR)/hw_config.yaml

# server instance to target for DISH packet capture/RDMA Rx
TALON_UNDER_TEST := talon3_test

BITE_CAPTURE_LOCAL_DIR = $(PWD)"/mnt/bite-data/capture-results"
BITE_STATUS_LOCAL_DIR = $(PWD)"/mnt/bite-data/board-status"
VIS_CAPTURE_LOCAL_DIR = $(PWD)"/mnt/visibility-capture"

# This is the default board specification.
# BOARDS = "1"

# Default simulation mode
SIM = "1"

DM_PACKAGE_DRAFT = "delay_models/input/dm_package_draft.json"
DM_WITH_EPOCHS = "ext_config/delay_models/dm_package_with_epochs.json"
DM_CADENCE_SECONDS = 10

HOSTNAME=$(shell hostname)
ifeq ($(HOSTNAME),rmdskadevdu001.mda.ca)
	BITE_IFACE_NAME="ens2f0np0"
	VIS_IFACE_NAME="ens2f1np1"
else ifeq ($(HOSTNAME),rmdskadevdu002.mda.ca)
	BITE_IFACE_NAME="ens4f0np0"
	VIS_IFACE_NAME="ens4f1np1"
endif

VIS_IP=$(shell ifconfig $(VIS_IFACE_NAME) | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')


config-tango-dns: ## Create list of additional MCS DS hosts for docker run command
	@. scripts/config-tango-dns.sh $(KUBE_NAMESPACE) $(DOMAIN) $(ADD_HOSTS_FILE)

config-etc-hosts:
	@. scripts/config-etc-hosts.sh $(KUBE_NAMESPACE) $(DOMAIN)

FINAL_DIR_FILE := $(PWD)/scripts/final_dir.out
FINAL_DIR = $(shell cat $(FINAL_DIR_FILE))

capture-datetime-dir:
	@. scripts/create-datetime-dir.sh $(BITE_CAPTURE_LOCAL_DIR) $(FINAL_DIR_FILE)

status-datetime-dir:
	@. scripts/create-datetime-dir.sh $(BITE_STATUS_LOCAL_DIR) $(FINAL_DIR_FILE)

visibility-datetime-dir:
	@. scripts/create-datetime-dir.sh $(VIS_CAPTURE_LOCAL_DIR) $(FINAL_DIR_FILE)

x-jive: config-tango-dns  ## Run Jive with X11
	@chmod 644 $(HOME)/.Xauthority
	@docker run --rm \
	--network host \
	--env DISPLAY \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	--volume /tmp/.X11-unix:/tmp/.X11-unix \
	--volume $(HOME)/.Xauthority:/home/tango/.Xauthority \
	$(ADD_HOSTS) artefact.skao.int/ska-tango-images-tango-jive:7.22.5 &

x-pogo:  ## Run POGO with X11
	@echo DS_XMI=$(DS_XMI)
	@chmod 644 $(HOME)/.Xauthority
	@docker run --rm \
	--network host \
	--env DISPLAY \
	--volume /tmp/.X11-unix:/tmp/.X11-unix \
	--volume $(HOME)/.Xauthority:/home/tango/.Xauthority \
	--volume $(PWD)/pogo/$(DS_XMI).xmi:/home/tango/ds/$(DS_XMI).xmi:rw \
	--volume $(PWD)/pogo/SkaMidTalondxHpsBase.xmi:/home/tango/ds/SkaMidTalondxHpsBase.xmi:rw \
	--volume $(PWD)/pogo/.pogorc:/home/tango/.pogorc:rw \
	--volume $(PWD)/pogo/:/home/tango/pogo/:rw \
	--user tango \
	$(ADD_HOSTS) artefact.skao.int/ska-tango-images-tango-pogo:9.6.36 &

run-interactive: config-tango-dns  ## Run docker in interactive mode
	docker run --rm -it \
	--network host \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/ext_config \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) bash

config-db: ## Configure the database
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_deploy.py  --configure-db

device-check: ## Verify devices exported
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_deploy.py  --device-check

generate-talondx-config: ## Generate configuration JSONs for the deployer device.
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_deploy.py  --generate-config --boards=$(BOARDS) $(if $(FSPMODES),--fsp-modes=$(FSPMODES),)
	
#Creates a new directory: /mnt/talondx-config
download-artifacts:  ## Turns on the deployer TANGO device and downloads artifacts from the repository
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_deploy.py  --download-artifacts

check-deployer: ## Print the status of the deployer device and what is currently 
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_deploy.py  --check-deployer

ds-override-local:
	python3 ds_local_build.py ds_local_build.json $(ds_list) $(ds_basedir) $(ec_dir)

mcs-itango3:   ## open itango3 shell in MCS
	kubectl exec -it cbfcontroller-controller-0 -n $(KUBE_NAMESPACE) -- itango3

TEST_ID="Test_1"

BITE_CONFIG_DIR ?= ## Update this to point to the system-test test_parameters directory

load-bite-configs: ## Load in required config JSON files to the device
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_bite.py --load-config-files --talon-config-dir=$(BITE_CONFIG_DIR) --test-id=$(TEST_ID)

generate-bite-data: ## Configure the BITE device on target Talon board
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_bite.py --generate-bite-data

start-lstv-replay: ##  Starts the LSTV replay loop, must have configured TANGO BITE device ready
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_bite.py --start-lstv-replay

stop-lstv-replay:  ##  Stops the LSTV replay loop, must have configured TANGO BITE device ready
	export TANGO_HOST="$(MCS_TANGO_HOST)"; \
	python3 scripts/ds_local_bite.py --stop-lstv-replay

dish-packet-capture: config-tango-dns capture-datetime-dir
	mkdir -p $(BITE_CAPTURE_LOCAL_DIR)
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--env "CAPTURE_DIR=/app/images/$(strip $(OCI_IMAGE))-etc/bite-capture-results" \
	--env BITE_IFACE_NAME=$(BITE_IFACE_NAME) \
	--volume "$(FINAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/bite-capture-results:rw" \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/ext_config \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE))-etc:$(release) ./talondx.py --dish-packet-capture
	
write-board-status: config-tango-dns status-datetime-dir
	mkdir -p $(BITE_STATUS_LOCAL_DIR)
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--env "TALONDX_STATUS_OUTPUT_DIR=/app/talon-board-status" \
	--volume $(FINAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/talon-board-status:rw \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/ext_config \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE))-etc:$(release) ./talondx.py --write-board-status

talon-bite-lstv-replay: config-tango-dns
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-bite/ext_config \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE))-bite:$(release) ./midcbf_bite.py --talon-bite-lstv-replay --boards=$(BOARDS)

talon-bite-stop-lstv-replay: config-tango-dns
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-bite/ext_config \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE))-bite:$(release) ./midcbf_bite.py --talon-bite-stop-lstv-replay --boards=$(BOARDS)

talon-version: config-tango-dns
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/artifacts:rw \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/ext_config \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE))-etc:$(release) ./talondx.py --talon-version

talon-status: config-tango-dns
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--env TERM=xterm \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/artifacts:rw \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/ext_config \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE))-etc:$(release) ./talondx.py --talon-status
	
mcs-update-delay-models: config-tango-dns
	@docker run --rm \
	--network host \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/ext_config \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE))-etc:$(release) ./delay_models_formatter.py --update-delay-models --dm-template-full-path=$(DM_PACKAGE_DRAFT) --cadence-seconds=$(DM_CADENCE_SECONDS)  --intermediate-dm-full-path=$(DM_WITH_EPOCHS)

mcs-publish-delay-models: config-tango-dns
	@docker run --rm \
	--network host \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	-v $(EXT_CONFIG_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))-etc/ext_config \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE))-etc:$(release) ./delay_models_publisher.py --publish-delay-models --intermediate-dm-full-path=$(DM_WITH_EPOCHS) --board-nums=$(BOARDS)


documentation:  ## Re-generate documentation
	cd docs && make clean && make html

pipeline-unit-test: ## Run simulation mode unit tests in a docker container as in the gitlab pipeline
	@docker run --volume="$$(pwd):/home/tango/ska-tango-examples" \
	--env PYTHONPATH=src:src/ska_tango_examples --env FILE=$(FILE) -it $(ITANGO_DOCKER_IMAGE) \
	sh -c "cd /home/tango/ska-tango-examples && make requirements && make python-test"

help: ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


.PHONY: all test help k8s lint logs describe namespace delete_namespace kubeconfig kubectl_dependencies k8s_test install-chart uninstall-chart reinstall-chart upgrade-chart interactive

