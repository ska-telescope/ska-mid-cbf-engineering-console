# -*- coding: utf-8 -*-
#
# This file is part of the Mid.CBF EC project
#
#
#
"""Release information for SKA Mid.CBF Engineering Console Python Package."""
import sys
from typing import Optional

name = "ska_mid_cbf_engineering_console"
version = "1.1.1"
version_info = version.split(".")
description = "A set of Mid MCS tango devices for the SKA Telescope."
author = "Team CIPA"
author_email = "jared.murphy@mda.space"
url = "https://gitlab.com/ska-telescope/ska-mid-cbf-engineering-console"
license = "BSD-3-Clause"  # noqa: A001
copyright = ""  # noqa: A001


def get_release_info(clsname: Optional[str] = None) -> str:
    """
    Return a formatted release info string.

    :param clsname: optional name of class to add to the info
    :type clsname: str

    :return: str
    """
    rmod = sys.modules[__name__]
    info = ", ".join(
        # type: ignore[attr-defined]
        (rmod.name, rmod.version, rmod.description)
    )
    if clsname is None:
        return info
    return ", ".join((clsname, info))


if __name__ == "__main__":
    print(version)
