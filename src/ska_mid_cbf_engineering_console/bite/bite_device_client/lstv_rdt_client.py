import csv
import logging
from glob import glob

from tango import DevFailed, DeviceProxy, DevState


class LstvRdtClient:
    """
    This class controls the LSTV Resampler Delay Tracker (RDT).
    Tasks include:
        - configure the input and output sampling rate for LSTV
        - provide the delay models to the LSTV RDT
        - start the LSTV RDT
        - stop the LSTV RDT

    :param talon_inst: instance of talon (e.g. 001)
    :param logger: logger
    """

    def __init__(
        self,
        talon_inst: str,
        logger: logging.Logger,
    ):
        self._logger = logger

        # RDT
        self._fqdn = (
            f"talondx-{talon_inst}/resamplerdelaytracker/resampler_bite_0"
        )
        self._rdt_dp = DeviceProxy(self._fqdn)

    """
    Configures the LSTV RDT. Sets input sample rate to the sky bandwidth 3960000000.
    Sets output sample rate to sky bandwidth + 1800 * k. Then start running the RDT.

    If delay_sourcce_dir is not provided, a polynomial with all 0's will be used
    for the duration of the LSTV. The RDT will still resample the data.

    :param output_sample_rate: the output sample rate
    :param lstv_seconds: length of LSTV in seconds
    :param fo_validity_interval: FO polynomial interval to be used in the LSTV resampler delay tracker
    :param delay_source_dir: directory containing delay models used in BITE RDT. If set to None,
                             zero coefficients are used
    """

    def start_rdt(
        self,
        output_sample_rate: int,
        lstv_seconds: int,
        fo_validity_interval: float,
        delay_source_dir: str = None,
    ):
        input_sample_rate = 3_960_000_000

        try:
            self.stop_rdt()
            self._rdt_dp.is_lstv_gen = True
            self._rdt_dp.fo_validity_interval = fo_validity_interval
            self._rdt_dp.num_lsq_points = 10
            self._rdt_dp.fo_low_threshold = 512
            self._rdt_dp.input_sample_rate = input_sample_rate
            self._rdt_dp.output_sample_rate = output_sample_rate
            self._rdt_dp.enable_resampler_dithering = 1
            self._rdt_dp.enable_phase_correction_dithering = 1

            # TODO: different signal sources should have different seeds.
            #       for now there is only 1 source.
            source_id = 0
            self._rdt_dp.dithering_seed_polX = 5490 * (source_id + 3)
            self._rdt_dp.dithering_seed_polY = (
                self._rdt_dp.input_sample_rate // (source_id + 4)
            )

            self._rdt_dp.start_resampler_delay_tracker()
            self._logger.info(
                f"Started LSTV RDT {self._fqdn}, resampling from {input_sample_rate} to {output_sample_rate}"
            )

            # Feed delay model(s) to the LSTV RDT.
            # LSTV will start at the first timestamp (typically 0)
            hodm_x = []
            hodm_y = []
            if delay_source_dir is not None:
                self._logger.info(
                    f"Reading delay models from {delay_source_dir}"
                )
                hodm_x = self._get_hodm(
                    delay_source_dir=delay_source_dir, pol="X"
                )
                hodm_y = self._get_hodm(
                    delay_source_dir=delay_source_dir, pol="Y"
                )
                if len(hodm_x) == 0 or len(hodm_y) == 0:
                    msg = (
                        f"No delay model CSVs are found at {delay_source_dir}"
                    )
                    self._logger.error(msg)
                    raise RuntimeError(msg)

            # Correct an offset in time created by resampling.
            S_TO_NS = 1000000000
            coeff0_offset = (
                (1.0 / input_sample_rate) - (1.0 / output_sample_rate)
            ) * S_TO_NS
            self._logger.info(
                f"Delay model const coefficient offset = {coeff0_offset}"
            )

            if len(hodm_x) == 0:
                self._logger.info(
                    "No delay models provided. Using default coefficients."
                )
                t0 = 0.0
                cadence = float(
                    lstv_seconds * 2 + 10
                )  # add some buffer just in case

                poly_x = [
                    cadence,
                    cadence,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    coeff0_offset,
                ]
                poly_y = poly_x
                self._push_to_rdt(t0, poly_x, poly_y)
            else:
                for dmx, dmy in zip(hodm_x, hodm_y):
                    if (
                        dmx["t_start"] != dmy["t_start"]
                        or dmx["t_stop"] != dmy["t_stop"]
                    ):
                        err_msg = "Start or stop time of X and Y pol polynomials mistmatch"
                        self._logger.error(err_msg)
                        raise Exception(err_msg)
                    self._logger.info(
                        f"Pushing HODM from t = {dmx['t_start']} to {dmx['t_stop']}"
                    )
                    t0 = dmx["t_start"]
                    cadence = dmx["t_stop"] - dmx["t_start"]
                    poly_x = [cadence, cadence] + dmx["coeffs"]
                    poly_y = [cadence, cadence] + dmy["coeffs"]
                    poly_x[-1] += coeff0_offset
                    poly_y[-1] += coeff0_offset
                    self._push_to_rdt(t0, poly_x, poly_y)

        except DevFailed as df:
            err_msg = (
                f"Failed to set LSTV RDT sampling rates: {df.args[0].desc}"
            )
            self._logger.error(err_msg)
            raise Exception(err_msg)
        return

    """
    Stops running the LSTV RDT
    """

    def stop_rdt(self):
        try:
            rdt_state = self._rdt_dp.state()
            if rdt_state == DevState.FAULT:
                self._rdt_dp.reset()
            self._rdt_dp.stop_resampler_delay_tracker()
            self._logger.info("Stopped LSTV RDT")
        except DevFailed as df:
            self._logger.error(f"Failed to stop LSTV RDT: {df.args[0].desc}")

    """
    Reads HODMs from the provided directory for the specified polarization.

    :param delay_source_dir: directory path where CSV files are in
    :param pol: polarzation. 'X' or 'Y'
    :return: a list of dict. Each dict has 3 fields "t_start", "t_stop", and "coeffs".
    """

    def _get_hodm(self, delay_source_dir: str, pol: str):
        csv_files = glob(f"{delay_source_dir}/*pol{pol.upper()}.csv")

        self._logger.info(f"Found CSV files: {csv_files}")

        hodm = []
        for cf in csv_files:
            with open(cf) as f:
                reader = csv.DictReader(
                    f, fieldnames=["t_start", "t_stop"], restkey="coeffs"
                )
                next(reader, None)  # skip header row
                for row in reader:
                    row["t_start"] = float(row["t_start"])
                    row["t_stop"] = float(row["t_stop"])
                    row["coeffs"] = [float(x) for x in row["coeffs"]]
                    hodm.append(row)
        return hodm

    """
    RDT expects the format {cadence_sec, validity_period_sec, coeffs[n-1], ..., coeffs[0]}
    For BITE's RDT, assign cadence and validty period to the same number.
    """

    def _reformat_for_rdt(self, hodm_in: dict):
        cadence = hodm_in["t_stop"] - hodm_in["t_start"]
        return [cadence, cadence] + hodm_in["coeffs"]

    """
    The start time of HODM and num coeffs have to be provided separately before
    sending the HODM polynomial.
    """

    def _push_to_rdt(self, t_start, poly_x, poly_y):
        self._rdt_dp.ho_start_utc_time = t_start

        # don't count cadence and validity period
        self._rdt_dp.num_coeffs = len(poly_x) - 2

        self._rdt_dp.push_hodm_polX(poly_x)
        self._rdt_dp.push_hodm_polY(poly_y)
        return
