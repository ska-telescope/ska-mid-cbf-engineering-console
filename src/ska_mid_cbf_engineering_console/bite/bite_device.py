"""
TANGO Device to interact with bite_device_client,
in order to configure and orchestrate tests across boards
"""

from __future__ import annotations  # allow forward references in type hints

import concurrent.futures
import json
from datetime import datetime, timezone

from astropy.time import Time
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import ResultCode
from tango import AttrWriteType, DevVarLongStringArray
from tango.server import attribute, command, run

from ska_mid_cbf_engineering_console.bite.bite_device_client.bite_client import (
    BiteClient,
)


class ECBite(SKABaseDevice):
    """
    TANGO device for handling the configuration and management of BITE tests.
    """

    # ----------
    # Attributes
    # ----------

    packet_rate_scale_factor = attribute(
        label="Packet Rate Scale Factor",
        dtype=float,
        access=AttrWriteType.READ_WRITE,
        doc="The packet rate scale factor.",
        fget="read_packet_rate_scale_factor",
        fset="write_packet_rate_scale_factor",
    )

    bite_config_data = attribute(
        label="BITE Config data",
        dtype=str,
        access=AttrWriteType.READ,
        doc="A set of BITE config data.",
        fget="read_bite_config_data",
    )

    cbf_input_data = attribute(
        label="CBF Input data",
        dtype=str,
        access=AttrWriteType.READ,
        doc="A set of CBF input data.",
        fget="read_cbf_input_data",
    )

    filter_data = attribute(
        label="Filter data",
        dtype=str,
        access=AttrWriteType.READ,
        doc="A set of filter data.",
        fget="read_filter_data",
    )

    enable_resampler = attribute(
        label="Enable resampler",
        dtype=bool,
        access=AttrWriteType.READ_WRITE,
        doc="Controls whether the LSTV resampler delay tracker is enabled. Default is True.",
        fget="read_enable_resampler",
        fset="write_enable_resampler",
    )

    fo_validity_interval = attribute(
        label="FO polynomial interval",
        dtype=float,
        access=AttrWriteType.READ_WRITE,
        doc="FO polynomial interval to be used in the LSTV resampler delay tracker in seconds.",
        fget="read_fo_validity_interval",
        fset="write_fo_validity_interval",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self: ECBite):
        """Initialize BITE Device"""
        super().init_device()
        self.logger.info("Initializing BITE device...")

        # Set default values for tests
        self._packet_rate_scale_factor = 1.0
        self._bite_config_data = ""
        self._cbf_input_data = ""
        self._filter_data = ""
        self._enable_resampler = True
        self._fo_validity_interval = 1 / 128

        # Store of boards that we will be testing with
        self._bite_receptors = []

        # A dict to contain BITE Clients, indexed by the talon target ID
        self._bite_clients = {}

        self.logger.info("BITE device initialized.")

    # ------------------
    # Attributes methods
    # ------------------

    def read_packet_rate_scale_factor(self: ECBite) -> float:
        """Return the packet_rate_scale_factor attribute"""
        return self._packet_rate_scale_factor

    def write_packet_rate_scale_factor(
        self, packet_rate_scale_factor: float
    ) -> None:
        """Write the packet_rate_scale_factor attribute"""
        self._packet_rate_scale_factor = packet_rate_scale_factor

    def read_bite_config_data(self: ECBite) -> str:
        """Return the bite_config_data attribute"""
        return self._bite_config_data

    def read_cbf_input_data(self: ECBite) -> str:
        """Return the cbf_input_data attribute"""
        return self._cbf_input_data

    def read_filter_data(self: ECBite) -> str:
        """Return the filter_data attribute"""
        return self._filter_data

    def read_enable_resampler(self: ECBite) -> bool:
        """Return the enable_resampler attribute"""
        return self._enable_resampler

    def write_enable_resampler(self, enable_resampler: bool) -> None:
        """Write the enable_resampler attribute"""
        self._enable_resampler = enable_resampler

    def read_fo_validity_interval(self: ECBite) -> float:
        """Return the fo_validity_interval attribute"""
        return self._fo_validity_interval

    def write_fo_validity_interval(self, fo_validity_interval: float) -> None:
        """Write the fo_validity_interval attribute"""
        self._fo_validity_interval = fo_validity_interval

    # --------
    # Commands
    # --------

    @command(
        dtype_in="DevString",
        doc_in="The entire set of BITE configurations loaded in as a string",
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a return code and a string message indicating status",
    )
    def load_bite_config_data(
        self: ECBite, bite_config_data: str
    ) -> DevVarLongStringArray:
        """Load BITE config data"""

        # json schema validation done by system-tests
        try:
            bite_config_json = json.loads(bite_config_data)["bite_configs"]
            self._bite_config_data = json.dumps(bite_config_json)

        except ValueError as e:
            log_msg = f"Invalid JSON syntax in {bite_config_data}: {e}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        except KeyError as e:
            log_msg = f"Missing key '{e}' in {bite_config_data}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        except Exception as e:
            log_msg = f"Failed to load BITE config data: {e}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        log_msg = "Successfully loaded BITE config data."
        self.logger.info(log_msg)
        return [[ResultCode.OK], [log_msg]]

    @command(
        dtype_in="DevString",
        doc_in="A specific set of CBF input data loaded in as a string",
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a return code and a string message indicating status",
    )
    def load_cbf_input_data(
        self: ECBite, cbf_input_data: str
    ) -> DevVarLongStringArray:
        """Load the specific set of CBF input data"""

        # json schema validation done by system-tests
        try:
            cbf_input_data_json = json.loads(cbf_input_data)
            self._cbf_input_data = json.dumps(cbf_input_data_json)

        except ValueError as e:
            log_msg = f"Invalid JSON syntax in {cbf_input_data}: {e}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        except Exception as e:
            log_msg = f"Failed to load CBF input data: {e}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        log_msg = "Successfully loaded CBF input data."
        self.logger.info(log_msg)
        return [[ResultCode.OK], [log_msg]]

    @command(
        dtype_in="DevString",
        doc_in="The entire set of filter configurations loaded in as a string",
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a return code and a string message indicating status",
    )
    def load_filter_data(
        self: ECBite, filter_data: str
    ) -> DevVarLongStringArray:
        """Load filter data"""

        # json schema validation done by system-tests
        try:
            filter_json = json.loads(filter_data)
            self._filter_data = json.dumps(filter_json)

        except ValueError as e:
            log_msg = f"Invalid JSON syntax in {filter_data}: {e}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        except Exception as e:
            log_msg = f"Failed to load filter data: {e}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        log_msg = "Successfully loaded filter data."
        self.logger.info(log_msg)
        return [[ResultCode.OK], [log_msg]]

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a return code and a string message indicating status",
    )
    def generate_bite_data(self: ECBite) -> DevVarLongStringArray:
        """
        Using the data set in the BITE TANGO device, generates tests to be run on the boards.
        Uses threading to be able to work on multiple boards at once.
        """
        # Clear out past data for fresh generation
        self._bite_receptors = []
        self._bite_clients = {}

        # json schema validation done by system-tests
        try:
            cbf_input_json = json.loads(self._cbf_input_data)
            bite_config_json = json.loads(self._bite_config_data)
            filters_json = json.loads(self._filter_data)
        except Exception as e:
            log_msg = f"Failed to load the necessary input data: {e}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        try:
            self._bite_receptors = cbf_input_json["receptors"]
        except KeyError as e:
            log_msg = f"Missing key '{e}' in {cbf_input_json}"
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        # Generate BITE data utilizing the set Tango attributes per receptor
        def talon_bite_config_thread(receptor: dict) -> None:
            """Threaded helper method to configure the BITE client"""

            receptor_id = receptor["talon"]

            # Add BITE client using talon number for server name, no UML logging
            self._bite_clients[receptor_id] = BiteClient(
                inst=receptor["talon"],
                dish_id=receptor.get("dish_id"),
                bite_config_id=receptor.get("bite_config_id"),
                bite_configs=bite_config_json,
                filters=filters_json,
                freq_offset_k=receptor.get("sample_rate_k"),
                enable_resampler=self._enable_resampler,
                fo_validity_interval=self._fo_validity_interval,
                log_to_UML=False,
                logger=self.logger,
            )

            self.logger.info("BITE client initialized.")

            # this is optional. If not provided, we will use coefficients of 0's
            delay_source_files = receptor.get("delay_source_files", None)
            self._bite_clients[receptor_id].configure_bite(
                delay_source_files=delay_source_files,
            )

        # Spin up threads for each BITE client using config thread funct
        with concurrent.futures.ThreadPoolExecutor() as executor:
            self.logger.info("BITE client threads spinup...")
            futures = [
                executor.submit(talon_bite_config_thread, receptor)
                for receptor in self._bite_receptors
            ]
            # Await results from each thread
            results = [f.result() for f in futures]
            self.logger.debug(results)

        log_msg = "BITE Receptors successfully configured!"
        self.logger.info(log_msg)
        return [[ResultCode.OK], [log_msg]]

    # Starts LSTV replay using a configured BITE client
    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a return code and a string message indicating status",
    )
    def start_lstv_replay(self: ECBite) -> DevVarLongStringArray:
        """Starts LSTV replay for configured boards as chosen in the BITE device"""
        self.logger.info("Starting LSTV replay...")

        try:
            # The current time is used if delay source CSV files
            # are not used. All LSTV replays should start with
            # the same time offset.
            SKA_EPOCH = "1999-12-31T23:59:28Z"
            ska_epoch_utc = Time(SKA_EPOCH, scale="utc")
            ska_epoch_tai = ska_epoch_utc.unix_tai
            self.logger.info(f"ska_epoch_utc = {ska_epoch_utc}")
            self.logger.info(f"ska_epoch_tai = {ska_epoch_tai}")

            start_utc_time = Time(datetime.now(timezone.utc), scale="utc")
            self.logger.info(f"start_utc_time = {start_utc_time}")
            self.logger.info(
                f"start_utc_time.unix_tai = {start_utc_time.unix_tai}"
            )
            current_utc_time_offset = start_utc_time.unix_tai - ska_epoch_tai

            # Iterate over board clients
            for receptor in self._bite_receptors:
                # Call the BITE device command
                receptor_id = receptor["talon"]

                self._bite_clients[receptor_id].config_lstv_replay(
                    self._packet_rate_scale_factor,
                    current_utc_time_offset,
                )
        except Exception as e:
            log_msg = repr(e)
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        # try to start LSTV replay on all the boards at around the same time
        def start_lstv_replay_thread(client):
            client.start_lstv_replay()

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(
                    start_lstv_replay_thread,
                    self._bite_clients[receptor["talon"]],
                )
                for receptor in self._bite_receptors
            ]
            [f.result() for f in futures]  # wait for completion

        log_msg = "Successfully started LSTV replay."
        self.logger.info(log_msg)
        return [[ResultCode.OK], [log_msg]]

    # Stops LSTV replay using a configured BITE client
    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a return code and a string message indicating status",
    )
    def stop_lstv_replay(self: ECBite) -> DevVarLongStringArray:
        """Stops LSTV replay for configured boards"""
        self.logger.info("Stopping LSTV replay...")
        try:
            for receptor in self._bite_receptors:
                # Call the BITE device command
                receptor_id = receptor["talon"]

                self._bite_clients[receptor_id].stop_lstv_replay()
        except Exception as e:
            log_msg = repr(e)
            self.logger.error(log_msg)
            return [[ResultCode.FAILED], [log_msg]]

        log_msg = "Successfully stopped LSTV replay."
        self.logger.info(log_msg)
        return [[ResultCode.OK], [log_msg]]


def main(args=None, **kwargs):
    # PROTECTED REGION ID(ECBite.main) ENABLED START #
    return run((ECBite,), args=args, **kwargs)
    # PROTECTED REGION END # // ECBite.main


if __name__ == "__main__":
    main()
