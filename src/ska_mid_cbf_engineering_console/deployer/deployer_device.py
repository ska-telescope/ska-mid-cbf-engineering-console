# -*- coding: utf-8 -*-
#
# This file is part of the Mid.CBF MCS project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""
TANGO device class for deployment of the Engineering Console
"""

from __future__ import annotations

import json
import logging

# Tango Imports
from ska_tango_base import SKABaseDevice
from tango import ApiUtil, AttrWriteType, Except
from tango.server import attribute, command, run

# Python Imports
from ska_mid_cbf_engineering_console.deployer.midcbf_deployer import (
    FspMode,
    configure_tango_db,
    db_device_check,
    download_ds_binaries,
    download_fpga_bitstreams,
    generate_talondx_config,
    install_remotes,
)

__all__ = ["ECDeployer", "main"]

CONAN_REMOTES = "/app/src/ska_mid_cbf_engineering_console/deployer/conan_local/remotes.json"
TALONDX_CONFIG_DIR = (
    "/app/src/ska_mid_cbf_engineering_console/deployer/talondx_config/"
)
DS_BINARIES_JSON = "ds_binaries.json"
FPGA_BITSTREAMS_JSON = "fpga_bitstreams.json"
CONFIG_COMMANDS_JSON = "config_commands.json"
TANGO_DB_JSON = "tango_db.json"

# This is how many boards we can create configurations for currently.
# Can be increased as needed.
MAX_BOARDS = 8

# This is how big we want a group of boards with the same FSP Mode to be
# [1,FSP_GROUP_SIZE] will be one set of board, [FSP_GROUP_SIZE+1,2*FSP_GROUP_SIZE]
# should be another set, etc..
# Change below when the allowable limit increases
FSP_GROUP_SIZE = 4


class ECDeployer(SKABaseDevice):
    def init_device(self):
        super().init_device()
        self.logger = logging.getLogger(__name__)

    # Default value all CORR for fspModeList
    _fsp_mode_list = [FspMode.CORR for _ in range(MAX_BOARDS)]

    # Defaults Target Talons to MAX_BOARDS
    _target_talons = [i + 1 for i in range(MAX_BOARDS)]

    # Flag to indicate if we want to validate the board configurations pass to
    # the deployer for Configuration json generation.  Defaults to True.
    _validate_board_configuration = True

    # ----------
    # Attributes
    # ----------

    # read/write spectrum attribute to set talon board indices
    targetTalons = attribute(
        dtype=("int",),
        max_dim_x=MAX_BOARDS,
        access=AttrWriteType.READ_WRITE,
        label="Target Talons",
        doc="Target Talons: Setting Talon Board Indices",
    )

    dsBinaries = attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
        label="Datastream Binaries",
        doc="Storing ds_binaries JSON data",
    )

    fpgaBitstreams = attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
        label="FPGA Bitstreams",
        doc="Storing fpga_bitstreams JSON data",
    )

    # config_commands: for copying into the controller pod to pass the appropriate HPS Master devices
    configCommands = attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
        label="Config Commands",
        doc="Storing config_commands JSON data",
    )

    # tango_db: for populating the tango database
    tangoDB = attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
        label="Tango DB",
        doc="Storing tango_db JSON data",
    )

    @attribute(
        dtype=(FspMode,),
        max_dim_x=MAX_BOARDS,
        access=AttrWriteType.READ_WRITE,
        label="FSP Function Mode List",
        doc=(
            "List of FSP function mode used to determine the which device servers to load onto the boards. "
            r"\nEach index in fspModeList corresponds to a board in targetTalons. "
            r"\nSize of this list must be the same as targetTalons"
        ),
    )
    def fspModeList(self) -> list[FspMode]:
        """
        Reads and return the value in fspModeList

        :return: the value in fspModeList
        :rtype: list[FspMode]
        """
        return self._fsp_mode_list

    @fspModeList.write
    def fspModeList(self, values: list[FspMode]) -> None:
        """
        Writes to the fspModeList Attribute.
        Each index of fspModeList represent a target Function Mode for a board in targetTalon.

        :param value: List of FspMode that is one of: CORR, PST
        """

        if len(values) != len(self._target_talons):
            msg = (
                "The list of FSP Modes provided is not the same size as the given targetTalons list. "
                "Given values not set."
            )
            Except.throw_exception(
                "write_fspModeList Failed", msg, "write_fspModeList()"
            )
            return

        temp_fsp_mode_list = [FspMode(value) for value in values]

        # Validates each of the FSP Mode given and convert them to a FSPMode Enum
        # Mostly a issue where Jive is passing in ints to represent the FSPMode and
        # Functions further down the pipeline is not recognizing it as enum
        valid_configuration: bool = self.validate_fspModeList_input(
            temp_fsp_mode_list
        )
        if valid_configuration:
            self._fsp_mode_list = temp_fsp_mode_list
        else:
            msg = "FSP Modes given is not valid. See log for detail."
            Except.throw_exception(
                "write_fspModeList Failed", msg, "write_fspModeList()"
            )
            return

    @attribute(
        dtype="DevBoolean",
        access=AttrWriteType.READ_WRITE,
        label="Enable Board List/Mode Configuration Validation",
        doc=(
            "Flag to enable the validations with the configuration of which"
            "boards gets assigned to which FSP mode. Defaults to True"
        ),
    )
    def validateBoardModeConfiguration(self) -> bool:
        """
        Reads and return the value of the validateBoardModeConfiguration flag.

        Returning True indicates that the Deployer will validate incoming FSP
        mode assignments in accordance with the AA 0.5/1.0 supported configurations.

        :return: the validateBoardModeConfiguration flag
        :rtype: bool
        """
        return self._validate_board_configuration

    @validateBoardModeConfiguration.write
    def validateBoardModeConfiguration(self, value: bool):
        """
        Writes to the validateBoardModeConfiguration flag.

        When True, the Deployer will validate incoming FSP mode assignments
        in accordance with the AA 0.5/1.0 supported configurations.

        When False, the Deployer will not validate those assignments.

        Logs a warning level message when the attribute is set to False.

        :param value: Set the flag to True/False
        """
        if value is False:
            msg = (
                "Setting validateBoardModeConfiguration to False. "
                "Using unsupported configurations may result in "
                "unexpected system behaviour."
            )

            self.logger.warning(msg)

        self._validate_board_configuration = value

    # ------------------
    # Attributes methods
    # ------------------
    def read_targetTalons(self) -> list[int]:
        return self._target_talons

    def write_targetTalons(self, value: list[int]) -> None:
        self._target_talons = value

    def read_dsBinaries(self) -> str:
        return self._ds_binaries

    def write_dsBinaries(self, value: str) -> None:
        self._ds_binaries = value

    def read_fpgaBitstreams(self) -> str:
        return self._fpga_bitstreams

    def write_fpgaBitstreams(self, value: str) -> None:
        self._fpga_bitstreams = value

    def read_configCommands(self) -> str:
        return self._config_commands

    def write_configCommands(self, value: str) -> None:
        self._config_commands = value

    def read_tangoDB(self) -> str:
        return self._tango_db

    def write_tangoDB(self, value: str) -> None:
        self._tango_db = value

    # --------
    # Commands
    # --------

    @command
    def generate_config_jsons(self: ECDeployer) -> None:
        """
        Generates configuration JSONs and saves them to the talondx_config directory,
        then loads those JSONs into their respective attributes:
        ds_binaries.json, fpga_bitstreams.json, config_commands.json, and tango_db.json.
        This function also copies config_commands.json into the shared kubernetes
        storage provisions with CBFController in the artifacts directory for MCS On()/Off().
        """
        self.logger.info(
            "Generate ds_binaries, fpga_bitstreams, config_commands, and tango_db JSON config files"
        )
        generate_talondx_config(self._target_talons, self._fsp_mode_list)

        with open(TALONDX_CONFIG_DIR + DS_BINARIES_JSON) as file:
            self._ds_binaries = json.dumps(json.load(file))

        with open(TALONDX_CONFIG_DIR + FPGA_BITSTREAMS_JSON) as file:
            self._fpga_bitstreams = json.dumps(json.load(file))

        # NOTE: The ordering of the devices in the Config Commands matters
        # ex. Syncbuffer for PST needs to be near the top of the list or
        # it will segfaults when HPS Master tries to start it up. Hypothesis is
        # that certain devices needs talk to each other during startup.
        with open(TALONDX_CONFIG_DIR + CONFIG_COMMANDS_JSON) as file:
            self._config_commands = json.dumps(json.load(file))

        with open(TALONDX_CONFIG_DIR + TANGO_DB_JSON) as file:
            self._tango_db = json.dumps(json.load(file))

    @command
    def download_artifacts(self: ECDeployer) -> None:
        """
        Download artifacts from CAR for HPS deployment onto Talon boards
        """
        self.logger.info("Download Artifacts")
        with open(CONAN_REMOTES) as file:
            install_remotes(json.load(file))

        download_ds_binaries(json.loads(self._ds_binaries), self.logger)
        download_fpga_bitstreams(
            json.loads(self._fpga_bitstreams), self.logger
        )

    @command
    def configure_db(self: ECDeployer) -> None:
        """
        Configures the TANGO DB. If the DB has already been configured during the same run, will reconfigure on the chance there have been changes.
        """
        self.logger.info(
            f'Configure DB - TANGO_HOST = {ApiUtil.get_env_var("TANGO_HOST")}'
        )
        configure_tango_db(
            json.loads(self._tango_db),
            json.loads(self._config_commands),
            self.logger,
        )

    @command
    def device_check(self: ECDeployer) -> None:
        """
        Pings all devices in the TANGO DB to ensure they are alive
        """
        self.logger.info("Check device status:")
        db_device_check(self.logger)

    @command(
        dtype_in="DevString",
        doc_in="Board and FSP Mode configuration",
    )
    def configure_boards_and_fsp_mode(self: ECDeployer, argin: str) -> None:
        """
        Parse the given JSON string and set targetTalons and FspModeList.
        The JSON string must be in key:value format of Board ID (str):FSP Mode (str).
        Size of the parsed object must be less than MAX_BOARDS.
        Each Board ID is a numeric string in the range  [1,MAX_BOARDS].
        FSP Mode is a string representing an Enum in [CORR, PST].

        Throws an exception when the argin provides more boards than allowed with MAX_BOARDS.

        :param argin: a JSON string containing the Board ID (int): FSP Mode (str) mapping.

        :raises Tango Except: if the JSON provided more boards than allowed
        """
        pair_dictionary = json.loads(argin)
        if len(pair_dictionary) > MAX_BOARDS:
            msg = (
                "The JSON provided more boards than allowed."
                f"Max Boards Supported: {MAX_BOARDS}"
            )
            Except.throw_exception(
                "configure_boards_and_fsp_mode Failed",
                msg,
                "configure_boards_and_fsp_mode()",
            )
            return

        boards = []
        fspModes = []
        for boardID, fspModeStr in pair_dictionary.items():
            boards.append(int(boardID))
            fspModes.append(FspMode[fspModeStr])

        valid_configuration: bool = self.validate_fspModeList_input(fspModes)
        if valid_configuration:
            self._fsp_mode_list = fspModes
            self.write_targetTalons(boards)
        else:
            msg = "FSP Modes given is not valid. Boards and FSP modes not set. See log for detail."
            Except.throw_exception(
                "configure_boards_and_fsp_mode Failed",
                msg,
                "configure_boards_and_fsp_mode()",
            )
            return

    # --------
    # Helpers
    # --------

    def validate_fspModeList_input(self, fspModes: list[FspMode]) -> bool:
        """
        Returns True if the fspModes list contains valid assignments of FSP modes
        to boards in targetTalons.

        Supported assignments of FSP modes to boards according to AA 0.5/1.0 are:
         - 4 CORR (Boards 1-4),
         - 4 PST  (Boards 1-4), or
         - 4 CORR (Boards 1-4) and 4 PST (Boards 5-8)

        This validation can be disable for testing purposes by setting the
        validateBoardModeConfiguration attribute to False.

        :param fspModes: a list of fspModes

        :return: a bool indicating if the fspMode assignemnts are valid or not.
        :rtype: bool

        """

        # Disables the valdiation.  Use for testing.
        if self._validate_board_configuration is False:
            msg = "Validation of the FSP mode assignment is disabled."
            self.logger.info(msg)
            return True

        fsp_mode_list_size = len(fspModes)
        start_idx, end_idx = 0, 0

        # First check is to make sure that consecutive groupings of FSP_GROUP_SIZE boards
        # contains the same FSP modes
        # For AA 0.5/1.0, FSP_GROUP_SIZE = 4
        while fsp_mode_list_size > 0:
            # Sets the end index for the range we want to check
            # We want to set end_idx to be either the min of size of the list
            # (or size of remaining items that hasn't been checked yet) or FSP_GROUP_SIZE
            # (4 for AA 0.5/1.0)
            end_idx = start_idx + min(fsp_mode_list_size, FSP_GROUP_SIZE)

            # As of AA 1.0, we want the same FSP Mode assigned to boards in groups of 4.
            group_mode = fspModes[start_idx]

            for i in range(start_idx, end_idx):
                if fspModes[i] != group_mode:
                    msg = (
                        f"FSP Modes within a group of {FSP_GROUP_SIZE} boards "
                        "must be the same."
                    )
                    self.logger.error(msg)
                    return False

            # Reduce remaining FSP modes in the list to check
            fsp_mode_list_size = fsp_mode_list_size - end_idx

            start_idx = end_idx

        # Below checks that the FSP modes are grouped together based on supported
        # assignments when there are more than FSP_GROUP_SIZE of assignment (4 for AA 0.5/1.0).
        # See docstring for details on the supported assignment.

        fsp_mode_list_size = len(fspModes)

        # Do the check if there is more than 1 grouping (> 4 boards).
        if fsp_mode_list_size > FSP_GROUP_SIZE:
            # This checks that each grouping sets a different FSP mode.
            if fspModes[0] == fspModes[FSP_GROUP_SIZE]:
                msg = (
                    f"FSP Modes must be a group of {FSP_GROUP_SIZE} CORR, "
                    f"{FSP_GROUP_SIZE} PST, or "
                    f"{FSP_GROUP_SIZE} CORR and {FSP_GROUP_SIZE} PST"
                )
                self.logger.error(msg)
                return False
            # PST modes must be in the second grouping if we are using an 8 board configuration.
            elif (
                fspModes[0] == FspMode.PST
                and fspModes[FSP_GROUP_SIZE] == FspMode.CORR
            ):
                msg = (
                    f"For AA 0.5/1.0, using {FSP_GROUP_SIZE*2} boards must have "
                    f"Boards 1-{FSP_GROUP_SIZE} configured to CORR and "
                    f"Boards {FSP_GROUP_SIZE+1}-{FSP_GROUP_SIZE*2} configured to PST"
                )
                self.logger.error(msg)
                return False
            else:
                return True
        else:
            return True


def main(args=None, **kwargs):
    return run((ECDeployer,), args=args, **kwargs)


if __name__ == "__main__":
    main()
