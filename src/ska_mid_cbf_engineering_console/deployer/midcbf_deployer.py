#!/usr/bin/env python3
import copy
import getpass
import json
import logging
import os
import re
import tarfile
import zipfile
from collections import OrderedDict
from enum import IntEnum

import requests
import tango

from ska_mid_cbf_engineering_console.deployer.conan_local.conan_wrapper import (
    ConanWrapper,
)
from ska_mid_cbf_engineering_console.deployer.nrcdbpopulate.dbPopulate import (
    DbPopulate,
)

LOG_FORMAT = "[midcbf_deployer.py: line %(lineno)s]%(levelname)s: %(message)s"


class FspMode(IntEnum):
    CORR = 0
    PST = 1


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    OK = "\x1b[6;30;42m"
    FAIL = "\x1b[0;30;41m"
    ENDC = "\x1b[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class Version:
    """
    Class to facilitate extracting and comparing version numbers in filenames.

    :param filename: string containing a version substring in the x.y.z format, where x,y,z are numbers.
    """

    def __init__(self, filename):
        [ver_x, ver_y, ver_z] = re.findall("[0-9]+", filename)
        self.X = int(ver_x)
        self.Y = int(ver_y)
        self.Z = int(ver_z)

    def match(self, ver):
        """
        Compare two Version object and return true if the versions match.

        :param ver: Version object being compared to this one.
        """
        return self.X == ver.X and self.Y == ver.Y and self.Z == ver.Z


PROJECT_DIR = "/app/src/ska_mid_cbf_engineering_console/deployer/"
ARTIFACTS_DIR = "/app/src/ska_mid_cbf_engineering_console/deployer/artifacts/"
TALONDX_CONFIG_DIR = (
    "/app/src/ska_mid_cbf_engineering_console/deployer/talondx_config/"
)
FPGA_BITSTREAMS_JSON = "fpga_bitstreams.json"
DS_BINARIES_JSON = "ds_binaries.json"
CONFIG_COMMANDS_JSON = "config_commands.json"
TANGO_DB_JSON = "tango_db.json"
TALONDX_CONFIG_JSON = "talondx-config.json"
DEVICE_LIST_JSON = "device_list.json"
TALONDX_BOARDMAP_JSON_PATH = (
    "/app/src/ska_mid_cbf_engineering_console/deployer/talondx_config/"
)
BOARDMAP_JSON_FILE = "talondx_boardmap.json"

# To be filled in with .format()
DS_HPS_MASTER_FQDN_TEMPLATE = "talondx-{target_talon}/hpsmaster/hps-{board}"
SERVER_INSTANCE_TEMPLATE = "talon{board}_test"

DBPOPULATE_SCHEMA_JSON = "nrcdbpopulate/schema/dbpopulate_schema.json"

# TODO: Currently hardcoded for Corr, need to make a design change so that
# it can accommendate bitstreams json for other FSP modes
BITSTREAM_DICTIONARY = os.path.join("fpga-talon", "bin")


DOWNLOAD_CHUNK_BYTES = 1024
GITLAB_PROJECTS_URL = "https://gitlab.drao.nrc.ca/api/v4/projects/"
GITLAB_API_HEADER = {
    "PRIVATE-TOKEN": f'{os.environ.get("GIT_ARTIFACTS_TOKEN")}'
}

# Name of the matching FSP Controller Device for a Given FSP Mode
FSP_HPS_DEVICE_NAME = {
    FspMode.CORR: "DsFspCorrController",
    FspMode.PST: "DsFspPSTController",
}


def generate_talondx_config(
    boards_list: list[int],
    boardmap_list: list[FspMode],
) -> None:
    """
    Reads and displays the state and status of each HPS Tango device running on the
    Talon DX boards, as specified in the configuration commands -- ref `"config_commands"`
    in four JSON files: fpga_bitstreams, ds_binaries, config_commands, and tango_db.

    :param boards_list: List of boards to deploy
    :type boards_list: list[int]
    :param boardmap_list: The boardmap configuration to use for the board(s).  One of enum[CORR,PST]
    :type boardmap_list: list[FspMode]
    """

    with open(
        TALONDX_BOARDMAP_JSON_PATH + BOARDMAP_JSON_FILE,
        "r",
    ) as config_map:
        config_map_json = json.load(config_map)
        fpga_bitstreams = {
            "fpga_bitstreams": config_map_json["fpga_bitstreams"]
        }
        ds_binaries = {"ds_binaries": config_map_json["ds_binaries"]}
        config_commands = {}
        tango_db = {}
        board_function_mode_list = boardmap_list
        talondx_config_commands = []
        for i in range(len(boards_list)):
            board = boards_list[i]
            function_mode: FspMode = board_function_mode_list[i]
            board_config = copy.deepcopy(
                config_map_json["config_commands"][function_mode.name]
            )
            target_talon = str(board).zfill(3)
            board_config["target"] = target_talon
            board_config[
                "ds_hps_master_fqdn"
            ] = DS_HPS_MASTER_FQDN_TEMPLATE.format(
                target_talon=target_talon, board=board
            )
            board_config["server_instance"] = SERVER_INSTANCE_TEMPLATE.format(
                board=board
            )
            talondx_config_commands.append(board_config)
        config_commands["config_commands"] = talondx_config_commands

        db_servers_list = []
        for db_server in config_map_json["tango_db"]["db_servers"]:
            for i in range(len(boards_list)):
                board = boards_list[i]
                function_mode: FspMode = board_function_mode_list[i]
                db_server_tmp = copy.deepcopy(db_server)
                if db_server["server"] == "dshpsmaster":
                    db_server_tmp["deviceList"][0]["id"] = str(board)
                    if (
                        "TalonStatusFQDN"
                        in db_server_tmp["deviceList"][0]["devprop"]
                    ):
                        db_server_tmp["deviceList"][0]["devprop"][
                            "TalonStatusFQDN"
                        ] = db_server_tmp["deviceList"][0]["devprop"][
                            "TalonStatusFQDN"
                        ].replace(
                            "<device>", "talondx-00" + str(board)
                        )
                # From CIP-3184: We want to deploy only the FSP Devices related to the FSP Mode
                if db_server["server"] == "ska-mid-cbf-fsp-app":
                    temp_device_list = []
                    for device in db_server_tmp["deviceList"]:
                        if device["class"] == "DsFspController":
                            temp_device_list.append(device)
                        elif (
                            device["class"]
                            == FSP_HPS_DEVICE_NAME[function_mode]
                        ):
                            temp_device_list.append(device)
                    db_server_tmp["deviceList"] = temp_device_list

                if db_server["server"] in [
                    "ska-mid-cbf-vcc-app",
                    "ska-mid-cbf-fsp-app",
                    "dshostlutstage1",
                ]:
                    for device in db_server_tmp["deviceList"]:
                        for devprop in device["devprop"]:
                            if "FQDN" in devprop:
                                device["devprop"][devprop] = device["devprop"][
                                    devprop
                                ].replace(
                                    "<device>", "talondx-00" + str(board)
                                )
                            if "host_lut_stage_2_device_name" in devprop:
                                device["devprop"][devprop] = device["devprop"][
                                    devprop
                                ].replace(
                                    "<device>", "talondx-00" + str(board)
                                )
                if db_server["server"] == "dsrdmarx":
                    db_server_tmp["deviceList"][0]["devprop"][
                        "rdmaTxTangoDeviceName"
                    ] = db_server_tmp["deviceList"][0]["devprop"][
                        "rdmaTxTangoDeviceName"
                    ].replace(
                        "<device>", "talondx-00" + str(board)
                    )
                    db_server_tmp["deviceList"][0]["alias"] = "rx" + str(board)
                if db_server["server"] != "talondx_log_consumer":
                    db_server_tmp[
                        "instance"
                    ] = SERVER_INSTANCE_TEMPLATE.format(board=board)
                    if db_server["server"] != "dsrdmarx":
                        db_server_tmp["device"] = "talondx-00" + str(board)
                    db_servers_list.append(db_server_tmp)
            if db_server["server"] == "talondx_log_consumer":
                db_server_tmp = copy.deepcopy(db_server)
                db_servers_list.append(db_server_tmp)
        tango_db["tango_db"] = {"db_servers": db_servers_list}

        fpga_bitstreams_file = open(
            TALONDX_CONFIG_DIR + FPGA_BITSTREAMS_JSON, "w"
        )
        ds_binaries_file = open(TALONDX_CONFIG_DIR + DS_BINARIES_JSON, "w")
        talondx_config_commands_file = open(
            TALONDX_CONFIG_DIR + CONFIG_COMMANDS_JSON, "w"
        )
        tango_db_file = open(TALONDX_CONFIG_DIR + TANGO_DB_JSON, "w")

        # This is for loading into MCS during the On()/Off() sequence.
        # Could be depreciated in favour of device proxies if this seems expedient in the future.
        talondx_config_file = open(ARTIFACTS_DIR + TALONDX_CONFIG_JSON, "w")

        json.dump(fpga_bitstreams, fpga_bitstreams_file, indent=6)
        json.dump(ds_binaries, ds_binaries_file, indent=6)
        json.dump(config_commands, talondx_config_commands_file, indent=6)
        json.dump(tango_db, tango_db_file, indent=6)
        json.dump(config_commands, talondx_config_file, indent=6)

        fpga_bitstreams_file.close()
        ds_binaries_file.close()
        talondx_config_commands_file.close()
        tango_db_file.close()
        talondx_config_file.close()


# TODO: Need to make multiple pass to configure the DB for various FSP Devices
# Current setup assumes that the DeTri is universal for all FSP Modes, which is
# not the case for Corr and PST HPS devices
def configure_tango_db(
    inputjson: dict, config_commands: dict, logger: logging.Logger
):
    """
    Configure the Tango DB with devices specified in the talon-config JSON file.
    :param inputjson: JSON string containing the device server specifications for populating the Tango DB
    :param config_commands: A dictionary representing a JSON string for the configurations to be deployed
    :param logger: a logger object
    """
    # Get the name of all the unique Bitstream JSON files that we need
    bitstream_json_file_names = {
        config_command["fpga_json_name"]
        for config_command in config_commands["config_commands"]
    }

    # Need to build the DeTrI memonics for each bitstream requested
    templates = {}
    for bitstream_json_file_name in bitstream_json_file_names:
        bitstream_file_path = os.path.join(
            ARTIFACTS_DIR, BITSTREAM_DICTIONARY, bitstream_json_file_name
        )

        bitstream_f = open(bitstream_file_path, "r")
        bitstream_json = json.load(bitstream_f)
        templates.update(bitstream_json["DeTrI"])

    # if configure_tango_db() is run multiple times in a row,
    # need to remove the existing device_list.json file first
    # otherwise it will append duplicate entries to the same file
    try:
        os.remove(ARTIFACTS_DIR + DEVICE_LIST_JSON)
        logger.info(
            "Existing device_list.json found from previous execution "
            "of configure_tango_db. File has been deleted."
        )
    except FileNotFoundError:
        logger.info("File device_list.json not found. It will be created.")

    for server in inputjson["tango_db"]["db_servers"]:
        # TODO: make schema validation part of the dbPopulate class
        with open(PROJECT_DIR + DBPOPULATE_SCHEMA_JSON, "r") as sch:
            # schemajson = json.load(sch, object_pairs_hook=OrderedDict)
            json.load(sch, object_pairs_hook=OrderedDict)
            sch.seek(0)

        # try:
        #     logger.info("Validation step")
        #     jsonschema.validate(server, schemajson)
        # except ValidationError as error:
        #     handleValidationError(error, server)
        #     exit(1)

        dbpop = DbPopulate(server, templates)

        try:
            device_list = []
            with open(ARTIFACTS_DIR + DEVICE_LIST_JSON, "r") as f:
                data = json.load(f)
                for i in data["devices"]:
                    device_list.append(i)
        except FileNotFoundError:
            device_list = []

        # Remove and add to ensure any previous record is overwritten
        dbpop.process(mode="remove")
        dbpop.process(mode="add", device_list=device_list)

        with open(ARTIFACTS_DIR + DEVICE_LIST_JSON, "w") as f:
            data = {"devices": device_list}
            json.dump(data, f)
            logger.info("Tango DB configured")


def download_git_artifacts(
    git_api_url: str, name: str, logger: logging.Logger
):
    """
    Downloads artifacts if they appear in a Gitlab respository, as opposed to CAR.
    :param git_api_url: The URL to hit for the artifacts we need
    :param name: The name of the specific artifact we need from the URL
    :param logger: a logging object passed from the TANGO Device
    """
    response = requests.head(url=git_api_url, headers=GITLAB_API_HEADER)

    if response.status_code == requests.codes.ok:  # pylint: disable=no-member
        total_bytes = int(response.headers["Content-Length"])

        response = requests.get(
            git_api_url, headers=GITLAB_API_HEADER, stream=True
        )
        ds_artifacts_dir = os.path.join(ARTIFACTS_DIR, name)
        filename = os.path.join(ds_artifacts_dir, "artifacts.zip")
        bytes_downloaded = 0
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "wb") as fd:
            for chunk in response.iter_content(
                chunk_size=DOWNLOAD_CHUNK_BYTES
            ):
                fd.write(chunk)
                bytes_downloaded = min(
                    bytes_downloaded + DOWNLOAD_CHUNK_BYTES, total_bytes
                )
                per_cent = round(bytes_downloaded / total_bytes * 100.0)
                logger.info(
                    f"Downloading {total_bytes} bytes to {os.path.relpath(filename, PROJECT_DIR)} "
                    f"[{bcolors.OK}{per_cent:>3} %{bcolors.ENDC}]",
                    end="\r",
                )
            logger.info("")
        logger.info("Extracting files... ", end="")
        with zipfile.ZipFile(filename, "r") as zip_ref:
            zip_ref.extractall(ds_artifacts_dir)
        logger.info(f"{bcolors.OK}done{bcolors.ENDC}")
    else:
        logger.error(
            f"{bcolors.FAIL}status: {response.status_code}{bcolors.ENDC}"
        )


def download_raw_artifacts(
    api_url: str, name: str, filename: str, logger: logging.Logger
):
    """
    Used to download .tar.gz files for bitstreams to install into the Talon boards
    :param api_url: The URL to hit looking for the artifact
    :param name: The name of the file we're looking for
    :param filename: The specified filename
    :param logger: A logging object passed from the TANGO Device.
    """
    # TODO: factorize common code with download_git_artifacts
    response = requests.head(url=api_url, auth=("", ""))
    if response.status_code == requests.codes.ok:  # pylint: disable=no-member
        total_bytes = int(response.headers["Content-Length"])
        response = requests.get(api_url, auth=("", ""), stream=True)
        artifacts_dir = os.path.join(ARTIFACTS_DIR, name)
        filename = os.path.join(artifacts_dir, filename)
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "wb") as fd:
            logger.info(
                f"Downloading {total_bytes} bytes to {os.path.relpath(filename, PROJECT_DIR)}"
            )
            for chunk in response.iter_content(
                chunk_size=DOWNLOAD_CHUNK_BYTES
            ):
                fd.write(chunk)

        logger.info("Extracting files... ")
        if tarfile.is_tarfile(filename):
            tar = tarfile.open(filename, "r")
            tar.extractall(artifacts_dir)
            tar.close()
        else:
            with zipfile.ZipFile(filename, "r") as zip_ref:
                zip_ref.extractall(artifacts_dir)
        logger.info(f"{bcolors.OK}Done extracting files{bcolors.ENDC}")
        # TODO: workaround; request permissions change from systems team
        os.chmod(os.path.join(artifacts_dir, "MANIFEST.skao.int"), 0o644)
    else:
        logger.error(
            f"{bcolors.FAIL}File extraction failed - status: {response.status_code}{bcolors.ENDC}"
        )


def download_ds_binaries(
    ds_binaries: dict, logger: logging.Logger, clear_conan_cache=False
):
    """
    Downloads and extracts Tango device server (DS) binaries from Conan packages
    or Git pipeline artifacts.

    :param ds_binaries: JSON string specifying which DS binaries to download.
    :param logger: a logger object
    :param clear_conan_cache: if true, Conan packages are fetched from remote; default true.
    """
    conan = ConanWrapper(ARTIFACTS_DIR)
    logger.info(f"Conan version: {conan.version()}")
    if clear_conan_cache:
        logger.info(f"Conan local cache: {conan.search_local_cache()}")
        logger.info(
            f"Clearing Conan local cache... {conan.clear_local_cache()}"
        )
    logger.info(f"Conan local cache: {conan.search_local_cache()}")

    for ds in ds_binaries["ds_binaries"]:
        logger.info(f"DS Binary: {ds['name']}")

        if ds.get("source") == "conan":
            # Download the specified Conan package
            conan_info = ds.get("conan")
            logger.info(f"Conan info: {conan_info}")
            conan.download_package(
                pkg_name=conan_info["package_name"],
                version=conan_info["version"],
                user=conan_info["user"],
                channel=conan_info["channel"],
                profile=os.path.join(
                    conan.profiles_dir, conan_info["profile"]
                ),
            )
        elif ds.get("source") == "git":
            # Download the artifacts from the latest successful pipeline
            git_info = ds.get("git")
            url = (
                f'{GITLAB_PROJECTS_URL}{git_info["git_project_id"]}/jobs/artifacts/'
                f'{git_info["git_branch"]}/download?job={git_info["git_pipeline_job"]}'
            )
            download_git_artifacts(
                git_api_url=url, name=ds["name"], logger=logger
            )
        else:
            logger.error(f'Error: unrecognized source ({ds.get("source")})')
            exit(-1)

    # Modify the permissions of Artifacts dir so they can be modified/deleted later
    chmod_r_cmd = "chmod -R o=rwx " + ARTIFACTS_DIR
    os.system(chmod_r_cmd)


def download_fpga_bitstreams(fpga_bitstreams: dict, logger: logging.Logger):
    """
    Downloads and extracts FPGA bitstreams from the CAR (Common Artefact Repository),
    or Git pipeline artifacts.

    :param fpga_bitstreams: JSON string specifying which FPGA bitstreams to download.
    :param logger: a logger object
    """
    for fpga in fpga_bitstreams["fpga_bitstreams"]:
        if fpga.get("source") == "raw":
            # Download the bitstream from the raw repo in CAR
            raw_info = fpga.get("raw")
            logger.info(
                f"FPGA bitstream {raw_info['base_filename']}-{fpga['version']}"
            )

            fpga_bitstream_file = (
                f"{raw_info['base_filename']}-{fpga['version']}.tar.gz"
            )

            download_url = f"https://artefact.skao.int/repository/raw-internal/{fpga_bitstream_file}"
            download_raw_artifacts(
                api_url=download_url,
                name="fpga-talon",
                filename=fpga_bitstream_file,
                logger=logger,
            )
            logger.info(
                f"Finished downloading FPGA bitstream: {fpga_bitstream_file}"
            )

        else:
            # Download the artifacts from the latest successful Git pipeline
            git_info = fpga["git"]
            url = (
                f'{GITLAB_PROJECTS_URL}{git_info["git_project_id"]}/jobs/artifacts/'
                f'{git_info["git_branch"]}/download?job={git_info["git_pipeline_job"]}'
            )
            logger.info(f"GitLab API call for bitstream download: {url}")

            # Alternate URL for downloading specific pipeline job
            """
            url = f"{GITLAB_PROJECTS_URL}{git_info['git_project_id']}/jobs/{git_info['git_pipeline_job']}/artifacts"
            logger.info(f"GitLab API call for bitstream download: {url}")
            # https://gitlab.drao.nrc.ca/SKA/Mid.CBF/FW/persona/tdc_vcc_processing/-/jobs/12180
            """
            download_git_artifacts(
                git_api_url=url, name=f"fpga-{fpga['target']}", logger=logger
            )


def db_device_check(logger: logging.Logger):
    """
    Ping all Devices in the TANGO DB to ensure they are up and available to connec to
    :param logger: A logging object passed in from the TANGO Device.
    """
    with open(ARTIFACTS_DIR + DEVICE_LIST_JSON, "r") as f:
        data = f.read()
        device_list = json.loads(data)

        for device in device_list["devices"]:
            # These 3 devices aren't currently implemented but will be eventually..
            if (
                ("vcc-band-3" not in device)
                and ("vcc-band-4" not in device)
                and ("vcc-band-5" not in device)
            ):
                try:
                    dev_proxy = tango.DeviceProxy(device)
                    dev_proxy.ping()
                    logger.info(f"Successfully pinged DeviceProxy ({device})")
                except tango.DevFailed as df:
                    for item in df.args:
                        logger.error(
                            f"Error pinging DeviceProxy {device} : {item.reason} {item.desc}"
                        )
                    continue


def install_remotes(inputjson: dict):
    """
    This automates the process of adding remotes from remotes.json stored in /conan_local/ to the Conan program running on the TANGO Device
    :param: inputjson: The dictionary that includes the remotes to add, parsed beforehand.
    """
    os.system("conan remote remove conancenter")
    for remote in inputjson["remotes"]:
        os.system(
            "conan remote add "
            + remote["name"]
            + " "
            + remote["url"]
            + " "
            + str(remote["verify_ssl"])
        )
    os.system("conan remote list")


if __name__ == "__main__":
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    logger = logging.getLogger("midcbf_deployer.py")
    logger.info(f"User: {getpass.getuser()}")
    logger.info("Hello from Mid CBF Engineering Console Deployer!")
