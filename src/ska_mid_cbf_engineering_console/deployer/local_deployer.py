import argparse
import copy
import json
import logging
import os
import shutil
import subprocess
import sys

from nrcdbpopulate.dbPopulate import DbPopulate
from talondx_config.talondx_config import TalonDxConfig

LOG_FORMAT = "[line %(lineno)s]%(levelname)s: %(message)s"


class LocalDeployer:
    def __init__(self, bitstream, boardmap):
        self._bitstream = bitstream
        self._boardmap = boardmap

        self._temp_dir = "./extract"
        self._config_file = "./talondx-config.json"

    def generate_talondx_config(self, boards):
        boards_list = boards.split(",")
        with open(self._boardmap, "r") as config_map:
            config_map_json = json.load(config_map)
            talondx_config_dict = {
                "ds_binaries": config_map_json["ds_binaries"]
            }
            talondx_config_dict = {
                "fpga_bitstreams": config_map_json["fpga_bitstreams"],
                "ds_binaries": config_map_json["ds_binaries"],
            }
            talondx_config_commands = []
            for board in boards_list:
                talondx_config_commands.append(
                    config_map_json["config_commands"][board]
                )
            talondx_config_dict["config_commands"] = talondx_config_commands
            db_servers_list = []
            for db_server in config_map_json["tango_db"]["db_servers"]:
                for board in boards_list:
                    db_server_tmp = copy.deepcopy(db_server)
                    if db_server["server"] == "dshpsmaster":
                        db_server_tmp["deviceList"][0]["id"] = board
                        if (
                            "TalonStatusFQDN"
                            in db_server_tmp["deviceList"][0]["devprop"]
                        ):
                            db_server_tmp["deviceList"][0]["devprop"][
                                "TalonStatusFQDN"
                            ] = db_server_tmp["deviceList"][0]["devprop"][
                                "TalonStatusFQDN"
                            ].replace(
                                "<device>", "talondx-00" + board
                            )
                    if db_server["server"] in [
                        "ska-mid-cbf-vcc-app",
                        "ska-mid-cbf-fsp-app",
                        "dshostlutstage1",
                    ]:
                        for device in db_server_tmp["deviceList"]:
                            for devprop in device["devprop"]:
                                if "FQDN" in devprop:
                                    device["devprop"][devprop] = device[
                                        "devprop"
                                    ][devprop].replace(
                                        "<device>", "talondx-00" + board
                                    )
                                if "host_lut_stage_2_device_name" in devprop:
                                    device["devprop"][devprop] = device[
                                        "devprop"
                                    ][devprop].replace(
                                        "<device>", "talondx-00" + board
                                    )
                    if db_server["server"] == "dsrdmarx":
                        db_server_tmp["deviceList"][0]["devprop"][
                            "rdmaTxTangoDeviceName"
                        ] = db_server_tmp["deviceList"][0]["devprop"][
                            "rdmaTxTangoDeviceName"
                        ].replace(
                            "<device>", "talondx-00" + board
                        )
                        db_server_tmp["deviceList"][0]["alias"] = "rx" + board
                    if db_server["server"] != "talondx_log_consumer":
                        db_server_tmp["instance"] = config_map_json[
                            "config_commands"
                        ][board]["server_instance"]
                        if db_server["server"] != "dsrdmarx":
                            db_server_tmp["device"] = "talondx-00" + board
                        db_servers_list.append(db_server_tmp)
                if db_server["server"] == "talondx_log_consumer":
                    db_server_tmp = copy.deepcopy(db_server)
                    db_servers_list.append(db_server_tmp)
            talondx_config_dict["tango_db"] = {"db_servers": db_servers_list}

        with open(self._config_file, "w") as f:
            json.dump(talondx_config_dict, f, indent=4)

    def config_db(self):
        try:
            bitstream_file = self._extract_archive()
            with open(bitstream_file, "r") as f:
                bitstream_json = json.load(f)
            templates = bitstream_json["DeTrI"]

            db_params = TalonDxConfig(config_file=self._config_file).tango_db()
            inputjson = db_params.get("db_servers", "")

            device_list = []
            for server in inputjson:
                logging.info(f"Adding {server.get('server')}")
                dbpop = DbPopulate(server, templates)

                # Remove existing records
                dbpop.process(mode="remove")
                dbpop.process(mode="add", device_list=device_list)
            logging.debug(f"Devices added:\n{device_list}")
        finally:
            logging.info(f"Removing temporary directory {self._temp_dir}")
            shutil.rmtree(self._temp_dir)

    """
    Extracts the bitstream archive to a local temporary directory. Returns the
    path of the extracted json file.
    """

    def _extract_archive(self):
        os.mkdir(self._temp_dir)
        cmd = f"tar -xvzf {self._bitstream} -C {self._temp_dir}".split()
        result = subprocess.run(
            cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, text=True
        )
        logging.info(result.stdout)

        if os.path.isdir(f"{self._temp_dir}/bin"):
            jsf = f"{self._temp_dir}/bin/talon_dx-tdc_base-tdc_vcc_processing.json"
        else:
            jsf = f"{self._temp_dir}/talon_dx-tdc_base-tdc_vcc_processing.json"

        if not os.path.isfile(jsf):
            logging.error("Failed to extract bitstream json file")
        logging.info(f"Extracted bitstream json file at {jsf}")
        return jsf


##########
# MAIN
##########

if __name__ == "__main__":
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)

    if os.getenv("TANGO_HOST") is None:
        logging.error("TANGO_HOST environment variable not defined")

    parser = argparse.ArgumentParser(description="Local DB Populate Helper")

    parser.add_argument(
        "bitstream",
        help="Path of the bitstream archive tar.gz",
        type=str,
    )

    parser.add_argument(
        "boardmap",
        help="Path of the talon boardmap json file",
        type=str,
    )

    parser.add_argument(
        "boards",
        help="Comma-separated board IDs. e.g. if using talon1 and talon2, use '1,2'",
        type=str,
    )
    args = parser.parse_args()

    print(args.bitstream)
    print(args.boardmap)
    print(args.boards)

    ld = LocalDeployer(args.bitstream, args.boardmap)
    ld.generate_talondx_config(args.boards)
    ld.config_db()

    sys.exit(0)
