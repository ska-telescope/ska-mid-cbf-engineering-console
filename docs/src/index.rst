SKA Mid CBF Engineering Console
===============================
.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

.. SYSTEM =============================================================
.. toctree::
   :maxdepth: 2
   :caption: System:

   system

.. README =============================================================
.. toctree::
   :maxdepth: 2
   :caption: Readme

   readme_link

.. BITE =============================================================
.. toctree::
   :maxdepth: 2
   :caption: BITE

   bite

.. CODE =============================================================
.. toctree::
   :maxdepth: 3
   :caption: Code

   talondx
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`