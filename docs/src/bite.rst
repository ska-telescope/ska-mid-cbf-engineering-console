BITE Client
===========
BITE is the built-in test environment for the Talon DX board. 
The BITE client is running in the Engineering Console container that runs on the Linux server (either standalone, or in a pod in the 
Kubernetes cluster). The BITE client communicates with Tango device servers running on the Talon DX board to monitor and control the 
FPGA firmware that generates the Long Sequence Test Vectors (LSTVs) used to test the correlator signal chain.

BITE Configuration
==================
Input Parameter Files
---------------------
There are four parameter files which are to be used in configuring the BITE for LSTV generation. 
Each of these has an accompanying schema file, against which the BITE client (or BDD test) will validate the input parameter files. 
The parameter files can be found in the ska-mid-cbf-system-tests repository, in the test_parameters directory. They are: 

    - `test_parameters/tests.json <https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/blob/main/test_parameters/tests.json>`_
    - `test_parameters/cbf_input_data/cbf_input_data.json <https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/blob/main/test_parameters/cbf_input_data/cbf_input_data.json>`_
    - `test_parameters/cbf_input_data/bite_config_parameters/bite_configs.json <https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/blob/main/test_parameters/cbf_input_data/bite_config_parameters/bite_configs.json>`_
    - `test_parameters/cbf_input_data/bite_config_parameters/filters.json <https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/blob/main/test_parameters/cbf_input_data/bite_config_parameters/filters.json>`_

The schemas for these are located in the same relative locations in the ska-mid-cbf-internal-schemas repository, but 
have "_schema.json" at the end of their names. They are: 

    - `test_parameters/tests_schema.json <https://gitlab.com/ska-telescope/ska-mid-cbf-internal-schemas/-/blob/main/test_parameters/tests_schema.json>`_
    - `test_parameters/cbf_input_data/cbf_input_data_schema.json <https://gitlab.com/ska-telescope/ska-mid-cbf-internal-schemas/-/blob/main/test_parameters/cbf_input_data/cbf_input_data_schema.json>`_
    - `test_parameters/cbf_input_data/bite_config_parameters/bite_configs_schema.json <https://gitlab.com/ska-telescope/ska-mid-cbf-internal-schemas/-/blob/main/test_parameters/cbf_input_data/bite_config_parameters/bite_configs_schema.json>`_
    - `test_parameters/cbf_input_data/bite_config_parameters/filters_schema.json <https://gitlab.com/ska-telescope/ska-mid-cbf-internal-schemas/-/blob/main/test_parameters/cbf_input_data/bite_config_parameters/filters_schema.json>`_


The `tests.json <https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/blob/main/test_parameters/tests.json>`_ parameter file contains a series of BITE tests. 
They are labelled by a Test ID, in the format of “Test 1” “Test 2”, etc., and each of them is defined with a set of test parameters according to their test scope and 
scan configuration. The “cbf_input_data” property associated with each Test ID links that test to a set of properties defined in the cbf_input_data JSON file above. 
These sets of CBF Input Data are named in this style: “BITE Data 1”, “BITE Data 2”, etc. Each is defined (in the aforementioned JSON) with an array of receptors; 
each receptor is defined in turn with a dish id, a bite configuration ID, a Talon board, and the initial timestamp of the BITE data that will be generated. 

The bite configuration ID corresponds with an identically-named ID in the “bite_configs.json”. These are named in the style of “BITE 1”, “BITE 2”, etc., 
and they are each given a brief summary in their “description” property to make up for the nondescript ID. Each bite configuration herein is defined with an array of 
Gaussian noise sources, an array of tone generators, and other parameters specific to LSTV generation and sample rate. 

Each element in the source array must be given a description, Gaussian noise parameters, a polarization coupling rho, and, if desired, the number of coefficients with 
which this source's filters will be defined (along with the bit width in which each coefficient is to be represented and stored). 
Each noise source is divided into the two polarizations, always termed “pol_x” and “pol_y”, and each of these is parameterized with the mean, standard deviation, 
and seed of the Gaussian noise it will generate. Furthermore, each polarization must be given a filter type, as named in the filters.json file in the list above. 

.. code-block:: javascript
    
    "sources": [
        {
            "description": "Noise input, independent between X and Y pols; unique filter shapes for X and Y pols.",
            "gaussian": {
                "pol_x": {
                    "seed": 1234,
                    "filter": "filter_ramp_up",
                    "noise_std": 32767,
                    "noise_mean": 0

                },
                "pol_y": {
                    "seed": 9876,
                    "filter": "filter_ramp_down",
                    "noise_std":32767,
                    "noise_mean":0
                }
            },
            "pol_coupling_rho": 0.0,
            "pol_Y_1_sample_delay": false,
            "fir_filter_num_taps": 1024,
            "fir_filter_coeff_bits": 16
        }
    ],

Each tone in the array of tone generators is similarly given a description, and divided into X and Y polarizations. Each of these components
is defined with a frequency and an amplitude scaling factor. 

.. code-block:: javascript

    "tone_gens": [
        {
            "description": "Basic 100 MHz tone.",
            "pol_x": {
                "frequency": 100e6,
                "scale": 0.0025
            },
            "pol_y": {
                "frequency": 333.3333333e6,
                "scale": 0.0025
            }
        }
    ],

Bite Configuration from BDD Tests
---------------------------------
`Mid CBF AA0.5 Test Strategy <https://confluence.skatelescope.org/display/SWSI/Mid+CBF+AA0.5+Test+Strategy>`_
`Mid CBF Signal Chain Verification <https://confluence.skatelescope.org/display/SE/Mid+CBF+AA0.5+Use+Cases#MidCBFAA0.5UseCases-MidCBFSignalChainVerification>`_

BITE Sequence Diagram
---------------------
.. uml:: diagrams/bite-sequence.puml
