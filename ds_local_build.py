import os
import json
import sys
import shutil

if __name__ == "__main__":
    f = open(sys.argv[1])
    ds_list = (sys.argv[2]).split(",")
    ds_base_dir=sys.argv[3]
    mcs_dir=sys.argv[4]
    eng_cons_dir = os.path.realpath(__file__)
    ds_config = json.load(f)
    for ds in ds_config:
        ds_name = ds['name']
        if ds_name in ds_list:
            source_exec = os.path.join(ds_base_dir, ds['src'])
            if ds_name == 'ds-rdma-rx':
                dest_exec = os.path.join(eng_cons_dir,ds['dest'])
            else:
                dest_exec = os.path.join(mcs_dir, ds['dest'])
            if os.path.exists(source_exec) == False:
                print(f'ERROR: Source executable for {ds_name} does not exist at: {source_exec}')
            elif os.path.exists(dest_exec) == False:
                print(f'ERROR: Source executable for {ds_name} does not exist at: {dest_exec}')
            else:
                shutil.copyfile(source_exec,dest_exec)
                print(f'Executable has been copied for: {ds_name}')



