############
Change Log
############

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

UNRELEASED
***********

1.1.1
***********
* CIP-3162: Bump ds-spead-descriptor to 1.7.0 to support multiple descriptor heaps for vis output
* CIP-3312: Bump RDT to 1.5.16 - changed fodm fitting algorithm, rounding phase correction coefficients
* CIP-3338: Removed BSP DS for safety since watchdog support is not yet implemented.

1.1.0
***********
* CIP-2621: Bump RDT to 1.5.14 to include First Order Phase Polynomials and reduce LSTV fo_validity_interval from 0.01 to 2**-13
* CIP-3148: Use the same start time for all LSTV replays
            Add a small constant to the HO delay polynomial for LSTV RDT
            RDT 1.5.15 - partial updates to address issues in cross-correlation
            Gaussian noise gen 2.0.1 - fix for pointer type safety
* CIP-3338: Adjusted fan controller settings to prevent board shuts down due to high temperature.
* CIP-2364: Bump HPS FSP to 0.9.1, fine channelizer to 1.3.1 to apply VCC gain corrections
* CIP-3127: Changed correlation bitstream from 0.5.0 to 1.0.1
* CIP-2987: Updated Board Map to Support PST

1.0.1
***********
* CIP-3183: Updated LUT Stage 1's regex to match location for PST Firmware
* CIP-3184: Updated midcbf-deployer's talondx_config builder process to deploy only the HPS FSP Devices related to the FSP Mode
* CIP-3048: Bumped ska-mid-cbf-fsp-app to v0.8.1, removing correlator switching to null bank during EndScan
* SKB-668:  ds-correlator v2.2.3 - Updated baseline ordering logic in LTA
* CIP-3140: ska-mid-cbf-fsp-app v0.8.2, ska-mid-cbf-vcc-app v1.2.7 - Align implementation for healthState checks in VCC and FSP
* ds-dct v2.0.2 - Fix memory leak
* CIP-3159: ds-slim-tx-rx v2.1.4 - Provide an interface to enable and disable loopback on SLIM Rx.
* CIP-3092: Change ddr_rate in ds-correlator (v2.2.2) from 40 to 45

1.0.0
***********
* CIP-2839: Updated FSP App to v0.8.0; adds RDT and DCT checks and polls the healthState attr every 3s.

1.0.0-rc.1
***********
* CIP-3028: Updated hw config after systems room re-organization.
* CIP-2240: Updated Vcc App to v1.2.5; adds DishID and sample rate checks and polls the healthState attr every 3s.
            Updated WIB to v0.6.0; ExpectedDishID is now an attribute instead of a property.
* CIP-2738: Fix a bug in resampler delay tracker causing gap in polynomials at HO delay
            model boundaries.
* CIP-2255: Updated Vcc App to v1.2.6; changed ConfigureScan command to parse FSP IDs as coarse channel ID
* SKB-485:  Limit sample_accum attribute value to max of 2,000,000 in WB state count
* CIP-3053: Updated RDT to not check against negative input timestamp.
            Reset LSTV RDT if it is in FAULT state.
* CIP-2622: Added 4 new attributes to be passed down to resampler delay tracker, in the write_fo_delay_model() function.
* CIP-2839: Updated FSP App to v0.8.0; adds RDT and DCT checks and polls the healthState attr every 3s.


0.10.12
*******
* CIP-2634: Added CircularBufferManager baseline test to ds-resampler-delay-tracker.
* CIP-2966: Added CreateDescriptor command to ds-spead-descriptor. It should be called
            before the StartScan command.
* CIP-2659: Updated HPS master to periodically check child processes and report health state.
* CIP-2624: Added support for calculating time_factor for FO phase_constant derivation.

0.10.11
*******
* CIP-1143: Updated resampler delay tracker (RDT) to support LSTV Gen.
            Added attribute enable_resampler to EC BITE
            Use the BITE RDT if enable_resampler is true, resampling LSTV to the specified dish sampling rate.
            If no delay models are provided, 0 coefficients are used.
            If delay models are provided, they will be provided to the RDT.
            Take talon target (e.g. "001") as input
* CIP-2923: Updated ds-spead-descriptor's UDP client and added more logs.
* CIP-2548: Updated talondx_boardmap.json with different template configurations based on FSP Function Mode
            requested for a board instead of hardcode configuration for each board.
            Updated talondx_boardmap.json to include a work in progress configuration for PST FSP Function Mode.
            Added features to map Target Boards to CORR/PST FSP Function Mode and to generate configurations based on the requested board:functionMode pair.
            Added the validateBoardModeConfiguration attribute in Deployer to disable the validation for FSP modes assignment to boards.
* CIP-2689: Reset corner turner status registers when starting up.
            Added a status summary attribute to the corner turner to return status and timestamp information.
            Added change events for some status attributes in the corner turner.
* SKB-393:  Fix baseline ordering bug
* CIP-2565: Fix correlator error during a scan that uses more receptors than the previous scan

0.10.10
*******
* MAP-70: Removing the directories in /images for the old, pod-based BITE and Deployer scripts
          Swapping refs to these to use the new TANGO devices under /src
* CIP-1382: Added logging statements for timegap_deteced to circular buffer and added an alarms for the timestamp gap, as well as the other three error statuses.
* CIP-2600: deploy correlator bitstream 0.5.0 and related HPS device server updates.
* CIP-2356: Updated PSR repo version to 1.4.1 to fix register set version mismatch. Also fixed an uncaught bug; the short_packet_error setter was writing to packet_error not short_packet_error.

0.10.9
******
* MAP-113: Removing disparate talondx_boardmap files, only one is located in /src/ now.
           Added a script written for local Docker container deployer commands, for local HPS deployment.
* MAP-110: Adding a PVC definition file back into EC for standalone deployments
* MAP-84: Updating the Makefile to run on ds-bite and ds-deployer, removing old/unused commands
* CIP-1382: Added logging statements for detecting timestamp gaps in the RDT
* CIP-2560: HPS updates to support multiple FSPs

0.10.8
******
* SKB-367:  Updates fsp-app to 0.4.13, which programs receive addresses in Host LUT stage
            2 using the 2-item tuple format instead of the 3-tuple format previously expected.
* CIP-2377: Fixed failing to capture visibilities in the subsequent scans after GoToIdle
* CIP-2368: Fixed baselines potentially coming out in the wrong order
* CIP-2264: Various updates to make 4VCC-1FSP scans more stable
* CIP-2531: Updated to use the validity period in delay model v3.0 as the model duration
* MAP-73:   Simplify ds-bite interface and add the following commands that take json inputs as strings:
            - load_cbf_input_data
            - load_bite_config_data
            - load_filter_data

0.10.7
******
* CIP-2471: Update the Deployer DS to download FPGA bitstream file in a more straightforward manner and
            synchronize the boardmap inside /src/ with the one in /images/
* MAP-69:   Refactored the ds-deployer so that MCS On() and Off() commands work with the new schema of
            JSON configuration set by the ds-deployer.

0.10.6
******
* CIP-2463: fix incorrect device property name for FSP Corr Controller in the boardmap.

0.10.5
******
* CIP-1620: Engineering console can send delay models to the TMC emulator sequentially
* CIP-2368: Speed up downloading fpga bitstream
* CIP-2359: fix AttributeError from make mcs-on command

0.10.4
******
* CIP-2356: bump ds-packet-stream-repair version to 1.4.0 to handle packet strean repair register update
* CIP-1766: bump ska-mid-cbf-fsp-app version to 0.4.10 for renaming receptor id to vcc id

0.10.3
******
* CIP-2257: bump ska-mid-cbf-fsp-app version to 0.4.9 to handle delay model schema v3.0

Development
************
* Add .readthedocs.yaml configuration file

0.10.2
******
* MAP-78: Update SKA Artefact repo URL to match new location, add in condition for ec Helm chart to check for tango-base being enabled.

0.10.1
******
* CIP-2238: Bump HPS FSP app version to 0.4.8

0.10.0
*****
* CIP-2264: Update FPGA Bitstream to version 0.2.6. Deploy board support package DS to Talon boards.

0.9.24
*****
* CIP-2007: Update FPGA Bitstream to version 0.2.5

0.9.23
*****
* MAP-40: Clean up TANGO device server for EC BITE: remove temporary config file folder and update attributes for implemenation.
* MAP-39: Clean up TANGO device server for EC Deployer: remove SLIM from device server, remove config files, fix db_device_check command. Will not function until MCS intergration is finished in MAP-71

0.9.22
*****
* CIP-2228: Removed ext_config/initial_system_param.json, as well as any reason BITE configuration scripts had for using it
* CIP-2228: Update basic_test_parameters.json so that it is up to date with ska-mid-cbf-system-tests test parameter files, and contains all required test parameters once provided by removed file
* CIP-2228: Update midcbf_bite.py to retrieve k-value from system-tests's 'cbf_input_data.json' script, rather than bite_config.json, which should only be providing info to bite_client.py

0.9.21
*****
* MAP-40: Make a TANGO Device server for engineering console BITE, using bite-client to hook up to other tango devices.

0.9.20
*****
* CIP-2311: Updated SLIM mesh test after changing FQDN format and updating DsSlimTxRx.

0.9.19
*****
* MAP-39: Creating EC Deployer TANGO Device Server
* MAP-39: Changing File Structure to Use Multiple Docker Container Configurations
* CIP-2174: Update EC Bite to set the time code relative to SKA epoch for lstv_replay

0.9.18
*****
* DISCARDED

0.9.17
*****
* DISCARDED

0.9.16
*****
* CIP-2053: Updated FSP App to 0.4.7 to fix incorrect SPEAD descriptor channel ID assignment.

0.9.15
*****
* CIP-2098 Updated k-values in initial_system_param.json to acceptable k-values. Also ensured that these will match the k-values defined in the init sys param file of system-tests
* Updated basic_test_parameters.json so that a valid k-value is defined in the BITE config, and so that the same naming convention of the system-tests parameter files is observed

0.9.14
*****
* CIP-2000 Updated HPS FSP App and resampler delay tracker to improve reliability during delay model initialization.

0.9.13
*****
* CIP-1940 Updated HPS VCC App and WIB to implement a sample rate matching check between MCS and FW during configure_scan.

0.9.12
*****
* CIP-2000 Updated HPS Resampler delay tracker to 1.5.2. Fixed race condition causing first order delay model store to be cleared sometimes.
* Updated the delay model to use current time as epoch when testing with eng console.

0.9.11
*****
* CIP-2047 Updated VCC App to v1.2.1 to fix off by one error in frequency slice and frequency band conversion to circuit switch.
* CIP-2052 Updated DsSlimTxRx to v2.1.2 in boardmap, which addresses idle error warnings.

0.9.10
*****
* CIP-1987 Updated boardmap to create Slim Vis links with mbo_1 and FS links with mbo_2.

0.9.9
*****
* CIP-1965 Updated mesh test to parse configs with any number of FSPs.

0.9.8
*****
* Updated HPS resampler-delay-tracker version to 1.5.1 (CIP-1830)

0.9.7
*****
* Updated HPS FSP app version (CIP-1883), fixed InitSysParam input argument schema.

0.9.6
*****
* Updated ds-slim-txrx version to 2.1.1; changes float attributes to doubles.

0.9.5
*****
* Updated bite client so that it will 'select' (and thus generate) the configured tones via the tone_select attribute of lstv_gen.

0.9.4
*****
* Updated bite client with fix to convert normalized noise_mean/noise_std values from test parameters file into signed/unsigned 16 bit values for the lstv_gen noise_mean/noise_std registers, which have not been updated to accept normalized values as dsgaussiannoisegen has.

0.9.3
*****
* Added new mesh-test make directive.
* Updated ds-histogram version to 1.2.1; implements samples_between_pps attribute.

0.9.2
*****
* Updated bite client with fix to scaling coefficients for polarization coupler alpha/beta write values.

0.9.1
*****
* Reintroduced SLIM Tx Rx into boardmap.

0.9.0
*****
* Updated Gaussian Noise Gen version, due to tango.json changes; namely noise_std and noise_mean.
* Tweaked how the noise_std attribute is configured in bite_client.py.

0.8.16
*****
* Fixed missing fetch of K value from sytem test parameters.

0.8.15
*****
* Updated HPS devices Resampler Delay Tracker and FSP App to changed fs sample rate to Hz in integer.
* Provide sys param to EC bite so that it can derive the sampling rate from offset k.

0.8.14
*****
* Removes slim-tx-rx devices from boardmap again.

0.8.13
*****
* Allows VCC App's ConfigureBand to be called from INIT state.
* Set's the VCC Controller's device state to ON from configure_band().
* Allows VCC Base's On and Disable to be called from INIT state.

0.8.12
*****
* Removes init_connection() call from slim-tx-rx initialization process.
* Removes slim-link HPS device server from boardmap now that it is obsolete.

0.8.11
*****
* Removes Delta F and K from VCC App and replaces them with dish_sample_rate and num_samples_per_frame.

0.8.10
*****
* Added a default 4 board Dish-VCC mapping to call the InitSysParam command.

0.8.9
*****
* Added hw_config.yaml file and updated scripts to enable configuration of MCS controlled hardware.

0.8.8
*****
* Returns SLIM Link and Tx/Rx devices to HPS deployment.
* Adds slim-link-test.py script for testing SLIM Link DS.

0.8.7
*****
* HPS master and Talon Status now support DDR calibration health check during the On command.
* SLIM Link and Tx/Rx are no longer deployed to HPS.
* Fix python linting to check all Engineering Console images.

0.8.6
*****
* Updated device-check logging to work with system-tests.
